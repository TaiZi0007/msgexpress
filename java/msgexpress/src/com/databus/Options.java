package com.databus;

public class Options {

	public byte type;
	public int dstaddr;
	public int serial;
	public boolean sequence;
	public byte protocol;
	public boolean multicast;
	public byte loadbalance;
	public short code;
	
}
