// 消息回调接口
package com.databus;

import com.packet.PackageData;

public interface MessageCallBack {
	enum EConnectStatus
	{
		ConnectStatus_Offline,   // 离线状态
		ConnectStatus_Online,    // 在线状态
		ConnectStatus_Master,    // 升级为主服务模式
		ConnectStatus_Slave,     // 降级为从服务模式
	}

	// 收到请求消息的回调函数
	public void OnRequest(PackageData packData);
	
	// 收到应答消息的回调函数
	public void OnResponse(PackageData packData);
	
	// 收到通知的回调函数
	public void OnPublish(PackageData packData);
	
	// 收到与服务端连接状态变化的回调函数
	public void OnConnectChange(EConnectStatus status);
	
	// 获取连接地址
	public int GetAddr();
}
