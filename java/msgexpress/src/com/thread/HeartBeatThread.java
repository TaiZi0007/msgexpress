// 蹇冭烦绾跨▼
package com.thread;

import com.databus.SerialNumGenerator;
import com.databus.*;
import com.protobuf.MsgExpress;

public class HeartBeatThread implements Runnable {
	
	public HeartBeatThread(MsgSendThread sendThread, InfoCount infoCount, int interval)
	{
		mSendThread        = sendThread;
		mInfoCount         = infoCount;
		mHeartBeatInterval = interval;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Log.info("Start Heart Beat Thread...");
		
		int topMem = 0;
		MsgExpress.HeartBeat.Builder builder = MsgExpress.HeartBeat.newBuilder();
		builder.setCpu(0);
		while(!Thread.interrupted())
		{
			int mem = (int)(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024;
			if(mem > topMem)
			{
				topMem = mem;
			}
			builder.setTopmemory(topMem);
			builder.setMemory(mem);
			if(mInfoCount!=null) {
				builder.setSendqueue(mInfoCount.GetSendQueue());
				builder.setReceivequeue(mInfoCount.GetReceiveQueue());
				builder.setSendrequest(mInfoCount.GetSendRequest());
				builder.setRecvrequest(mInfoCount.GetRecvRequest());
				builder.setSendresponse(mInfoCount.GetSendResponse());
				builder.setRecvresponse(mInfoCount.GetRecvResponse());
				builder.setSendpub(mInfoCount.GetSendPub());
				builder.setRecvpub(mInfoCount.GetRecvPub());
			}
			builder.setServertime(System.currentTimeMillis() / 1000);
			
			mSendThread.SendMessage(builder.build(), SerialNumGenerator.GetAsynNum(), null);
			
			try {
				Thread.sleep(mHeartBeatInterval);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				break;
			}
		}
		Log.info("End Heart Beat Thread...");
	}

	private MsgSendThread mSendThread;      // 鍙戦�佺紦鍐插尯
	private InfoCount mInfoCount;          // 淇℃伅缁熻鎺ュ彛
	private int mHeartBeatInterval;         // 蹇冭烦鍛ㄦ湡
}
