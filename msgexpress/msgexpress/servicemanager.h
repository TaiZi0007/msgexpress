#ifndef _SERVICEMANAGER_H_
#define _SERVICEMANAGER_H_

#ifdef _WIN32 
#include <winsock2.h>
#include <windows.h>
#include <stdio.h>
#define _CRTDBG_MAP_ALLOC
#endif
#include <assert.h>

#include <map>
#include <hash_map>
#include <set>
#include <vector>
#include <string>



#include "sputils.hpp"
#include "spthread.hpp"
#include "userlist.h"
#include "package.h"
#include "msgexpress.pb.h"

using namespace std;
#ifdef __GNUC__ 
using namespace __gnu_cxx;
namespace __gnu_cxx
{
    template<> struct hash<string>
    {
      size_t operator()(const string& s) const
      {
         return __stl_hash_string(s.c_str());
      }
    };
}
#endif

class MSGEXPRESS_API ServiceManager
{
private:
#define MapSubscription hash_map<int,vector<int>*>//key 是用户id
	hash_map<int,UserList*> mMapService;//服务提供者名单

	MapSubscription mMapUserSubIdList;//key是用户id，
	hash_map<string,MsgExpress::SubscribeData*> mMapUserSubInfo;//key是由用户id和subId构成

	hash_map<string,MapSubscription*> mMapSubDetails;//订阅的详细信息,key是由订阅条件生成的字符串
	UserList  mOnlineSidList;
	sp_thread_mutex_t mMutex;
	
public:
	ServiceManager();
	~ServiceManager();

	void regService(vector<int>& vecService,SP_Sid_t sid);
	void unregService(set<int>& setService,SP_Sid_t sid);
	void getServiceProviderList(int funcId,SP_SidList* list);

	void subscribe(const MsgExpress::SubscribeData& sub,unsigned int sid);
	void unSubscribe(int subId,unsigned int sid);
	void unSubscribeAll(unsigned int sid);
	UserList * getUserList(){ return &mOnlineSidList; }
	void getSubscriberList(MsgExpress::PublishData* pubData,map<int,vector<MsgExpress::SubscribeData*>*>& mapUserSubList);
	
};

#endif