#ifndef _MSGHANDLER_H_
#define _MSGHANDLER_H_

#ifdef _WIN32
#include <winsock2.h>
#include <windows.h>
#include <stdio.h>
#include <crtdbg.h>
#define _CRTDBG_MAP_ALLOC
#endif
#include <assert.h>



#include "spthread.hpp"
#include "spporting.hpp"
#include "spwin32iocp.hpp"
#include "spiocpserver.hpp"

#include "spmsgdecoder.hpp"
#include "spbuffer.hpp"

#include "sphandler.hpp"
#include "spresponse.hpp"
#include "sprequest.hpp"
#include "sputils.hpp"

#include "messageutil.h"
#include "msgdecoder.h"
#include "servicemanager.h"
#include "package.h"



class MsgHandler : public SP_Handler {
public:
	MsgHandler( ServiceManager * manager );
	virtual ~MsgHandler();

	virtual int start( SP_Request * request, SP_Response * response );

	// return -1 : terminate session, 0 : continue
	virtual int handle( SP_Request * request, SP_Response * response );

	virtual void error( SP_Response * response );

	virtual void timeout( SP_Response * response );

	virtual void close();

private:
	SP_Sid_t mSid;
	int mMeetingId;
	ServiceManager * mServiceManager;
	std::set<int> mServiceList;//我提供的服务
	//std::map<int,MsgExpress::SubscribeData*> mSubscribeList;//我订阅的服务
	static int mMsgSeq;

	void process(Package* package, SP_Response * response);
	void post( SP_Response * response, const char * buffer,size_t size, SP_SidList* toUsers);
	void post( SP_Response * response, const char * buffer,size_t size, SP_Sid_t toSid);
	void broadcast( SP_Response * response, const char * buffer,size_t size, SP_Sid_t * ignoreSid = 0 );
	void broadcast( SP_Response * response, const string& msg, SP_Sid_t * ignoreSid = 0 );

	void regService(Package* package, SP_Response * response);
	void subscribe(Package* package, SP_Response * response);
	void complexSubscribe(Package* package, SP_Response * response);
	void unsubscribe(Package* package, SP_Response * response);
	void publish(Package* package, SP_Response * response);
};

class MsgCompletionHandler : public SP_CompletionHandler {
public:
	MsgCompletionHandler(){}
	~MsgCompletionHandler(){}
	virtual void completionMessage( SP_Message * msg );
};

class MSGEXPRESS_API MsgHandlerFactory : public SP_HandlerFactory {
public:
	MsgHandlerFactory( ServiceManager * manager ){mServiceManager=manager;}
	virtual ~MsgHandlerFactory(){}

	virtual SP_Handler * create() const{return new MsgHandler(mServiceManager);}

	virtual SP_CompletionHandler * createCompletionHandler() const{return new MsgCompletionHandler();}

private:
	ServiceManager * mServiceManager;
};

#endif