#ifndef _MESSAGE_H_
#define _MESSAGE_H_

#include "protobuf.h"
#ifdef _WIN32
#include "winSock2.h"
using namespace std::tr1;
#elif __linux__
#include <arpa/inet.h>
#include <boost/shared_ptr.hpp>
using namespace boost;
#endif

#ifdef _WIN32
 #ifdef MSGEXPRESS_EXPORTS
 #define MSGEXPRESS_API __declspec(dllexport)
 #else
 #define MSGEXPRESS_API __declspec(dllimport)
 #endif
#elif __linux__
  #ifdef MSGEXPRESS_EXPORTS
  #define MSGEXPRESS_API
  #else
  #define MSGEXPRESS_API
  #endif
#endif


using namespace google::protobuf;

static const char PackageFlag='P';
static const unsigned char Version1=1;

#pragma pack(push,1)
struct MSGEXPRESS_API PackageHeader
{
	enum MsgType:unsigned char
	{
		Request=1,
		Response,
		Publish,
	};
	char flag1;                 //数据包开始标识'P'
	char flag2;                 //数据包开始标识'P'
	unsigned char version;     //版本号，1
	MsgType type;              //数据包类型
	unsigned char off;         //数据块的偏移量
	union {
	    unsigned char options;     //开关项
		struct {
			unsigned char ziped:1; //数据体是否压缩
			unsigned char ext:1; //是否有扩展数据
			unsigned char others:6;
		};
	};
	unsigned char reserve1;
	unsigned char reserve2;
	unsigned int serialnum;    //流水号
	unsigned int bodysize;     //数据包体大小
	unsigned int srcaddr;    //源地址
	unsigned int dstaddr;    //目标地址
	union {
		unsigned int command;   //命令，全局唯一，当消息类型为request/response时有效。
        struct { 
            unsigned int functionindex:20;//app的api标识，当消息类型为request/response时有效。
            unsigned int app:12;  //app类别，当消息类型为request/response时有效。
        };
	};
	PackageHeader()
	{
		memset(this,0,sizeof(PackageHeader));
		flag1=flag2=PackageFlag;
		version=Version1;
	}
	void read(const char* buffer)
	{
		memcpy(this,buffer,sizeof(PackageHeader));
		serialnum=ntohl(serialnum);
		bodysize=ntohl(bodysize);
		srcaddr=ntohl(srcaddr);
		dstaddr=ntohl(dstaddr);
        command=ntohl(command);
	}
	void write(char* buffer)
	{
		memcpy(buffer,this,sizeof(PackageHeader));
		PackageHeader* header=(PackageHeader*)buffer;
		header->serialnum=htonl(header->serialnum);
		header->bodysize=htonl(header->bodysize);
		header->srcaddr=ntohl(header->srcaddr);
		header->dstaddr=ntohl(header->dstaddr);
        header->command=ntohl(header->command);
	}
	std::string getDebugString()
	{
		char buf[1024];
		sprintf_s<1024>(buf,"serial:%d,type:%d,command:%x,app:%d,funcIdx:%d,src:%d,dst:%d,options:%x\r\n",serialnum,type,command,app,functionindex,srcaddr,dstaddr,options);
		return string(buf);
	}
};
#pragma pack(pop)

static const size_t PackageHeaderLength=sizeof(PackageHeader);

#define MessagePtr shared_ptr<Message>
#define PackagePtr shared_ptr<Package>

class MSGEXPRESS_API Package
{
private:
	PackageHeader m_Header;
	MessagePtr m_pMsg;
    MessagePtr m_pExtMsg;
	string m_Type;
	char*  m_Content;
	size_t m_Size;
private:
	Package(){}
public:
	#ifdef _WIN32
	Package(const PackageHeader& header,std::string& classType,const char* data,size_t size,MessagePtr pMsg=NULL);
        #elif __linux__
	Package(const PackageHeader& header,std::string& classType,const char* data,size_t size,MessagePtr pMsg);
        #endif	
	~Package();

	MessagePtr getMessage(){return m_pMsg;}
	MessagePtr getExtMessage(){return m_pExtMsg;}
	string getClassType(){return m_Type;}
	PackageHeader::MsgType getPackageType(){ return m_Header.type; }
	unsigned int getSerialNum(){return m_Header.serialnum;}
	unsigned int getCommand(){ return m_Header.command;}
	unsigned int getApp(){ return m_Header.app; }
	unsigned int getFunctionIdx(){ return m_Header.functionindex; }
	char* getContent(){ return m_Content;}
	size_t getSize(){ return m_Size; }
    const PackageHeader& getHeader(){ return m_Header; }
public:
    void setSrcAddr(unsigned int addr);
    void setDstAddr(unsigned int addr);
    unsigned int getSrcAddr(){ return m_Header.srcaddr; }
    unsigned int getDstAddr(){ return m_Header.dstaddr; }
    std::string getDebugString(){ return m_Header.getDebugString()+"--"+ m_pMsg->ShortDebugString(); }
};


#endif