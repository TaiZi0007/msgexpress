#pragma once

#include <string>
#include "xmlconfig.h"

using namespace std;

enum enumClusterMode
{
	None=0,
	MasterSlave=1,
	Loadbalance=2,
};
class MSGEXPRESS_API ServerConfig:public XMLConfig
{
public:
	bool connectRedis;
	string storage_ip;
	int storage_port;
	int storage_db;
	string name;
	string host_;
    int port_;
	int brokerId;
	enumClusterMode clustermode;
	string clusterCfg;
    int thread_;
    int max_client_;
    int connect_time_out_;
    int max_pack_len_;
	int mps;
	int zlib_switch;
	int zlib_threshold;
	string auth;
	string mastrslaveservice;
	int routemode;
	int recvqueuesize;
	int sockbuffer;

	bool publish2myselt;
	bool publishneedreply;

	int loglevel;
	bool reportlog;
	int reportlevel;
public:
    ServerConfig();
    ~ServerConfig(){};
    virtual bool ReadConfigFile();
private:
    bool ReadServerCFG(TiXmlElement *root);
    const char* GetNodeValue(string name,TiXmlElement *parent);

};

