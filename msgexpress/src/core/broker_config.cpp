
#include <iostream>
#include "broker_config.h"
#include "../inc/logger.h"
#include "../inc/package.h"


bool ServerConfig::ReadConfigFile()
{
	TiXmlElement *root = mDocument->FirstChildElement("configuration");
	if(!root)
		return false;
    if (!ReadServerCFG(root))
        return false;
    return true;
}
ServerConfig::ServerConfig()
{
	clustermode = enumClusterMode::None;
	routemode=0;
	recvqueuesize=1024;
	sockbuffer = 100000;
	loglevel=2;
	reportlog=true;
	mps = 0;
	publish2myselt = false;
	publishneedreply = false;
}
bool ServerConfig::ReadServerCFG(TiXmlElement *root)
{
	TiXmlElement *redis = root->FirstChildElement("cache");
	if( redis == NULL)
	{
		connectRedis=false;
	}
	else
	{
		//const char* str=redis->Attribute("port",&storage_port);
		const char* str=redis->Attribute("db",&storage_db);
		//const char* addr=redis->Attribute("ip");
		//if(addr)
		//	storage_ip=addr;
		connectRedis=true;
		LOG4_INFO( "Server storage settings,ip=%s,port=%d,db=%d" ,storage_ip.c_str(),storage_port,storage_db );
	}
	/** servercfg **/
	TiXmlElement *servercfg = root->FirstChildElement("server");
	if(!servercfg)
    {
        return false;
    }

    const char *str ;

	str = GetNodeValue("name",servercfg);
    if (str)
        name=str;
	else
		name="broker";

    str = GetNodeValue("host",servercfg);
    if (!str)
        return false;
    host_ = str;

    str = GetNodeValue("port",servercfg);
    if (!str)
        return false;
    port_ = atoi(str);

	str = GetNodeValue("brokerid", servercfg);
	if (!str)
	{
		LOG4_ERROR("Not config broker id.");
		return false;
	}
	brokerId = atoi(str);

	TiXmlElement* clustercfg = servercfg->FirstChildElement("cluster");
	if (clustercfg)
	{
		try{
			str = GetNodeValue("mode", clustercfg);
		}
		catch (...){}
		if (str)
		{
			if (stricmp(str, "Loadbalance") == 0)
			{
				clustermode = enumClusterMode::Loadbalance;
			}
			else if (stricmp(str, "masterslave") == 0)
			{
				clustermode = enumClusterMode::MasterSlave;
			}
		}
		str = GetNodeValue("config", clustercfg);
		if (clustermode != enumClusterMode::None && str)
		{
			clusterCfg = str;
		}
	}
    str = GetNodeValue("thread",servercfg);
    if (!str)
        return false;

    thread_ = atoi(str);

    str = GetNodeValue("maxclient",servercfg);
    if (!str)
        return false;
    max_client_ = atoi(str);

	str = GetNodeValue("auth",servercfg);
    if (!str)
        return false;
	auth = str;

	str = GetNodeValue("routemode",servercfg);
    if (!str)
        routemode=0;
	else
        routemode = atoi(str);

	str = GetNodeValue("recvqueuesize",servercfg);
    if (str)
        recvqueuesize = atoi(str);

	str = GetNodeValue("sockbuffer", servercfg);
	if (str)
		sockbuffer = atoi(str);

	str = GetNodeValue("master-slave-service",servercfg);
    if (str)
        mastrslaveservice = str;

	TiXmlElement* zlibcfg = servercfg->FirstChildElement("zlib");
    if (!zlibcfg)
        return false;

    str = GetNodeValue("switch",zlibcfg);
    if (!str)
        return false;
	zlib_switch = atoi(str);
	str = GetNodeValue("threshold",zlibcfg);
    if (!str)
        return false;
	zlib_threshold = atoi(str);


    TiXmlElement* ele = servercfg->FirstChildElement("connection");
    if (!ele)
        return false;

    str = GetNodeValue("timeout",ele);
    if (!str)
        return false;
    connect_time_out_ = atoi(str);

    str = GetNodeValue("maxpacklen",ele);
    if (str)
         max_pack_len_ = atoi(str);

	str = GetNodeValue("mps", ele);
	if (str)
		mps = atoi(str);

	TiXmlElement* publishcfg = servercfg->FirstChildElement("publish");
	if (publishcfg)
	{
		str = GetNodeValue("tomyself", publishcfg);
		if (str)
			publish2myselt = atoi(str);

		str = GetNodeValue("needreply", publishcfg);
		if (str)
			publishneedreply = atoi(str);
	}

	TiXmlElement *logcfg = root->FirstChildElement("log");
	if(logcfg)
	{
		str = GetNodeValue("level",logcfg);
		if (str)
			loglevel = atoi(str);
		str = GetNodeValue("report",logcfg);
		if (str)
			reportlog=atoi(str);
		str = GetNodeValue("reportlevel",logcfg);
		if (str)
			reportlevel=atoi(str);
	}

    return true;
}
const char* ServerConfig::GetNodeValue(string name,TiXmlElement *parent)
{
    if (!parent)
        return NULL;
	TiXmlElement *ne = parent->FirstChildElement(name.c_str());

	if (!ne || !ne->FirstChild())
    {
        return NULL;
    }
    return ne->FirstChild()->Value();
}



