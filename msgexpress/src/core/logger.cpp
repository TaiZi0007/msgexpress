#include "../inc/logger.h"
#include <stdarg.h>
#include <stdio.h>
#include "../inc/package.h"

#ifdef __linux__
#include <unistd.h>
#endif

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/helpers/exception.h>
#include <log4cxx/propertyconfigurator.h>


#pragma warning(disable:4996)

using namespace std;
using namespace log4cxx;

#define LOG_LENGTH_MAX 1024 //

log4cxx::LoggerPtr g_logger(log4cxx::Logger::getLogger("")); 
bool g_inited=false;

function<void(string, string)> g_logcb = nullptr;
int reportlevel=ll_warn;
int g_level=-1;

#ifdef _WIN32
bool LogInit(const char * conffile)
{
	if(g_inited)
		return true;
	g_inited=true;
    using namespace log4cxx;
	string tmp(conffile);
	if(strrchr(conffile,'\\')==NULL && strrchr(conffile,'/')==NULL)
	{
		char _szFilePath[MAX_PATH];
		//获得当前目录
		GetCurrentDirectoryA(MAX_PATH,_szFilePath);   
		char* p = strrchr(_szFilePath, '\\');
		*p = 0;
		//获得与当前目录平级的config目录
		string log_file(_szFilePath);
		log_file += "\\config\\" + tmp;
		printf("log path:%s\r\n",log_file.c_str());
		PropertyConfigurator::configure(File(log_file)); 
	}
	else
	{
		printf("log path:%s\r\n",conffile);
		PropertyConfigurator::configure(File(conffile)); 
	}
    return true;
}
bool LogInit(const wchar_t * conf)
{
	if(g_inited)
		return true;
	g_inited=true;
    using namespace log4cxx;
	wstring conffile(conf);
	if(wcsrchr(conffile.data(), L'\\')==NULL && wcsrchr(conffile.data(), L'/')==NULL)
	{
		wchar_t _szFilePath[MAX_PATH];
		//获得当前目录
		GetCurrentDirectory(MAX_PATH,_szFilePath);   
		wchar_t* p = wcsrchr(_szFilePath, L'\\');
		*p = 0;
		//获得与当前目录平级的config目录
		wstring log_file(_szFilePath);
		log_file += L"\\config\\" + conffile;
		wprintf(L"log path:%s\r\n",log_file.c_str());
		PropertyConfigurator::configure(File(log_file)); 
			
	}
	else
	{
		wprintf(L"log path:%s\r\n",conffile.c_str());
		PropertyConfigurator::configure(File(conffile)); 
	}
    return true;
}
#elif __linux__
bool LogInit(const char * conffile)
{
	if(g_inited)
		return true;
	g_inited=true;
    using namespace log4cxx;
	if(strrchr(conffile,'/')==NULL)
	{
		//get path 
		#define MAX_PATH 216
		char buffer[MAX_PATH];
		char* path =  getcwd(buffer ,MAX_PATH);
		if(path == NULL)
		{   
				LOG4_ERROR("can't get cur path");
				return false;
		}
		char * p = strrchr(path,'/');
		if(p == NULL)
		{
				return false;
		}
		*p = '\0';
		char * tmp = strrchr(path,'/');
		if(tmp == NULL)
			return false;
		*p = '\0';
		string log_file(path);
		log_file += "/config/" + string(conffile);
		PropertyConfigurator::configure(File(log_file)); 
		printf("log path:%s",log_file.c_str());
	}
	else
	{
		PropertyConfigurator::configure(File( string(conffile))); 
		printf("log path:%s",conffile);
	}
    return true;
}
#endif

void SetLogCallback(function<void(string,string)> logcb,int level)
{
	g_logcb=logcb;
	reportlevel=level;
}
void SetLevel(int level)
{
	if(-1==g_level)
	    g_level=level;
}
int GetLevel()
{
	return g_level;
}

log4cxx::LoggerPtr GetLogger()
{
	return g_logger;
}
void Log2(TLogLevel level, const char* log)
{
	if (g_logger == NULL)
		return;
	switch (level)
	{
	case ll_debug:
		g_logger->debug(log);
		if (g_logcb && level >= reportlevel)
			g_logcb("debug", log);
		break;
	case ll_info:
		g_logger->info(log);
		if (g_logcb && level >= reportlevel)
			g_logcb("info", log);
		break;
	case ll_warn:
		g_logger->warn(log);
		if (g_logcb && level >= reportlevel)
			g_logcb("warn", log);
		break;
	case ll_error:
		g_logger->error(log);
		if (g_logcb && level >= reportlevel)
			g_logcb("error", log);
		break;
	case ll_fatal:
	default:
		g_logger->fatal(log);
		if (g_logcb && level >= reportlevel)
			g_logcb("fatal", log);
		break;
	}
}
void Log(TLogLevel level, const char* fmt, ...)
{
	if(g_logger==NULL)
		return;
	char log[LOG_LENGTH_MAX];
	log[0] = 0;
	try{
		va_list args;
		va_start(args, fmt);
#ifdef _WIN32
		int ret = vsnprintf_s<LOG_LENGTH_MAX>(log, sizeof(log) - 1, fmt, args);
#else
		int ret=vsnprintf(log, sizeof(log)-1, fmt, args);
#endif
		va_end(args);
	}
	catch (exception ex)
	{
		Log2(ll_error, "format log message error");
	}
	
	Log2(level,log);
}