#pragma once

#include <map>

class HttpRequest;
class HttpResponse;
class HttpMessage;

using namespace std;

class HttpMsgParser {
public:
	HttpMsgParser();
	~HttpMsgParser();

	void setIgnoreContent( int ignoreContent );
	int isIgnoreContent() const;

	int append( const void * buffer, int len );

	void clear();
	// 0 : incomplete, 1 : complete
	int isCompleted() const;

	HttpRequest * getRequest() const;

	HttpResponse * getResponse() const;

private:
	static int parseStartLine( HttpMessage ** message,
		const void * buffer, int len );
	static int parseHeader( HttpMessage * message,
		const void * buffer, int len );
	static int parseChunked( HttpMessage * message,
		const void * buffer, int len, int * status );
	static int parseContent( HttpMessage * message,
		const void * buffer, int len, int * status );
	static void postProcess( HttpMessage * message );

	static int getLine( const void * buffer, int len, char * line, int size );

	HttpMessage * mMessage;

	enum { eStartLine, eHeader, eContent, eCompleted };
	int mStatus;

	int mIgnoreContent;
};

class HttpMessage {
public:
	static const char * HEADER_CONTENT_LENGTH;
	static const char * HEADER_CONTENT_LENGTH2;
	static const char * HEADER_CONTENT_TYPE;
	static const char * HEADER_CONTENT_TYPE2;
	static const char * HEADER_CONTENT_LANGUAGE;
	static const char * HEADER_CONNECTION;
	static const char * HEADER_PROXY_CONNECTION;
	static const char * HEADER_TRANSFER_ENCODING;
	static const char * HEADER_DATE;
	static const char * HEADER_SERVER;

public:
	HttpMessage( int type );
	virtual ~HttpMessage();

	enum { eRequest, eResponse };
	int getType() const;

	void setVersion( const char * version );
	const char * getVersion() const;

	void appendContent( const void * content, int length = 0, int maxLength = 0 );
	void setContent( const void * content, int length = 0 );
	void directSetContent( void * content, int length = 0 );
	const void * getContent() const;
	int getContentLength() const;

	void addHeader( const char * name, const char * value );
	int removeHeader( const char * name );
	int removeHeader( int index );
	int getHeaderCount() const;
	const char * getHeaderName( int index ) const;
	const char * getHeaderValue( int index ) const;
	string getHeaderValue(const char * name) const;
	int isKeepAlive() const;
	
protected:
	const int mType;

	char mVersion[ 16 ];
	void * mContent;
	int mMaxLength, mContentLength;

	map<string, string> mapHeaderNameValue;

};

class HttpRequest : public HttpMessage {
public:
	HttpRequest();
	virtual ~HttpRequest();

	void setMethod( const char * method );
	const char * getMethod() const;

	void setURI( const char * uri );
	const char * getURI() const;

	void setURL( const char * url );
	const char * getURL() const;

	void setClinetIP( const char * clientIP );
	const char * getClientIP() const;

	void addParam( const char * name, const char * value );
	int removeParam( const char * name );
	int getParamCount() const;
	const char * getParamName( int index ) const;
	const char * getParamValue( int index ) const;
	string getParamValue(const char * name) const;
	string getParamList() const;
	string getJsonParam() const;

private:
	char mMethod[ 16 ], mClientIP[ 16 ];
	char * mURI, * mURL;

	map<string, string> mapParamNameValue;
};

class HttpResponse : public HttpMessage {
public:
	HttpResponse();
	virtual ~HttpResponse();

	void setStatusCode( int statusCode );
	int getStatusCode() const;

	void setReasonPhrase( const char * reasonPhrase );
	const char * getReasonPhrase() const;
	string toString() const;
private:
	int mStatusCode;
	char mReasonPhrase[ 128 ];
};

