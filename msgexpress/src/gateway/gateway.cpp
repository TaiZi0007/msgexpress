
#include <time.h>

#include "gateway.h"
#include "../inc/logger.h"
#include "../core/command_config.h"
#include "../core/messageutil.h"
#include "../client/publishdata.h"
#include "../proxy/proxyclient_config.h"

namespace UpdateServer //自动升级服务器
{
	const static int APPID=0x003;
};

namespace Security //账号服务器
{
	const static int APPID = 0x002;
};

namespace AccountServer //账号服务器
{
	const static int APPID = 0x005;
};



struct tagArg
{
	PackagePtr package;
	std::string token;
	unsigned int addr;
};
GatewayProxy::GatewayProxy()
{
}

GatewayProxy::~GatewayProxy()
{
}

void GatewayProxy::Init(const ProxyClientConfig &pclientconf)
{
	LocalCache::Initialize();
	/*if(MemCache::Initialize()==0)
	{
		int count=0;
		while(!MemCache::IsConnected())
		{
			sp_sleep(100);
			count++;
			if(count>10)
				break;
		}
	}
	dataset=pclientconf.storage_db;
	if(MemCache::IsConnected())
	{
		LOG4_INFO( "Connect to Storage service success,db=%d",dataset);
		MemCache::Select(pclientconf.storage_db);
	}
	else
		LOG4_ERROR( "Connect to Storage service failed.");*/
}

ClientInfo* GatewayProxy::GetClientInfo(unsigned int addr)
{
	ClientInfo* info=NULL;
	std::unique_lock<std::mutex> lock(mMutex);
	hash_map<unsigned int, ClientInfo*>::iterator it=mapAppInfo.find(addr);
	if(it!=mapAppInfo.end())//
	{
		info=it->second;
	}
	else
	{

		char key[24];
		sprintf(key,"%d",addr);
		string data,errstr;
		if(LocalCache::Get(key,data,&errstr)!=0)
		{
			LOG4_ERROR("Get storage failed,addr=%d,,dataset=%d,err:%s",addr,dataset,errstr.c_str());
		}
		else
		{
			MsgExpress::AppInfo appInfo;
			if(!appInfo.ParseFromString(data))
			{
				LOG4_ERROR("Parse from storage data failed,addr=%d",addr);
			}
			else
			{
				info=new ClientInfo();
				info->address=addr;
				info->status=appInfo.status();
				info->token=appInfo.token();
				info->userid=appInfo.logintime();//logintime用于存储userid
				mapAppInfo[addr]=info;
			    if(info->token.size()>0)
				    mapToken2Addr[info->token]=addr;
			}
		}
	}

	return info;
}

bool GatewayProxy::UpdateClientStatus(ClientInfo* info)
{
	int ret=0;

	MsgExpress::AppInfo appInfo;
	MsgExpress::LoginInfo* loginInfo=new MsgExpress::LoginInfo();
	appInfo.set_allocated_logininfo(loginInfo);
	appInfo.set_addr(info->address);
	appInfo.set_token(info->token);
	appInfo.set_status(info->status);
	appInfo.set_logintime(info->userid);//logintime用于存储userid
	string data=appInfo.SerializeAsString();
	char key[24];
	sprintf(key,"%d",info->address);

	std::unique_lock<std::mutex> lock(mMutex);
	string errstr;
	//ret=MemCache::Set(key,data,&errstr);
	ret=LocalCache::Set(key,data,&errstr);
	if(ret!=0)
	{
		LOG4_ERROR("Set storage failed,addr=%d,err:%s",info->address,errstr.c_str());
	}
	return ret==0;
}

void GatewayProxy::OnRawPackageA(PackagePtr package)
{
	string clazz = CommandConfig::getInstance().GetClass(package->GetCommand());
	int service = CommandConfig::getInstance().GetServiceId(clazz.c_str());
	package->set_classtype(clazz);
	if(package->IsResponse())
	{
		if (service == Security::APPID)
		{
			MessagePtr msg = m_pServerB->CreateMessage(clazz);
			if(msg)
			{
				package->set_message(msg);
			}
			if(clazz==Security::ResSessionValid::default_instance().GetTypeName())
			{
				AppServerImpl::MsgCallbackPtr cbInfo=m_pServerA->getCallbackInfo(package->GetSerialNum());
				if(cbInfo==NULL)
				{
					LOG4_ERROR("Cant find callback info,content:%s",package->DebugString().c_str());
					return;
				}
				if(!MessageUtil::parse(package.get()))
				{
					LOG4_ERROR("Parse failed,cmd:0x%x,class name:%s",package->GetCommand(),package->classtype().c_str());
					return;
				}
				cbInfo->params.result=0;
				cbInfo->params.resp=package;
				cbInfo->cb(cbInfo->params);
				//delete cbInfo;
				return;
			}
		}
	}
	else if(package->IsRequest())
	{
		MessagePtr msg = m_pServerB->CreateMessage(clazz);
		if(msg)
		{
			package->set_message(msg);
		}
		if (package->classtype() == Gateway::Subscribe::default_instance().GetTypeName())
		{
			bool ret=MessageUtil::parse(package.get());
			if(!ret)
			{
				LOG4_ERROR("Parse failed,cmd:0x%x,class name:%s", package->GetCommand(), package->classtype().c_str());
				m_pServerA->Reply(package,Gateway::ERRCODE_PARSEFAILED,Gateway::ERRMSG_PARSEFAILED);
				return;
			}
			Gateway::Subscribe* sub=(Gateway::Subscribe*)package->message().get();
			string token=sub->token();
			string strData=sub->subdata();
			MsgExpress::SubscribeData *subData=new MsgExpress::SubscribeData();
			if(!subData->ParseFromArray(strData.data(),strData.size()))
			{
				LOG4_ERROR("Parse data failed,cmd:0x%x,class name:%s",package->GetCommand(),package->classtype().c_str());
				m_pServerA->Reply(package,Gateway::ERRCODE_PARSEFAILED,Gateway::ERRMSG_PARSEFAILED);
				return;
			}
			std::unique_lock<std::mutex> lock(mMutex);
			hash_map<std::string,unsigned int>::iterator it=mapToken2Addr.find(token);
			if(it==mapToken2Addr.end())
			{
				//表示这个客户端不在本gateway上
				LOG4_INFO("Subscription to gateway ignore,%s,token=%s",Gateway::ERRMSG_NOADDRESS,token.c_str());
				//m_pServerA->Reply(package,Gateway::ERRCODE_NOADDRESS,Gateway::ERRMSG_NOADDRESS);
				return;
			}
			lock.unlock();
			subData->set_useraddr(it->second);
			LOG4_INFO("Sub,token=%s,addr=%d,topic=0X%x",token.c_str(),it->second,subData->topic());
			tagArg * arg=new tagArg();
			arg->package=package;
			arg->token=token;
			function<void(const MsgParams&)> msgcb=bind(&GatewayProxy::SubscriptionCallback,this,_1);
			if(!m_pServerB->PostMessage(MessagePtr(subData),msgcb,arg,3000))
			{
				LOG4_ERROR("Subscription to gateway failed,token=%s",token.c_str());
			}
		}
		return;
	}
	else if(package->IsPublish())
	{
		if(package->GetCommand()==MsgExpress::TOPIC_TEST/* || package->GetCommand()==11534340*/)
		{
			package->set_classtype(MsgExpress::PublishData::default_instance().GetTypeName());
			bool ret=MessageUtil::parse(package.get());
			if(ret)
			{
				PublishHelperPtr helper=CreatePublishHelper((MsgExpress::PublishData*)package->message().get(),true);
				MsgExpress::DataItem* item=helper->GetItem(MsgExpress::KEY_TIME);
				if(item && item->ulval_size()>0)
				{
					uint64_t tm=item->ulval(0);
					uint64_t brokertime=m_pServerA->GetBrokerTime();
					long off=brokertime-tm;
					//if(off>50)
						LOG4_WARN("Receive a test publish cost %d ms,src serial:%d,content:%s",off,package->GetSerialNum(),package->DebugString().c_str());
					MsgExpress::PublishData *pubData =new MsgExpress::PublishData(*(MsgExpress::PublishData*)package->message().get());
					PublishHelperPtr helper2=CreatePublishHelper(pubData);
					uint64_t now = gettimepoint();
					helper2->AddUInt64(MsgExpress::KEY_TIME+1,now);
					helper2->AddString(88888,"hello");
					//LOG4_WARN("%s",pubData->ShortDebugString().c_str());
					string data;
					PackageHeader header(package->header());
					if(MessageUtil::serializePackageToString(header,*pubData,false,30,&data))
					{
						Package* pack=new Package(header,MsgExpress::PublishData::default_instance().GetTypeName(),data.data(),data.size(),MessagePtr(pubData));
						MsgExpressProxy::OnRawPackageA(PackagePtr(pack));
						return;
					}
				}
			}
		}
	}
	MsgExpressProxy::OnRawPackageA(package);
}
void GatewayProxy::SubscriptionCallback(const MsgParams& params)
{
	tagArg * arg=(tagArg *)params.arg;
	if(params.result==0)
	{
		LOG4_INFO("Subscription success,token=%s,content:%s",arg->token.c_str(),arg->package->DebugString().c_str());
	    Gateway::SubscribeResult result;
		result.set_ret(true);
		result.set_msg("");
		m_pServerA->Reply(arg->package,result);
	}
	else
	{
		LOG4_ERROR("Subscription failed,token=%s,ErrCode:%d,ErrMsg:%s,content:%s",arg->token.c_str(),params.result,params.errmsg.c_str(),arg->package->DebugString().c_str());
		Gateway::SubscribeResult result;
		result.set_ret(false);
		result.set_msg(params.errmsg);
		m_pServerA->Reply(arg->package,result);
	}
	delete arg;
}
void GatewayProxy::ProcessMyMessage(PackagePtr package)
{
	string clazz=CommandConfig::getInstance().GetClass(package->GetCommand());
	MessagePtr msg = m_pServerB->CreateMessage(clazz);
	if(msg==NULL)
	{
		LOG4_ERROR("Failed to create instance,cmd:0x%x,class name:%s",package->GetCommand(),clazz.c_str());
		return;
	}
	package->set_classtype(clazz);
	package->set_message(msg);
	if(!MessageUtil::parse(package.get()))
	{
		LOG4_ERROR("Parse failed,cmd:0x%x,class name:%s", package->GetCommand(), package->classtype().c_str());
		m_pServerB->Reply(package,Gateway::ERRCODE_PARSEFAILED,Gateway::ERRMSG_PARSEFAILED);
		return;
	}
	else if (package->classtype() == Gateway::Login::default_instance().GetTypeName())
	{
		unsigned int addr=package->GetSrcAddr();
		ClientInfo* appInfo=GetClientInfo(addr);
		LOG4_INFO("Client Login to gatway,session:%s,addr=%d[0x%x]",((Gateway::Login*)package->message().get())->token().c_str(),addr,addr);
		if(appInfo==NULL)
		{
			LOG4_ERROR("Gateway cant get Client Info from databus,addr=%d[0x%x]",addr,addr);
			m_pServerB->Reply(package,Gateway::ERRCODE_NOCLIENTINFO,Gateway::ERRMSG_NOCLIENTINFO);
			return;
		}
		Gateway::Login* msg=(Gateway::Login*)package->message().get();
		appInfo->token=msg->token();
		//appInfo->authRequest=package; 
		appInfo->status=MsgExpress::Authing;
		Security::ReqSessionValid *sessionCheck=new Security::ReqSessionValid();
		sessionCheck->set_token(msg->token());
		tagArg * arg=new tagArg();
		arg->package=package;
		arg->token=msg->token();
		arg->addr=addr;
		function<void(const MsgParams&)> msgcb=bind(&GatewayProxy::CheckSessionCallback,this,_1);
		if(!m_pServerA->PostMessage(MessagePtr(sessionCheck),msgcb,arg,3000))
		{
			LOG4_ERROR("CheckSession failed,cant post msg to broker,token=%s",msg->token().c_str());
		}
	}
	else if (package->classtype() == Gateway::Logout::default_instance().GetTypeName())
	{
		Gateway::CommonResponse rep;
		rep.set_retcode(0);
		rep.set_msg("");
		m_pServerB->Reply(package,rep);
		unsigned int addr=package->GetSrcAddr();
		ClientInfo* info=NULL;
		std::unique_lock<std::mutex> lock(mMutex);
		hash_map<unsigned int, ClientInfo*>::iterator it=mapAppInfo.find(addr);
		if(it!=mapAppInfo.end())
		{
			info=it->second;
			mapAppInfo.erase(it);
		}
		lock.unlock();
		if(info && info->status==MsgExpress::Authed)
		{
			time_t tm;
	        time(&tm);
			string token=info->token;
			MsgExpress::PublishData* pub=new MsgExpress::PublishData();
			pub->set_topic(Gateway::TOPIC_LOGOUT);
			PublishHelperPtr helper=CreatePublishHelper(pub);
			helper->AddString(Gateway::KEY_TOKEN,token);
			helper->AddUInt64(Gateway::KEY_USERID,info->userid);
			helper->AddDatetime(Gateway::KEY_TIME,tm);
			m_pServerA->Publish(MessagePtr(pub), nullptr, NULL);
			if(token.size()>0)
			{
				std::unique_lock<std::mutex> lock(mMutex);
				hash_map<std::string,unsigned int>::iterator it=mapToken2Addr.find(token);
				if(it!=mapToken2Addr.end())
					mapToken2Addr.erase(it);
			}
		}
		delete info;
		LOG4_INFO("Client logout,addr=%d[0x%x]",addr,addr);
	}
}
void GatewayProxy::TransferMessage(PackagePtr package)
{
	unsigned int addr=package->GetSrcAddr();
	ClientInfo* appInfo=GetClientInfo(addr);
	if(appInfo==NULL)
	{
		LOG4_ERROR("Gateway cant get Client Info from databus,addr=%d[0x%x]",addr,addr);
		m_pServerB->Reply(package,Gateway::ERRCODE_NOCLIENTINFO,Gateway::ERRMSG_NOCLIENTINFO);
		return;
	}
	appInfo->calltimes++;
	/*if(appInfo->status!=MsgExpress::Authed && package->GetAppId()==AccountServer::APPID && appInfo->calltimes>10)
	{
		LOG4_ERROR("Client called services too many times before authenrity,addr=%d[0x%x]",addr,addr);
		m_pServerB->Reply(package,Gateway::ERRCODE_NOAUTHENRITY,Gateway::ERRMSG_NOAUTHENRITY);
		return;
	}*/
	int service = CommandConfig::getInstance().GetServiceId(package->GetCommand());
	bool hasNoAuthed=true;
	if (appInfo->status == MsgExpress::Authed || service == AccountServer::APPID || service == UpdateServer::APPID)
		hasNoAuthed=false;
	if (service >= 950 && service<1000)
		hasNoAuthed=false;
	if(hasNoAuthed)
	{
		LOG4_ERROR("Client has not authenrized to access service,%s,addr=[0x%x]",package->DebugString().c_str(),addr);
		m_pServerB->Reply(package,Gateway::ERRCODE_NOAUTHENRITY,Gateway::ERRMSG_NOAUTHENRITY);
		MsgExpress::KickOffApp kick;
		kick.set_addr(addr);
		m_pServerB->PostMessage(kick);
		return;
	}
	if(appInfo->status==MsgExpress::Authed)
	{
		//attach token.
		string body;
		MsgExpress::TokenInfo tokenInfo;
		tokenInfo.set_token(appInfo->token);
		tokenInfo.set_userid(appInfo->userid);
		if(!MessageUtil::serializeExtMessageToString(tokenInfo,&body))
			return ;
		if(package->header().iszip())
		{
			if(!MessageUtil::unCompress(package->GetBodyData(),package->GetBodySize(),package->header().compratio(),body))
			{
				LOG4_ERROR("Message uncompress failed,msg=%s",package->DebugString().c_str());
				m_pServerB->Reply(package,Gateway::ERRCODE_UNCOMPRESSFAIL,Gateway::ERRMSG_UNCOMPRESSFAIL);
				return;
			}
		}
		else
		{
			body.append(package->GetBodyData(),package->GetBodySize());
		}
		PackageHeader header(package->header());
		header.set_hasext(true);
		if(appInfo->userid>0)
			header.set_code((unsigned char)appInfo->userid);
		else
			header.set_code( (unsigned char)((AppAddress)package->GetSrcAddr()).AddressNo);//
		LOG4_DEBUG("set loadbalance,cmd=%x,Code=%d",package->GetCommand(),header.code());
		string packdata;
		MessageUtil::serializePackageToString(header,body.data(),body.size(),false,0,&packdata);
		PackagePtr pack(new Package(header, package->classtype(), packdata.data(), packdata.size(), MessagePtr()));
		MsgExpressProxy::OnRawPackageB(pack);
		return;
	}
	else
	{
		LOG4_DEBUG("set loadbalance,cmd=%x,Addr=%d",package->GetCommand(),package->GetSrcAddr());
		if (package->header().offset() >= PackageHeaderLength)
		{
		    package->SetCode(package->GetSrcAddr());
		}
		MsgExpressProxy::OnRawPackageB(package);
	}
}
void GatewayProxy::ProcessPublish(PackagePtr package)
{
	string clazz=CommandConfig::getInstance().GetClass(package->GetCommand());
	MessagePtr msg = m_pServerB->CreateMessage(clazz);
	if(msg==NULL)
	{
		LOG4_ERROR("Failed to create instance,cmd:0x%x,class name:%s",package->GetCommand(),clazz.c_str());
		return;
	}
	package->set_classtype(clazz);
	package->set_message(msg);
	bool ret=MessageUtil::parse(package.get());
	if(!ret)
	{
		LOG4_ERROR("Parse failed,cmd:0x%x,class name:%s", package->GetCommand(), package->classtype().c_str());
		return;
	}
	MsgExpress::PublishData* pubData=(MsgExpress::PublishData*)package->message().get();
	if(pubData->topic()==MsgExpress::TOPIC_LOGIN)
	{
		PublishHelperPtr helper=CreatePublishHelper(pubData,true);
		const MsgExpress::DataItem* item=helper->GetItem(MsgExpress::KEY_ADDR);
		unsigned int addr=0;
		if(item && item->uival_size()>0)
		    addr=item->uival(0);
		else
			LOG4_ERROR("TOPIC_LOGIN cant find address");
		LOG4_INFO("Client connected,addr=%d[0x%x]",addr,addr);
	}
	else if(pubData->topic()==MsgExpress::TOPIC_LOGOUT)
	{
		PublishHelperPtr helper=CreatePublishHelper(pubData,true);
		const MsgExpress::DataItem* item=helper->GetItem(MsgExpress::KEY_ADDR);
		unsigned int addr=0;
		if(item && item->uival_size()>0)
		    addr=item->uival(0);
		else
			LOG4_ERROR("TOPIC_LOGOUT cant find address");
		ClientInfo* info=NULL;
		std::unique_lock<std::mutex> lock(mMutex);
		hash_map<unsigned int, ClientInfo*>::iterator it=mapAppInfo.find(addr);;
		if(it!=mapAppInfo.end())
		{
			info=it->second;
			mapAppInfo.erase(it);
		}
		lock.unlock();
		if(info && info->status==MsgExpress::Authed)
		{
			time_t tm;
	        time(&tm);
			string token=info->token;
			MsgExpress::PublishData* pub=new MsgExpress::PublishData();
			pub->set_topic(Gateway::TOPIC_OFFLINE);
			PublishHelperPtr helper2=CreatePublishHelper(pub);
			helper2->AddString(Gateway::KEY_TOKEN,token);
			helper2->AddUInt64(Gateway::KEY_USERID,info->userid);
			helper2->AddDatetime(Gateway::KEY_TIME,tm);
			LOG4_INFO("Client offline,userid=%lld,token=%s",info->userid,token.c_str());
			m_pServerA->Publish(MessagePtr(pub), nullptr, NULL);
			if(token.size()>0)
			{
				std::unique_lock<std::mutex> lock(mMutex);
				hash_map<std::string,unsigned int>::iterator it=mapToken2Addr.find(token);
				if(it!=mapToken2Addr.end())
					mapToken2Addr.erase(it);
			}
		}
		else if(info){
		    LOG4_INFO("Client offline,addr=%d[0x%x],ip=%s",addr,addr,info->ip.c_str());
		}
		else
			LOG4_INFO("Client offline,addr=%d[0x%x]",addr,addr);
		delete info;
	}
	else if(pubData->topic()==MsgExpress::TOPIC_HEARTBEAT)
	{
		PublishHelperPtr helper=CreatePublishHelper(pubData,true);
		const MsgExpress::DataItem* item=helper->GetItem(MsgExpress::KEY_ADDR);
		unsigned int addr=0;
		if(item && item->uival_size()>0)
		    addr=item->uival(0);
		else
			LOG4_ERROR("TOPIC_HEARTBEAT cant find address");
		if(addr==m_pServerB->GetAddress())
			return;
		LOG4_DEBUG("Client heartbeat,addr=%d[0x%x]",addr,addr);
		ClientInfo* info=GetClientInfo(addr);
		if(info && info->status==MsgExpress::Authed)
		{
			string token=info->token;
			MsgExpress::PublishData* pub=new MsgExpress::PublishData();
			pub->set_topic(Gateway::TOPIC_HEARTBEAT);
			MsgExpress::DataItem* item=pub->add_item();
			item->set_key(Gateway::KEY_TOKEN);
			item->add_strval(token);
			item=pub->add_item();
			item->set_key(Gateway::KEY_USERID);
			item->add_ulval(info->userid);
			m_pServerA->Publish(MessagePtr(pub), nullptr, NULL);
		}
	}
}
void GatewayProxy::CheckSessionCallback(const MsgParams& params)
{
	tagArg * arg=(tagArg *)params.arg;
	if(params.result==0)
	{
		unsigned int addr=arg->addr;
		LOG4_INFO("Session check reply to gatway,addr=%d[0x%x]",addr,addr);
		ClientInfo* appInfo=GetClientInfo(arg->addr);
		if(appInfo==NULL)
		{
			LOG4_ERROR("Gateway cant get Client Info from databus,addr=%d[0x%x]",addr,addr);
		}
		else if(appInfo->status==MsgExpress::Authing)
		{
			if(!MessageUtil::parse(params.resp.get()))
			{
				LOG4_ERROR("Parse failed,cmd:0x%x,class name:%s", params.resp->GetCommand(), params.resp->classtype().c_str());
			}
			else
			{
				if(((Security::ResSessionValid*)params.resp->message().get())->result()==0)
				{
					Gateway::CommonResponse reply;
					reply.set_retcode(0);
					reply.set_msg("login succeed");
					m_pServerB->Reply(arg->package,reply);
					appInfo->userid=((Security::ResSessionValid*)params.resp->message().get())->userid();
					appInfo->status=MsgExpress::Authed;

					std::unique_lock<std::mutex> lock(mMutex);
					mapToken2Addr[appInfo->token]=addr;
					lock.unlock();

					time_t tm;
	                time(&tm);
					MsgExpress::PublishData* pub=new MsgExpress::PublishData();
					pub->set_topic(Gateway::TOPIC_LOGIN);
					PublishHelperPtr helper=CreatePublishHelper(pub);
					helper->AddString(Gateway::KEY_TOKEN,appInfo->token);
					helper->AddUInt64(Gateway::KEY_USERID,appInfo->userid);
					helper->AddDatetime(Gateway::KEY_TIME,tm);
					m_pServerA->Publish(MessagePtr(pub),nullptr,NULL);
					if(!UpdateClientStatus(appInfo))
					{
						LOG4_ERROR("Update status failed,addr=%d[0x%x],status=%d",addr,addr,appInfo->status);
					}
					LOG4_INFO("Session check succeed,addr=%d[0x%x],userid=%lld",addr,addr,appInfo->userid);
					LOG4_INFO("Session check succeed,addr=%d[0x%x],token=%s",addr,addr,appInfo->token.c_str());
				}
				else
				{
					Gateway::CommonResponse reply;
					reply.set_retcode(1);
					reply.set_msg(((Security::ResSessionValid*)params.resp->message().get())->error_desc());
					m_pServerB->Reply(arg->package,reply);
					LOG4_ERROR("Session check failed,errmsg=%s,addr=%d[0x%x]",((Security::ResSessionValid*)params.resp->message().get())->error_desc().c_str(),addr,addr);
				}
			}
		}
	}
	else
	{
		Gateway::CommonResponse reply;
		reply.set_retcode(params.result);
		reply.set_msg(params.errmsg.c_str());
		m_pServerB->Reply(arg->package,reply);
		LOG4_ERROR("CheckSession failed,token=%s,ErrCode:%d,ErrMsg:%s",arg->token.c_str(),params.result,params.errmsg.c_str());
	}
	
	delete arg;
}
void GatewayProxy::OnRawPackageB(PackagePtr package)
{
	string clazz = CommandConfig::getInstance().GetClass(package->GetCommand());
	int service = CommandConfig::getInstance().GetServiceId(package->GetCommand());
	if(package->IsResponse())
	{
		if(clazz!=MsgExpress::CommonResponse::default_instance().GetTypeName())
		{
			LOG4_ERROR("Response from side B,content:%s",package->DebugString().c_str());
		    return;
		}
		AppServerImpl::MsgCallbackPtr cbInfo=m_pServerB->getCallbackInfo(package->GetSerialNum());
		if(cbInfo==NULL)
		{
			LOG4_ERROR("Cant find callback info,content:%s",package->DebugString().c_str());
		    return;
		}
		
		MessagePtr msg = m_pServerB->CreateMessage(clazz);
		if(msg==NULL)
		{
			LOG4_ERROR("Failed to create instance,cmd:0x%x,class name:%s",package->GetCommand(),clazz.c_str());
			return;
		}
		package->set_classtype(clazz);
		package->set_message(msg);
		if(!MessageUtil::parse(package.get()))
		{
			LOG4_ERROR("Parse failed,cmd:0x%x,class name:%s", package->GetCommand(), package->classtype().c_str());
			return;
		}
		cbInfo->params.result=0;
		cbInfo->params.resp=package;
		cbInfo->cb(cbInfo->params);
		//delete cbInfo;
	}
	else if(package->IsRequest())
	{
		if(!m_pServerA->IsLogin())
		{
			m_pServerB->Reply(package,Gateway::ERRCODE_NOLOGIN,Gateway::ERRMSG_NOLOGIN);
			return;
		}
		m_pServerA->ClearLatestError();
		if (service == (unsigned int)Gateway::APPID)
		{
			ProcessMyMessage(package);
			return;
		}
		else
		{
			TransferMessage(package);
		}
	}
	else if(package->IsPublish())
	{
		if(service==(unsigned int)MsgExpress::APPID)
		{
			ProcessPublish(package);
		}
		//else if(package->header().topic>=MsgExpress::TOPIC_SPECIAL_START && package->header().topic<MsgExpress::TOPIC_SPECIAL_END)
		//	MsgExpressProxy::OnRawPackageA(package);
		return;//B端不向A端推送东西
	}
}
void GatewayProxy::OnEventA(int eventId)
{
	if(eventId==Online)
	{
	    LOG4_INFO("GatewayProxy side A online,status:%d",eventId);
	}
	else if(eventId==Offline)
	    LOG4_ERROR("GatewayProxy side A offline,status:%d",eventId);
	MsgExpress::PublishData* pub=new MsgExpress::PublishData();
	pub->set_topic(Gateway::TOPIC_NETWORKSTATE);
	MsgExpress::DataItem* item=pub->add_item();
	item->set_key(Gateway::KEY_STATE);
	item->add_ival(eventId);
	m_pServerB->Publish(MessagePtr(pub), nullptr, NULL);
}
void GatewayProxy::OnEventB(int eventId)
{
	if(eventId==Online)
	{
	    LOG4_INFO("GatewayProxy side B online,status:%d",eventId);
	}
	else if(eventId==Offline)
	{
	    LOG4_ERROR("GatewayProxy side B offline,status:%d",eventId);
		LOG4_INFO("Begin clean client info,total=%d",mapAppInfo.size());
		ClientInfo* info=NULL;
		std::unique_lock<std::mutex> lock(mMutex);
		hash_map<unsigned int, ClientInfo*>::iterator it=mapAppInfo.begin();
	    while(it!=mapAppInfo.end())//
	    {
		    info=it->second;
			delete info;
			it++;
		}
		mapAppInfo.clear();
		LOG4_INFO("End clean client info,total=%d",mapAppInfo.size());
	}
}
