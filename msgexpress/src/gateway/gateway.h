#pragma once

#include <hash_map>
#include "../inc/msgexpress.h"
#include "../proxy/proxy.h"
#ifdef _WIN32
//#include "redis/redisapi.h"
#else
//#include "../redis/redisapi.h"
#endif
#include "../client/cacheapi.h"

struct ClientInfo
{
	ClientInfo()
	{
		status=MsgExpress::Connected;
		userid=0;
		calltimes=0;
	}
	~ClientInfo()
	{
	}
	unsigned int address;
	string token;
	uint64_t userid;
	string ip;
	time_t loginTime;
	MsgExpress::AppStatus status;
	int calltimes;
	//PackagePtr authRequest;
};

class MSGEXPRESS_API GatewayProxy : public MsgExpressProxy
{
	struct AuthData
	{
		unsigned int serialNum;
	};

private:
	//hash_map<unsigned int,unsigned int> mapSerial2Addr;
	hash_map<unsigned int, ClientInfo*> mapAppInfo;
	hash_map<std::string,unsigned int> mapToken2Addr;
	std::mutex mMutex;
	int dataset;
private:
	ClientInfo* GetClientInfo(unsigned int addr);
	bool UpdateClientStatus(ClientInfo* info);
	void ProcessMyMessage(PackagePtr package);
	void TransferMessage(PackagePtr package);
	void ProcessPublish(PackagePtr package);
	void SubscriptionCallback(const MsgParams& params);
	void CheckSessionCallback(const MsgParams& params);
public:
	GatewayProxy();
	virtual ~GatewayProxy();
	virtual void Init(const ProxyClientConfig &pclientconf);
protected:
	virtual void OnRawPackageA(PackagePtr package);
	virtual void OnEventA(int eventId);

	virtual void OnRawPackageB(PackagePtr package);
	virtual void OnEventB(int eventId);
};

