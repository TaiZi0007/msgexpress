#include "appserver.h" 
#include <stdio.h> 
#include "../inc/logger.h"
#include "../core/messageutil.h"

AppServer::AppServer(){ 
}
AppServer::~AppServer(){ 
}
void AppServer::Stop()
{
	qData.push(NULL);
}
void AppServer::OnRawPackage(PackagePtr package)
{
	MessageItem* item=new MessageItem();
	item->header=package->header();
	const char* bodyData=package->GetBodyData();
	unsigned int bodySize=package->GetBodySize();
	if(package->header().iszip())
	{
		//printf("upzip\r\n");
		bool unzipOK = MessageUtil::unCompress(bodyData,bodySize,package->header().compratio(),item->body);
		if(!unzipOK)
		{
			LOG4_ERROR("Unzip failed,%s",package->DebugString().c_str());
			return;
		}
	}
	else
		item->body.assign(bodyData,bodySize);
	qData.push(item);
}
void AppServer::OnEvent(int eventId)
{
	lstEvent.push_back(eventId);
}
MessageItem* AppServer::GetItem()
{
	MessageItem* item=(MessageItem*)qData.pop();
	return item;
}
