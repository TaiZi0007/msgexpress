#pragma once

#include "../core/spthread.h"
#include "../inc/protobuf.h"
#include "../inc/msgexpress.h"
#include "../client/client_impl.h"
#include <list>
#include <mutex>
#include "jni.h"


using namespace google::protobuf;

struct MessageItem
{
	PackageHeader header;
	string body;
};
class AppServer:public AppServerImpl
{
private:
	std::mutex mtx;
	std::list<int> lstEvent;
	SafeQueue qData;
public:
	AppServer();
	virtual ~AppServer();
private:

protected:
	virtual void OnRawPackage(PackagePtr package);
	virtual void OnEvent(int eventId);
public:
	void Stop();
	MessageItem* GetItem();
};

