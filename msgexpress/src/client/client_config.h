#pragma once

#include <string>
#include "../inc/service_config.h"
#include "../core/xmlconfig.h"

using namespace std;

class  ClientConfig:public XMLConfig
{
public:
    ClientConfig();
public:
    ~ClientConfig(){};

    virtual bool ReadConfigFile();
public:
	bool ReadServerCFG(TiXmlElement *root,ClientCFG &clientCfg);
private:
    const char* GetNodeValue(string name,TiXmlElement *parent);
public:
    ClientCFG clientCfg;
};
