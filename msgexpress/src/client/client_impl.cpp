
#include <time.h>
#include <string.h>
#include <signal.h>
#include "client_impl.h"
#include "../inc/logger.h"
#include "../core/messageutil.h"
#include "../core/command_config.h"
#include "client_config.h"
#include "publishdata.h"
#include "../core/codebase64.h"
#include "../core/porting.h"
#include "../core/ioutils.h"
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/prctl.h>
#endif


#ifdef _WIN32
#pragma warning(disable:4996)
#endif

using namespace google::protobuf;

#define SETLASTERROR(err) \
	MsgExpress::ErrMessage* errmsg=GetErrorMsg(); \
	errmsg->set_errcode(MsgExpress::ERRCODE_##err); \
	errmsg->set_errmsg(MsgExpress::ERRMSG_##err);

const int DEFAULT_RECV_BUF_SIZE = 4096; 
const int AUTH_METHOD_BASIC = 1;    
const int AUTH_METHOD_DIGEST = 10;  
const int AUTH_METHOD_NEGOTIATE = 3;
const int AUTH_METHOD_NTLM = 4;
const int AUTH_METHOD_KERBEROS = 5; 
const int AUTH_METHOD_UNKNOWN = 30;
const int MAX_CONNECT_ATTEMPT = 10;


int AppServerImpl::identity=-1;

AppServerImpl::AppServerImpl()
{
	inited=false;
	isRun=false;
	state=STATE::OFFLINE;
#ifdef __linux__
	needCreatEP=false;
	epfd=-1;
#endif
	sendMsgQueue = nullptr;
	decoder = nullptr;

	logServer = nullptr;
	m_pSyncMsgArr = new SyncMessageArray();
	mMaxThreads=4;
	onCreateMessage = nullptr;
	onMessage=nullptr;
	onPublish=nullptr;
	onEvent=nullptr;
	serverMode = -1;
	address=0;
	serverIndex=0;
	m_iHeartBeatTick = 0;
	m_iTryConnectTick = 0;
	m_iTryConnectInterval = 30;
	minoff=99999;
    timeoff=0;
	heartbeatrunning=false;
	time(&startTime);
	
	if(identity<0)
	{
		srand((unsigned int)startTime);
		identity=rand() % 100;
	}
	id=identity;
	identity++;

	sp_thread_semaphore_init(&timeoutSem,1,10);

	databuff = evbuffer_new();

	LOG4_INFO("Begin event base work");
	const char ** methods = event_get_supported_methods();
	struct event_config *pCfg = event_config_new();
	event_config_avoid_method(pCfg, "evport");
	event_config_avoid_method(pCfg, "kqueue");
	// 分配并初始化event_base 
	base = event_base_new_with_config(pCfg);
	if (!base) {
		LOG4_ERROR("Could not initialize libevent!");
	}
	else
	    int ret = evthread_make_base_notifiable(base);
}
AppServerImpl::~AppServerImpl()
{
	evbuffer_free(databuff);
	m_pSyncMsgArr->Clear();
	Release();

	SetLogCallback(nullptr, 0);

	mMapAsyncInfo.clear();
	delete m_pSyncMsgArr;
	m_pSyncMsgArr=NULL;
	if(inited)
	{
	    while(sendMsgQueue && sendMsgQueue->getLength()>0)
		    delete (SendDataItem*)sendMsgQueue->pop();
	    delete sendMsgQueue;
	    sendMsgQueue=NULL;
	    delete decoder;
	    decoder=NULL;
	}

	if(logServer)
	{
		logServer->Release();
		delete logServer;
		logServer=NULL;
	}
}

void AppServerImpl::LogCallback(string level,string log)
{
	if(!logServer)
		return;
	static time_t lasttime=0;
	time_t tm;
	time(&tm);
	if(tm-lasttime==0)
		return;
	lasttime=tm;

	MsgExpress::PublishData* pub=new MsgExpress::PublishData();
	pub->set_topic(MsgExpress::TOPIC_LOG);
	PublishHelperPtr helper=CreatePublishHelper(pub);
	helper->AddString(MsgExpress::KEY_LOGLEVEL,level);
	helper->AddString(MsgExpress::KEY_LOGDATA,log);
	helper->AddString(MsgExpress::KEY_UUID,clientCfg.uuid);
	helper->AddString(MsgExpress::KEY_NAME,clientCfg.appName);
	helper->AddString(MsgExpress::KEY_IP,clientCfg.localIp);
	helper->AddUInt32(MsgExpress::KEY_ADDR,this->GetAddress());
	helper->AddDatetime(MsgExpress::KEY_TIME,tm);
	if(logServer)
		logServer->Publish(MessagePtr(pub), nullptr, NULL);
}
int  AppServerImpl::Initialize(const char* configPath,ProxyCFG* proxyCfg)
{
	/*string xml_file = "Command.xml";
	CommandConfig &cconf=CommandConfig::getInstance();
	if(!cconf.LoadFile(xml_file))
	{   
		LOG4_ERROR("read client command config file error");
		return -1;
	}  */
	if(configPath)
	    XMLConfig::SetPath(configPath);
	string xml_file = "clientcfg.xml";
	ClientConfig sconf;
	if(!sconf.LoadFile(xml_file))
	{   
		LOG4_ERROR("read client config file error");
		return 0;
	}  
	
	return Initialize(&sconf.clientCfg,proxyCfg);
}
int  AppServerImpl::Initialize(const wchar_t* configPath,ProxyCFG* proxyCfg)
{
	if(configPath)
	{
#ifdef _WIN32
	    char chPath[MAX_PATH];
	    WideCharToMultiByte(CP_ACP, 0, configPath, -1, chPath, MAX_PATH, NULL, NULL );   
	    return Initialize(chPath,proxyCfg);
#else
		return Initialize();
#endif
	}
	return Initialize();
}
int AppServerImpl::Initialize(ClientCFG* pClientCfg,ProxyCFG* pProxyCfg)
{
	if(inited)
	{
		LOG4_ERROR("id=%d,Has initailized",id);
		return 0;
	}
	/*string xml_file = "Command.xml";
	CommandConfig &cconf=CommandConfig::getInstance();
	if(!cconf.LoadFile(xml_file))
	{   
		LOG4_ERROR("read client command config file error");
		return -1;
	}  */
	if(pClientCfg && pClientCfg->reportlog)
	{
		string xml_file = "logcfg.xml";
	    ClientConfig sconf;
		if(!sconf.LoadFile(xml_file))
		{   
			LOG4_ERROR("read log config file error");
			//return -1;
		}  
		else
		{
			logServer=new AppServerImpl();
	        logServer->Initialize(&sconf.clientCfg,0);
		    SetLogCallback(bind(&AppServerImpl::LogCallback,this,_1,_2),pClientCfg->reportlevel);
			LOG4_WARN("Set log callback.");
		}
	}
	if(pClientCfg)
		SetLevel(pClientCfg->loglevel);
	inited=true;
	LOG4_INFO("id=%d,Initailize,libVersion=%s",id,LIBVERSION);
	if(pClientCfg==NULL)
	{
		LOG4_ERROR( "Client config is null." );
		return -1;
	}
	clientCfg=(*pClientCfg);
	size_t size = evbuffer_get_length(databuff);
	if(size>0)
	{
		evbuffer_drain(databuff, size);
	    LOG4_WARN("id=%d,Reset buffer,size:%d",id,size);
	}
	sendMsgQueue=new SafeQueue(clientCfg.sendqueuesize);
	decoder=new MsgDecoder(clientCfg.receivequeuesize);

#ifdef __linux__
	//pgmSend=new PgmSend();
	//pgmRecv=new PgmRecv();
#endif
    isRun=true;
	if(pClientCfg->serverGroup.size()>0)
	    LOG4_INFO("id=%d,client connect to Data Bus Server(%s:%d)",id,pClientCfg->serverGroup[0].serverIP.c_str(),pClientCfg->serverGroup[0].port);
	int ret = Connect(pClientCfg,pProxyCfg);
	if(ret != 0)
	{
		LOG4_ERROR("id=%d,client connect to Data Bus Server fail",id);
	}
	else
	{
		LOG4_INFO("id=%d,client connect to Data Bus Server success",id);
	}
	LOG4_INFO("id=%d,Sequence queue: 0X%x,none sequence queue:0X%x",id,decoder->getSequenceQueue(),decoder->getNormalQueue());
	ret = InitThread();
	if(ret != 0)
	{
		LOG4_ERROR("init thread error");
		return -1;
	}
	if(IsOnLine() && clientCfg.udpReceiverCfg.isValid==false && clientCfg.udpSenderCfg.isValid==false)
	{
		ret = Login();
		if(ret != 0)
		{
			Close();
			LOG4_ERROR("id=%d,client login error",id);
		}
	}
	//启动心跳线程
	if(clientCfg.heartbeatSwitch == 1  && clientCfg.udpReceiverCfg.isValid==false && clientCfg.udpSenderCfg.isValid==false)
	{
		sp_thread_t thread;
		ret = sp_thread_create( &thread, NULL, heartBeatThread, this );
		if( 0 == ret ) {
			LOG4_INFO( "id=%d,Thread #%llx has been created to send heart beat",id, thread );
		} else {
			LOG4_ERROR( "id=%d,Unable to create a thread to send heart beat, %d",id, errno );
			return -1;
		}
	}

	this->ReportServerEvent(3, "Initailize,libVersion=" + string(LIBVERSION));
    return 0;	
}
bool AppServerImpl::IsLogin()
{
	return state==STATE::LOGINED;
}
SOCKET AppServerImpl::ConnectServer(const char* strAddr, unsigned short nHostPort)
{
#ifdef _WIN32
	struct sockaddr_in sin;
	memset( &sin, 0, sizeof(sin) );
	hostent *pHost = gethostbyname(strAddr);
	if(pHost==NULL || pHost->h_length==0)
		return INVALID_SOCKET;
	
	sin.sin_family = AF_INET;	
	sin.sin_addr.s_addr = *((DWORD*)pHost->h_addr_list[0]) ;
	sin.sin_port = htons( nHostPort );
	LOG4_INFO("id=%d,connect Server,IP: %s,Port: %d",id,strAddr,nHostPort);

	SOCKET sock = socket( AF_INET, SOCK_STREAM, 0 );
	if( sock < 0 ) {
		LOG4_ERROR( "socket failed, errno %d", errno );
		return INVALID_SOCKET;
	}

	const char chOpt=1;   
    int   nErr=setsockopt(  sock,   IPPROTO_TCP,   TCP_NODELAY,   &chOpt,   sizeof(char));   
    if(nErr==-1)   
    {   
         LOG4_ERROR("setsockopt()   error%d",WSAGetLastError());   
         return INVALID_SOCKET;   
    }  
	// set send buf size
	int snd_size = clientCfg.buffersize;
	int optlen = sizeof(snd_size); 
	int err = setsockopt(sock, SOL_SOCKET, SO_SNDBUF, (const char*)&snd_size, optlen); 
	if(err<0){ 
			printf("set send buf size error\n"); 
	} 
	//set recv buf size
	int rcv_size = clientCfg.buffersize;
	optlen = sizeof(rcv_size); 
	err = setsockopt(sock,SOL_SOCKET,SO_RCVBUF, (char *)&rcv_size, optlen); 
	if(err<0){ 
			printf("set recv buf size error\n"); 
	} 

	u_long iMode = 1;  
	ioctlsocket(sock, FIONBIO, &iMode); 

	int ret = -1;  
	if (-1 != connect(sock, (struct sockaddr *)&sin, sizeof(sin)))
	{
		LOG4_INFO("Direct Connect Success!");
		ret = 1;
	}
	else
	{
		fd_set set;  
		FD_ZERO(&set);  
		FD_SET(sock, &set);

		struct timeval tm;  
		tm.tv_sec  = 5;  
		tm.tv_usec = 0;  
		if (select(-1, NULL, &set, NULL, &tm) <= 0)  
		{  
			LOG4_ERROR("select failed!");
			ret = -1; // 有错误(select错误或者超时)  
		}
		else
		{
			int error  = -1;  
			int optLen = sizeof(int);  
			getsockopt(sock, SOL_SOCKET, SO_ERROR, (char*)&error, &optLen);

			LOG4_INFO("id=%d,error = %d",id, error);
			ret = (0 == error) ? 1 : -1;
		}
	}

	iMode = 0;
	ioctlsocket(sock, FIONBIO, &iMode); //设置为阻塞模式  
	IOUtils::setTimeout(sock, 3, 3);

	if (1 != ret)
	{
		LOG4_ERROR( "id=%d,connect to %s:%d failed, errno %d, %s", id ,strAddr, nHostPort,errno, strerror( errno ) );
		CloseSocket(sock);
		return INVALID_SOCKET;
	}

	return sock;
#else
	struct sockaddr_in sin;
	memset( &sin, 0, sizeof(sin) );
	sin.sin_family = AF_INET;	
	sin.sin_addr.s_addr = inet_addr(strAddr );
	sin.sin_port = htons( nHostPort );
	LOG4_INFO("id=%d,connect Server,IP: %s,Port: %d",id,strAddr,nHostPort);

	SOCKET sock = socket( AF_INET, SOCK_STREAM, 0 );
	if( sock <= 0 ) {
		LOG4_ERROR( "socket failed, errno %d", errno );
		return INVALID_SOCKET;
	}
    // set send buf size
	int snd_size = 1024*1024;    
	socklen_t optlen = sizeof(snd_size); 
	int err = setsockopt(sock, SOL_SOCKET, SO_SNDBUF, &snd_size, optlen); 
	if(err<0){ 
			printf("set send buf size error\n"); 
	} 
	//set recv buf size
	int rcv_size = 1024*1024;    
	optlen = sizeof(rcv_size); 
	err = setsockopt(sock,SOL_SOCKET,SO_RCVBUF, (char *)&rcv_size, optlen); 
	if(err<0){ 
			printf("set recv buf size error\n"); 
	} 
	if( connect( sock, (struct sockaddr *)&sin, sizeof(sin) ) != 0) {
			LOG4_ERROR( "id=%d,connect  to %s:%d failed, errno %d, %s",id,strAddr, nHostPort, errno, strerror( errno ) );
			CloseSocket(sock);
			return INVALID_SOCKET;
	}
	return sock;
#endif
}

int AppServerImpl::Connect(ClientCFG* pClientCfg,ProxyCFG* pProxyCfg)
{
	if(pProxyCfg!=NULL)
		proxyCfg=*pProxyCfg;
	
#ifdef _WIN32
	WSADATA wsaData;
	int err = WSAStartup( MAKEWORD( 2, 0 ), &wsaData );
	if ( err != 0 ) {
		LOG4_ERROR( "Couldn't find a useable winsock.dll." );
		return -1;
	}

	hIocp = CreateIoCompletionPort( INVALID_HANDLE_VALUE, NULL, 0, 0 );
	if( NULL == hIocp ) {
		LOG4_ERROR( "CreateIoCompletionPort failed, errno %d", WSAGetLastError() );
		return -1;
	}
	if(clientCfg.serverGroup.size()<=0)
	{
		LOG4_ERROR( "ServerGroup config is null." );
		return -1;
	}
	int index = serverIndex++%clientCfg.serverGroup.size();
	//每次连接Data Bus Server时都要换一个新的IP和Port
	SOCKET sock=INVALID_SOCKET;
	if(!proxyCfg.enable)
	    sock=ConnectServer(clientCfg.serverGroup[index].serverIP.c_str(),clientCfg.serverGroup[index].port);
	if(sock==INVALID_SOCKET)
		return -1;
	clientInfo.mFd=sock;
	state=STATE::ONLINE;
#elif __linux__
#define MAXSIZE         64000 
#define MAXEPS            256 
#define MAX_PATH          256

	if(clientCfg.udpReceiverCfg.isValid || clientCfg.udpSenderCfg.isValid)
	{
		if(clientCfg.udpReceiverCfg.isValid)
		{
			//pgmRecv->init(clientCfg.udpReceiverCfg.serverIP.c_str(),clientCfg.udpReceiverCfg.port);
		    //LOG4_INFO("Prepare receive data from udp server,IP: %s,Port: %d",clientCfg.udpReceiverCfg.serverIP.c_str(),clientCfg.udpReceiverCfg.port);
		}
		if(clientCfg.udpSenderCfg.isValid)
		{
			//pgmSend->init(clientCfg.udpSenderCfg.serverIP.c_str(),clientCfg.udpSenderCfg.port,40);
		    //LOG4_INFO("Prepare send data to udp server,IP: %s,Port: %d",clientCfg.udpSenderCfg.serverIP.c_str(),clientCfg.udpSenderCfg.port);
		}
		state=STATE::ONLINE;
		return 0;
	}

	if(clientCfg.serverGroup.size()<=0)
	{
		LOG4_ERROR( "ServerGroup config is null." );
		return -1;
	}
	//每次连接Data Bus Server时都要换一个新的IP和Port
	int index = serverIndex++%clientCfg.serverGroup.size();
	//socket
	struct sockaddr_in sin;
	memset( &sin, 0, sizeof(sin) );
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = inet_addr(clientCfg.serverGroup[index].serverIP.c_str() );
	sin.sin_port = htons(clientCfg.serverGroup[index].port);
	LOG4_INFO("id=%d,connect Data Bus Server,IP: %s,Port: %d",id,clientCfg.serverGroup[index].serverIP.c_str(),clientCfg.serverGroup[index].port);
	memset( &clientInfo, 0, sizeof( clientInfo ) );

	clientInfo.mFd = socket( AF_INET, SOCK_STREAM, 0 );
	if( clientInfo.mFd < 0 ) {
			LOG4_ERROR( "socket failed, errno %d, %s\r", errno, strerror( errno ) );
			return -1;
	}


	if( connect( clientInfo.mFd, (struct sockaddr *)&sin, sizeof(sin) ) != 0) {
			LOG4_ERROR( "id=%d,connect  to %s:%d failed, errno %d, %s", id,clientCfg.serverGroup[index].serverIP.c_str(),clientCfg.serverGroup[index].port,errno, strerror( errno ) );
			Close();
			return -1;
	}
	state=STATE::ONLINE;
	needCreatEP=true;
#endif
	IOUtils::setBuffer(clientInfo.mFd, 1000000, 1000000);
	return 0;
}
struct ThreadPoolArg
{
	AppServerImpl* server;
	int index;
};
int AppServerImpl::InitThread()
{
	sp_thread_t thread;
	int ret=0;
	if(clientCfg.udpReceiverCfg.isValid || clientCfg.serverGroup.size()>0 )
	{
		//初始化接收线程
		ret = sp_thread_create( &thread, NULL, eventThread, this );
		if( 0 == ret ) {
			LOG4_INFO( "id=%d,Thread 0x%llx has been created to read socket",id, thread );
		} else {
			LOG4_INFO( "id=%d,Unable to create a thread to read socket, %s",id, strerror( errno ) );
			return -1;
		}
	}
	if(clientCfg.udpSenderCfg.isValid || clientCfg.serverGroup.size()>0)
	{
		//初始化发送线程
		ret = sp_thread_create( &thread, NULL, sendThread, this );
		if( 0 == ret ) {
			LOG4_INFO( "id=%d,Thread 0x%llx has been created to send message",id, thread );
		} else {
			LOG4_INFO( "id=%d,Unable to create a thread to send message, %s",id, strerror( errno ) );
			return -1;
		}
	}
	//初始化timeout线程
    ret = sp_thread_create( &thread, NULL, timeoutThread, this );
	if( 0 == ret ) {
		LOG4_INFO( "id=%d,Thread 0x%llx has been created to timeout message",id, thread );
	} else {
		LOG4_INFO( "id=%d,Unable to create a thread to timeout message, %s",id, strerror( errno ) );
		return -1;
	}
	//初始化事件处理线程
	mMaxThreads =clientCfg.threadNum+2;
	if(mMaxThreads<5)
		mMaxThreads=5;
	//mThreadPool = new SP_ThreadPool( mMaxThreads );
	for( unsigned int i = 0; i < mMaxThreads; i++ ) {
		ThreadPoolArg *arg=new ThreadPoolArg();
		arg->index=i;
		arg->server=this;
		/*mThreadPool->dispatch( msgLoop, &arg );
		sp_sleep(100);*/
		ret = sp_thread_create( &thread, NULL, msgLoop, arg );
		if( 0 == ret ) {
			LOG4_INFO( "id=%d,Thread 0x%llx has been created to process message", id,thread );
		} else {
			LOG4_ERROR( "id=%d,Unable to create a thread to process message, %s", id,strerror( errno ) );
		}
	}
	return 0;
}
int AppServerImpl::Login()
{
	state=STATE::LOGINNING;
	int ret = RegAppServer();
	if(ret != 0)
		return ret;
	return 0;
}
void AppServerImpl::LoginCallback(const MsgParams& params)
{
	if(params.result==0)
	{
		uint64_t brokertime = ((MsgExpress::LoginResponse*)params.resp->message().get())->brokertime();
		uint64_t now = gettimepoint();
		timeoff=(int)(now-brokertime);
		address = ((MsgExpress::LoginResponse*)params.resp->message().get())->addr();
		LOG4_INFO("id=%d,Login success,time offset=%d ms,appServer address:%s",id,timeoff,((AppAddress)address).toString().c_str());
		state=STATE::LOGINED;
		OnEvent(Online);
		RegSubscribe();
	}
	else
	{
		LOG4_ERROR("id=%d,Login errer:%s",id,params.errmsg.c_str());
		state=STATE::ONLINE;
	}
	
}
int AppServerImpl::RegAppServer()
{
	LOG4_INFO("id=%d,AppServer appname:%s",id,clientCfg.appName.c_str());
	//注册appserver。获取地址
	MsgExpress::LoginInfo *login=new MsgExpress::LoginInfo();
	login->set_name(clientCfg.appName);
	login->set_type(clientCfg.appType);
	login->set_group(clientCfg.appGroup);
	login->set_uuid(clientCfg.uuid);
	login->set_starttime(startTime);
	login->set_ip("0.0.0.0");//让数据包压缩成功
	login->set_auth(clientCfg.auth);
	login->set_masterslave(serverMode);
	if(clientCfg.appType!=0)
	{
		for(size_t i = 0;i < clientCfg.serviceList.size();i++ )
		{
			login->add_serviceid(clientCfg.serviceList[i]);
		}
	}
	if (serverMode == Event::Downgrade2Slave || serverMode == Event::Downgrade2Standby)
	{
		ReportServerEvent(2, "I am a slave server ,now wait 10s to login");
		LOG4_INFO("id=%d,I am a slave server ,now wait 10s to login", id);
		std::this_thread::sleep_for(std::chrono::milliseconds(10000));
	}

	//LOG4_INFO("id=%d,AppServer reg:%s",id,login->ShortDebugString().c_str());
	LOGGER_INFO("id=" << id << ",AppServer reg:" << login->ShortDebugString().c_str());
	uint64_t start = gettimepoint();
	MessagePtr response;
	bool ret=SendMessage(*login,response,5000);
	if(!ret)
	{
		MsgExpress::ErrMessage err=this->GetLatestError();
		LOG4_ERROR("id=%d,Login error,info=%s",id,err.errmsg().c_str());
		return -1;
	}
	else
	{
		//LOG4_INFO("id=%d,Begin logining",id);
		uint64_t end = gettimepoint();
		int off=(int)(end-start);
		if(off<minoff)
		{
			minoff=off;
			uint64_t brokertime=((MsgExpress::LoginResponse*)response.get())->brokertime();
			timeoff=(int)(end-brokertime-off/2);
		}
		address=((MsgExpress::LoginResponse*)response.get())->addr();
		LOG4_WARN("id=%d,Login success,time offset=%d ms,deviation=%d ms,appServer address:%s",id,timeoff,off,((AppAddress)address).toString().c_str());
		state=STATE::LOGINED;
		ReportServerEvent(1, "Login to bus success,libVersion:" + string(LIBVERSION));
		OnEvent(Online);
		RegSubscribe();
		//ReportServerEvent(1, "subscribe ok");
	}
	return 0;
}
int AppServerImpl::RegService()
{
	/*if(isLogin == false)
		return -1;
	if(clientCfg.serviceList.size()==0)
		return 0;
	MsgExpress::RegService req;
	for(size_t i = 0;i < clientCfg.serviceList.size();i++ )
	{
		req.add_serviceid(clientCfg.serviceList[i]);
	}
	LOG4_INFO("register service:%s",req.ShortDebugString().c_str());
	MessagePtr resp;
	if(!SendMessage(req,resp,3000))
	{
		LOG4_INFO("register service failed:%s",req.ShortDebugString().c_str());
		return -1;
	}*/
	return 0;
}
int AppServerImpl::RegSubscribe()
{
	if (!IsLogin())
	{
		LOG4_ERROR("id=%d,addr=%s,not login,cant subscribe", id, ((AppAddress)address).toString().c_str());
		return -1;
	}
	if (clientCfg.subscribeList.size() < 1 && clientCfg.datasync.size() < 1)
	{
		LOG4_INFO("id=%d,addr=%s,no need to subscribe", id, ((AppAddress)address).toString().c_str());
		return 0;
	}
	int subid=10000;
	MsgExpress::ComplexSubscribeData *comSub=new MsgExpress::ComplexSubscribeData();
	for(size_t i = 0;i < clientCfg.subscribeList.size();i++)
	{
		MsgExpress::SubscribeData* sub=comSub->add_sub();
		sub->set_subid(subid++);
		sub->set_topic(clientCfg.subscribeList[i]);
	}
	if(comSub->sub_size()<1)
	{
		delete comSub;
		return 0;
	}
	MessagePtr response;
	if(ComplexSubscribe(*comSub,response))
	{
		LOGGER_INFO("id=" << id << ",addr=" << ((AppAddress)address).toString().c_str() << ",subscribe success:" << comSub->ShortDebugString().c_str());
		//LOG4_INFO("id=%d,addr=%s,subscribe success:%s", id, ((AppAddress)address).toString().c_str(), comSub->ShortDebugString().c_str());
		ReportServerEvent(1, "subscribe success");
	}
	else
	{
		LOGGER_INFO("id=" << id << ",addr=" << ((AppAddress)address).toString().c_str() << ",subscribe failed:" << comSub->ShortDebugString().c_str());
		//LOG4_ERROR("id=%d,addr=%s,subscribe failed:%s", id, ((AppAddress)address).toString().c_str(), comSub->ShortDebugString().c_str());
		ReportServerEvent(1, "subscribe failed");
		//this->PostMessage(*comSub, bind(&AppServerImpl::SubscribeCallback, this, _1), nullptr);
	}
	delete comSub;

	return 0;
}
void AppServerImpl::SubscribeCallback(const MsgParams& params)
{
	if(params.result==0)
	{
		LOG4_INFO("id=%d,subscribe success",id);
		ReportServerEvent(1, "subscribe success");
	}
	else
	{
		LOG4_ERROR("id=%d,subscribe errer:%s",id,params.errmsg.c_str());
		ReportServerEvent(1, "subscribe failed");
	}
}

void AppServerImpl::SendHeartBeat()
{
	if(decoder->hasHeartbeat())
		m_iHeartBeatTick=0;
	MsgExpress::HeartBeat* heartbeat=new MsgExpress::HeartBeat(heartbeatInfo);
	unsigned int topmem, mem;
	processInfo.GetMemoryInfo(topmem,mem);
	heartbeat->set_topmemory(topmem);
	heartbeat->set_memory(mem);
	heartbeat->set_cpu(processInfo.GetCpu());
	heartbeat->set_sendqueue(sendMsgQueue->getLength());
	heartbeat->set_receivequeue(decoder->getNormalQueue()->getLength() + decoder->getSequenceQueue()->getLength());
	uint64_t servertime = gettimepoint();
	heartbeat->set_servertime(servertime);
	heartbeat->set_addr(this->address);
	std::unique_lock<std::mutex> lock(statusMutex);
	heartbeat->set_status(serverStatus);
	serverStatus = "";
	lock.unlock();

	PostMessage(MessagePtr(heartbeat), nullptr, NULL);
	m_iHeartBeatTick++;
	//LOG4_DEBUG("id=%d,send heart beat,info:%s", id, heartbeat->ShortDebugString().c_str());
}
void AppServerImpl::HeartBeatCallback(const MsgParams& params)
{
	if(params.result!=0)
		m_iHeartBeatTick++;
	else
		m_iHeartBeatTick=0;
}
void AppServerImpl::OnHeartBeatMessage(PackagePtr ptr)
{
	//m_iHeartBeatTick = 0;
	bool ret=ParseMessage(ptr);
	if(!ret)
		return;
	uint64_t brokertime=((MsgExpress::HeartBeatResponse*)ptr->message().get())->brokertime();
	uint64_t servertime=((MsgExpress::HeartBeatResponse*)ptr->message().get())->servertime();
	uint64_t now = gettimepoint();
	int off=(int)(now-servertime);
	if(off<=minoff)
	{
		int tmp=(int)(now-brokertime-off/2);
		if(timeoff!=tmp)
		    LOG4_INFO("id=%d,Server time compared with broker time,offset=%d ms,deviation=%d ms",id,tmp,off);
		timeoff=tmp;
		minoff=off;
	}
}
uint64_t AppServerImpl::GetBrokerTime()
{
	uint64_t now = gettimepoint();
	return now-timeoff;
}
void AppServerImpl::ReportServerEvent(int code, const string& status)
{
	std::unique_lock<std::mutex> lock(statusMutex);
	std::ostringstream   ostr;
	ostr << sp_thread_self() << "|" << gettimepoint() << "|" << code << "|" << status << std::endl;
	serverStatus += ostr.str();

}
bool AppServerImpl::IsOnLine()
{
	return state>=STATE::ONLINE;
}
int AppServerImpl::AutoTryConnectDataBusServer()
{
	m_iTryConnectTick++;
	int ret = Connect(&clientCfg,&proxyCfg);
	if (ret == 0)
	{
		LOG4_INFO("id=%d,client reconnect success",id);
		m_iTryConnectTick = 0;
	}
	return ret;
}
void AppServerImpl::HaveASleep(DWORD milliseconds)
{
#define SHORT_INTERVAL  50
	int count = milliseconds / SHORT_INTERVAL;
	while (count > 0)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(SHORT_INTERVAL));
		count--;
		//如果线程停止
		if (!isRun)
			break;
		//如果连接刚断开,则跳出循环
		if ((!IsOnLine() && m_iTryConnectTick == 0))
			break;
	}
}
sp_thread_result_t SP_THREAD_CALL  AppServerImpl::heartBeatThread(void* arg)
{
	AppServerImpl* appServer=(AppServerImpl*)arg;
	appServer->heartbeatrunning=true;
	LOG4_INFO("id=%d,Start heartbeat",appServer->id);
	int count = 0;
	while(appServer->isRun)
	{
		count++;
		if (count % 20 == 0)
		{
			/*LOG4_INFO("Thread Info:");
			std::unordered_map<string, string>::const_iterator it = appServer->mapThread.begin();
			while (it != appServer->mapThread.end())
			{
				LOG4_INFO("%s", it->second.c_str());
				it++;
			}*/
		}
		if(appServer->state==STATE::LOGINED)
		{
			appServer->SendHeartBeat();
			if (appServer->m_iHeartBeatTick > appServer->clientCfg.maxHeartBeatTick)
			{
				LOG4_INFO("id=%d,Heartbeat failed too many times,now broke the connection",appServer->id);
				appServer->Close();
			}
			appServer->HaveASleep(appServer->clientCfg.heartbeat_interval);
		}
		else
		{
			if (appServer->state==STATE::OFFLINE)
			{
				appServer->m_iHeartBeatTick = 0;
				LOG4_INFO("id=%d, try reconnect Data Bus Server", appServer->id);
				int ret = appServer->AutoTryConnectDataBusServer();
				//appServer->serverIndex++;
				if(!appServer->isRun)
					break;
			}
			if (appServer->state==STATE::ONLINE)
			{
				LOG4_INFO("id=%d,client try to login ...",appServer->id);
			    int ret = appServer->Login();
			    if(ret != 0)
			    {
				    LOG4_ERROR("id=%d,client login error after reconnect",appServer->id);
				    appServer->Close();
			    }
			}
			appServer->HaveASleep(appServer->clientCfg.broken_interval);
		}
	}
	LOG4_INFO("id=%d,Stop heartbeat",appServer->id);
	appServer->heartbeatrunning=false;
	return 0;
}
bool AppServerImpl::Release()
{
	LOG4_INFO("id=%d,release client",id);
	if(!isRun)
		return false;
	/*MsgExpress::Logout logout;
	logout.set_reserve(0);
	MessagePtr resp;
	if(!SendMessage(logout,resp,1000))
	{
		LOG4_ERROR("App client logout failed.");
	}*/
	isRun=false;
	decoder->getSequenceQueue()->push(NULL);
	SafeQueue* queue=decoder->getNormalQueue();
	for(unsigned int i=0;i<mMaxThreads;i++)
	{
	    queue->push(NULL);
	}
	sendMsgQueue->push(NULL);
	sp_thread_semaphore_post(&timeoutSem);
	completeMsgQueue.push(NULL);
	int count=0;
	while(heartbeatrunning)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		count++;
		if(count>=100)
		{
			LOG4_ERROR("Something wrong when heartbeat thread exit.");
			break;
		}
	}
#ifdef _WIN32
	Sleep(500);
#elif __linux__
	usleep(1000*1000);
	//delete pgmSend;
	//pgmSend=NULL;
	//delete pgmRecv;
	//pgmRecv=NULL;
#endif

	Close();

	return true;
}
void AppServerImpl::CloseSocket(SOCKET& sock)
{
	SOCKET tmp=sock;
	sock=INVALID_SOCKET;
	if (tmp == INVALID_SOCKET)
		return;
	LOG4_INFO("id=%d,close socket %d", id, tmp);
#ifdef _WIN32
	closesocket(tmp);
#elif __linux__
	close(tmp);
	epfd = -1;
#endif
}
void AppServerImpl::Close()
{
	LOG4_INFO("id=%d,Begin close",id);
	std::unique_lock<mutex> lock(mCloseMutex);
	address = 0;
	if(clientInfo.mFd==INVALID_SOCKET || state==STATE::OFFLINE)
	{
		CloseSocket(clientInfo.mFd);
		lock.unlock();
		state=STATE::OFFLINE;
		LOG4_INFO("id=%d,Has closed",id);
		return;
	}
	state=STATE::OFFLINE;
	lock.unlock();
	if(decoder)
		LOG4_INFO("id=%d,SyncQueue size:%d ,AsyncQueue size:%d", id, decoder->getSequenceQueue()->getLength(), decoder->getNormalQueue()->getLength());
	std::unique_lock<std::mutex> lock2(mBufferMutex);
	size_t size = evbuffer_get_length(databuff);
	if(size>0)
		evbuffer_drain(databuff, size);
	LOG4_INFO("id=%d,Reset buffer,size:%d",id,size);
	lock2.unlock();

	OnEvent(Offline);
}
MsgExpress::ErrMessage* AppServerImpl::GetErrorMsg()
{
#ifdef _WIN32
	static __declspec(thread) MsgExpress::ErrMessage* latestErr=NULL;
#else
	static __thread MsgExpress::ErrMessage* latestErr=NULL;
#endif
	if(latestErr==NULL)
		latestErr=new MsgExpress::ErrMessage();
	return latestErr;
}
void AppServerImpl::ClearLatestError()
{
	MsgExpress::ErrMessage* latestErr=GetErrorMsg();
	latestErr->Clear();
}
MsgExpress::ErrMessage AppServerImpl::GetLatestError()
{
	MsgExpress::ErrMessage err(*GetErrorMsg());
	return err;
}
sp_thread_result_t AppServerImpl::msgLoop(void* arg)
{
	ThreadPoolArg* args=(ThreadPoolArg*)arg;
	AppServerImpl* appServer=args->server;
	appServer->processMsg(args->index);
	delete args;
	return 0;
}
bool AppServerImpl::ParseMessage(PackagePtr package)
{
	string errmsg;
	return ParseMessage(package, errmsg);
}
bool AppServerImpl::ParseMessage(PackagePtr package, string& errstr)
{
	if (package->message())
		return true;
	bool ret = false;
	string clazz = CommandConfig::getInstance().GetClass(package->GetCommand());
	if (clazz.length() < 1)
	{
		errstr = "Failed to get Class name";
		LOG4_ERROR("Failed to get Class name for command:%d", package->GetCommand());
		return false;
	}
	MessagePtr msg = CreateMessage(clazz);
	if (msg)
	{
		package->set_message(msg);
		ret = MessageUtil::parse(package.get(), errstr);
		if (!ret)
		{
			LOG4_ERROR("id=%d,Parse failed,cmd:0x%x,class name:%s", id, package->GetCommand(), clazz.c_str());
		}
	}
	if (!ret){
		LOG4_ERROR("Create message failed,class name=%s", clazz.c_str());
	}
	return ret;
}
bool AppServerImpl::ProcessMultiPackage(PackagePtr ptr)
{
	if (ptr->header().ismultipage())
	{
		std::unique_lock<mutex> lock(mMutexMultiPackage);
		shared_ptr<std::string> data;
		if (ptr->header().pageno() == 1)
		{
			data = shared_ptr<std::string>(new string());
			mMapMultiPackage[ptr->GetSerialNum()] = data;
		}
		else
			data = mMapMultiPackage[ptr->GetSerialNum()];
		lock.unlock();
		if (ptr->header().pageno() != 1 && (!data || data->size() == 0))//前面就出错了，下面的包丢弃
		{
			LOGGER_ERROR("Multi package error," << ptr->DebugString().c_str());
			//LOG4_ERROR("Multi package error,%s", ptr->DebugString().c_str());
			MsgCallbackPtr cbInfo = getCallbackInfo(ptr->GetSerialNum(), true);
			if (cbInfo)
			{
				cbInfo->params.result = MsgExpress::ERRCODE_LOSTDATA;
				cbInfo->params.errmsg = MsgExpress::ERRMSG_LOSTDATA;
				cbInfo->cb(cbInfo->params);
			}
		}
		else
		{
			if (ptr->header().iszip())
			{
				string tmp;
				bool unzipOK = MessageUtil::unCompress(ptr->GetBodyData(), ptr->GetBodySize(), ptr->header().compratio(), tmp);
				if (!unzipOK)
				{
					LOGGER_ERROR("unCompress failed," << ptr->DebugString().c_str());
					//LOG4_ERROR("unCompress failed,%s", ptr->DebugString().c_str());
					data->clear();//出错，清理历史数据，后面新收到的包的前面丢弃
					MsgCallbackPtr cbInfo = getCallbackInfo(ptr->GetSerialNum(), true);
					if (cbInfo)
					{
						cbInfo->params.result = MsgExpress::ERRCODE_UNZIPFAILED;
						cbInfo->params.errmsg = MsgExpress::ERRMSG_UNZIPFAILED;
						cbInfo->cb(cbInfo->params);
					}
				}
				else
					data->append(tmp);
			}
			else
			{
				data->append(ptr->GetBodyData(), ptr->GetBodySize());
			}
			if (ptr->header().pageno() != 0)
			{
				MsgCallbackPtr cbInfo = getCallbackInfo(ptr->GetSerialNum(), false);
				if (cbInfo)
					cbInfo->params.hasNextPackage = true;
			}
			else
			{
				std::unique_lock<mutex> lock(mMutexMultiPackage);
				mMapMultiPackage[ptr->GetSerialNum()] = shared_ptr<std::string>();
				lock.unlock();
				MessagePtr message = CreateMessage(ptr->classtype());
				if (message == NULL || !MessageUtil::parseFromArray(message.get(), data->data(), data->size(), false, 0))
				{
					LOGGER_ERROR("unSeialize failed," << ptr->DebugString().c_str());
					//LOG4_ERROR("unSeialize failed,%s", ptr->DebugString().c_str());
					MsgCallbackPtr cbInfo = getCallbackInfo(ptr->GetSerialNum(), true);
					if (cbInfo)
					{
						cbInfo->params.result = MsgExpress::ERRCODE_UNSERIALIZEFAILE;
						cbInfo->params.errmsg = MsgExpress::ERRMSG_UNSERIALIZEFAILE;
						cbInfo->cb(cbInfo->params);
					}
				}
				else
				{
					ptr->set_message(message);
					MsgCallbackPtr cbInfo = getCallbackInfo(ptr->GetSerialNum(), false);
					if (cbInfo)
					{
						cbInfo->params.hasNextPackage = false;
					}
				}
			}
		}
	}
	return true;
}
void AppServerImpl::processMsg(int index)
{
	sp_thread_t threadId=sp_thread_self();
	char tmp[128];
	memset(tmp, 0, sizeof(tmp));
	snprintf(tmp, 128, "0x%llx", (long long unsigned int)threadId);
#ifdef __linux__
	prctl(PR_SET_NAME, tmp);
#endif
	std::unique_lock<std::mutex> lock(mutexThread);
	mapThread.insert(std::make_pair(tmp, tmp));
	lock.unlock();

	if(index==0)
	{
		LOG4_INFO("id=%d,Thread 0x%llx for sequence message handle",id,threadId);
	}
	else if (index == 1)
	{
		LOG4_INFO("id=%d,Thread 0x%llx for priority message handle", id, threadId);
	}
	while(isRun)
	{
		SafeQueue* queue=NULL;
		if(index==0)
			queue = decoder->getSequenceQueue();
		else if (index == 1 )
			queue = decoder->getPriorityQueue();
		else
			queue=decoder->getNormalQueue();
		LOG4_INFO("id=%d,Thread 0x%llx,queue:0X%x",id,threadId,queue);
		Package* package=NULL;
		while((package=(Package*)queue->pop())!=NULL)
		{
			time_t time1;
			time(&time1);
			struct tm tm1;
#ifdef WIN32  
			tm1 = *localtime(&time1);
#else  
			localtime_r(&time1, &tm1);
#endif 
			char key[64];
			memset(key, 0, sizeof(key));
			snprintf(key, 64, "0x%llx", (long long unsigned int) threadId);
			snprintf(tmp, 128, "0x%llx[%d]:[%2.2d:%2.2d:%2.2d]%s", (long long unsigned int) threadId, index, tm1.tm_hour, tm1.tm_min, tm1.tm_sec, package->classtype().c_str());
			mapThread[key] = tmp;
			int service = CommandConfig::getInstance().GetServiceId(package->GetCommand());
			PackagePtr ptr=PackagePtr(package);
			if (service != 0 && (!ptr->header().ismultipage() || ptr->header().pageno() == 0) )
			{
				std::unique_lock<mutex> lock(mHeartbeatMutex);
				if(ptr->IsRequest())
					heartbeatInfo.set_recvrequest(heartbeatInfo.recvrequest()+1);
				else if(ptr->IsResponse())
					heartbeatInfo.set_recvresponse(heartbeatInfo.recvresponse()+1);
				else if(ptr->IsPublish())
					heartbeatInfo.set_recvpub(heartbeatInfo.recvpub()+1);
			}
			if(!isRun) break;
			
			if (ptr->classtype() == MsgExpress::HeartBeatResponse::default_instance().GetTypeName())
			{
				OnHeartBeatMessage(ptr);
				continue;
			}
			else if (ptr->classtype() == MsgExpress::LoginResponse::default_instance().GetTypeName() || ptr->classtype() == MsgExpress::KickOffApp::default_instance().GetTypeName())
			{
				AppServerImpl::OnRawPackage(ptr);
				continue;
			}

			if (ptr->IsRequest())
			{
				time_t t = package->time();
				time_t tm;
				time(&tm);
				if (t > 0 && tm - t > this->clientCfg.waittime)
				{
					LOGGER_ERROR("id=" << id << ",msg wait too long to process,ignore:" << ptr->DebugString().c_str());
					continue;
				}
			}
			if (queue == decoder->getSequenceQueue())
			{
				LOGGER_DEBUG("id=" << id << ",Process a sequence msg:" << ptr->DebugString().c_str());
			}
			OnRawPackage(ptr);
			if(!isRun) break;
		}
	}
	LOG4_INFO("id=%d,One thread exit,id:%llx",id,sp_thread_self());
}
void AppServerImpl::Tongji(const char* data)
{
	PackageHeader header;
	header.read(data);
	LOG4_DEBUG("id=%d,Msg   posted:%s", id, header.DebugString().c_str());
	int service = CommandConfig::getInstance().GetServiceId(header.command());
	if (service != 0)
	{
		if (!header.ismultipage() || header.pageno() == 0)
		{
			std::unique_lock<mutex> lock(mHeartbeatMutex);
			if (header.type() == PackageHeader::Request)
				heartbeatInfo.set_sendrequest(heartbeatInfo.sendrequest() + 1);
			else if (header.type() == PackageHeader::Response)
				heartbeatInfo.set_sendresponse(heartbeatInfo.sendresponse() + 1);
			else if (header.type() == PackageHeader::Publish)
				heartbeatInfo.set_sendpub(heartbeatInfo.sendpub() + 1);
		}
	}
}
void AppServerImpl::processSend()
{
#define BUFFERSIZE   102400
	char buffer[BUFFERSIZE];
#define ARRSIZE  128
	struct iovec iovArray[ ARRSIZE ];
	SendDataItem* dataitem[ARRSIZE];
	memset( iovArray, 0, sizeof( iovArray ) );

	struct sockaddr_in addr;
	SOCKET sock=INVALID_SOCKET;
	if(clientCfg.udpSenderCfg.isValid)
	{
		/* create what looks like an ordinary UDP socket */  
		if ((sock=socket(AF_INET,SOCK_DGRAM,0)) < 0)   
		{  
			LOG4_ERROR( "create udp socket failed." );
			return ;
		}  
		/* set up destination address */  
		memset(&addr,0,sizeof(addr));  
		addr.sin_family=AF_INET;  
		addr.sin_addr.s_addr=inet_addr(clientCfg.udpSenderCfg.serverIP.c_str());  
		addr.sin_port=htons(clientCfg.udpSenderCfg.port);
	}
	int count = 0;
	while(isRun)
	{
		if(!IsOnLine())
		{
			if (sendMsgQueue->getLength() > 0)
			{
				LOG4_INFO("id=%d,Clear sendMsgQueue,size:%d", id, sendMsgQueue->getLength());
				while (sendMsgQueue->getLength() > 0)
				{
					SendDataItem* item = (SendDataItem*)sendMsgQueue->pop();
					delete item;
				}
			}
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
			continue;
		}
		if (!isRun)
			break;
		int iovSize = 0;
		SendDataItem* item=(SendDataItem*)sendMsgQueue->pop();
		if(item==NULL)
		{
			LOG4_INFO("id=%d,null item.",id);
			continue;
		}

		/*{
			PackageHeader header;
			header.read(item->data());
			LOG4_DEBUG("id=%d,Prepare post:%s, item number=%d.",id,header.DebugString().c_str(),sendMsgQueue->getLength()+1);
		}*/
		if(clientCfg.udpSenderCfg.isValid)
		{
			int totalsize=0;
			memcpy(buffer+totalsize,item->data(),item->size());
			totalsize+=item->size();
			dataitem[iovSize++]=item;
			while(totalsize<1024 && sendMsgQueue->getLength()>0)
			{
				item=(SendDataItem*)sendMsgQueue->top();
				if(item==NULL)
					break;
				if(totalsize+item->size()>1024)
					break;
				item=(SendDataItem*)sendMsgQueue->pop();
				memcpy(buffer+totalsize,item->data(),item->size());
				totalsize+=item->size();
				dataitem[iovSize++]=item;
			}
			if(totalsize>0)
			{
#ifdef __linux__
				//if(!pgmSend->pgm_send(buffer,totalsize))
				//    LOG4_ERROR("Multicast failed.");
#endif
			}
			for(int i=0;i<iovSize;i++)
				delete dataitem[i];
		}
		else
		{
			if(item->size()>=10240)
			{
				//LOG4_INFO("id=%d,Prepare sending big message,size=%d\r\n",id,item->size());
				int leftBytes= item->size();
				int sendedBytes=0;
				while(leftBytes>0)
				{
					int iovSize = 0;
					int sendBytes=leftBytes>1024? 1024:leftBytes;
					iovArray[ 0 ].iov_base =(char*) item->data()+sendedBytes;
					iovArray[ 0 ].iov_len =sendBytes;
					int size = sp_writev( (SOCKET)clientInfo.mFd,iovArray, 1 );
					if(size<sendBytes)
						LOG4_INFO("sp_writev lost data,want send:%d,real send=%d\r\n",sendBytes,size);
					if(size>0)
					{
						leftBytes-=size;
						sendedBytes+=size;
					}
					if (!IsOnLine())
						continue;
				}
				Tongji(item->data());
				//LOG4_INFO("id=%d,Finished sending big message,size=%d\r\n",id,item->size());
				delete item;
			}
			else
			{
				int iovSize = 0;
				int totalBytes=0;
				dataitem[iovSize]=item;
				iovArray[ iovSize ].iov_base = (char*)item->data();
				iovArray[ iovSize++ ].iov_len =item->size();
				totalBytes+=item->size();
				while(iovSize<ARRSIZE && sendMsgQueue->getLength()>0 && totalBytes<10240)
				{
					item=(SendDataItem*)sendMsgQueue->top();
					if(item==NULL || totalBytes+item->size()>10240)
						break;
					dataitem[iovSize]=(SendDataItem*)sendMsgQueue->pop();
					iovArray[ iovSize ].iov_base = (char*)item->data();
					iovArray[ iovSize++ ].iov_len =item->size();
					totalBytes+=item->size();
				}
				if(totalBytes>500000)
				{
					LOG4_INFO("id=%d,Too big package,size=%d",id,totalBytes);
				}
				int bytesSend=sp_writev((SOCKET)clientInfo.mFd,iovArray,iovSize);
				if(bytesSend<totalBytes)
				{
					LOG4_ERROR("id=%d,Lost data,sended:%d,want send:%d",id,bytesSend,totalBytes);
				}
				for(int i=0;i<iovSize;i++)
				{
					Tongji(dataitem[i]->data());
					delete dataitem[i];
				}
			}
		}
	}
	LOG4_INFO("id=%d,send thread exit", id);
}

void AppServerImpl::processComplete()
{
	while(isRun)
	{
		SendDataItem* item=(SendDataItem*)completeMsgQueue.pop();
		if(item)
			delete item;
	}
	LOG4_INFO("id=%d,complete thread exit", id);
}
sp_thread_result_t SP_THREAD_CALL AppServerImpl::sendThread(void* arg)
{
	AppServerImpl* appServer=(AppServerImpl*)arg;
	appServer->processSend();
	return 0;
}
sp_thread_result_t SP_THREAD_CALL AppServerImpl::completeThread(void* arg)
{
	AppServerImpl* appServer=(AppServerImpl*)arg;
	appServer->processComplete();
	return 0;
}
DWORD AppServerImpl::timeoutNext( MyHeap<TimeoutItem>& timeoutHeap )
{
	std::unique_lock<std::mutex> lock(mMutex);
	bool empty=timeoutHeap.isEmpty();
	TimeoutItem itm ;
	if(!empty)
		itm = timeoutHeap.top();

	if(empty ) return 100;

	struct timeval curr;
	sp_gettimeofday(&curr, NULL);
	struct timeval * first = &( itm.tv );
	int ret = ( first->tv_sec - curr.tv_sec ) * 1000 + ( first->tv_usec - curr.tv_usec ) / 1000;
	if( ret < 0 ) 
		ret = 1;
	else if(ret>100)
		ret=100;
	return (DWORD)ret;
}
void  AppServerImpl::popAndCallback( )
{
	do
	{
		//LOG4_INFO("check minheap,heapSize=%d", minheap.size());
		struct timeval curr;
		sp_gettimeofday(&curr, NULL);
		std::unique_lock<std::mutex> lock(mMutex);
		bool empty = minheap.isEmpty();
		if(empty)
		{
			break;
		}
		TimeoutItem itm = minheap.top();
		struct timeval * first = &( itm.tv );
		int ret = ( first->tv_sec - curr.tv_sec ) * 1000 + ( first->tv_usec - curr.tv_usec ) / 1000;
		//LOG4_INFO("check minheap,time left=%d,heapSize=%d", ret, minheap.size());
		if (ret < 1 )
		{
			minheap.pop();
			int heapSize = minheap.size();
			int unanswered = 0;//
			{
				std::unique_lock<std::mutex> lock(cbMutex);
				unanswered=mMapAsyncInfo.size();
			}
			MsgCallbackPtr cbInfo=getCallbackInfo(itm.serial,false);
			if(cbInfo)
			{
				if(cbInfo->params.hasNextPackage)
				{
					cbInfo->params.hasNextPackage=false;
					TimeoutItem newItem=createTimeout(itm.msTimeout,itm.serial);
					minheap.push(newItem);
					LOG4_INFO("Multi package timeout");
				}
				else
				{
					LOG4_ERROR("id=%d,timeout popAndCallback,serial=%d,heapSize: %d,unanswered size:%d",id,itm.serial,heapSize,unanswered);
					cbInfo->params.result=1;
					cbInfo->params.errmsg="Time out";
					cbInfo->cb(cbInfo->params);
					getCallbackInfo(itm.serial);
				}
			}
			else
				LOG4_DEBUG("id=%d,popAndCallback,serial=%d ,heapSize: %d,unanswered size:%d",id,itm.serial,heapSize,unanswered);
		}
		else{
			break;
		}
	}while(this->isRun);
	
}
void AppServerImpl::clearTimeout()
{
	while (this->minheap.size() > 0)
	{
		TimeoutItem itm = this->minheap.pop();
		MsgCallbackPtr cbInfo = getCallbackInfo(itm.serial);
		if (cbInfo)
		{
			LOG4_ERROR("id=%d,timeout popAndCallback,serial=%d", id, itm.serial);
			cbInfo->params.result = 1;
			cbInfo->params.errmsg = "Time out";
			cbInfo->cb(cbInfo->params);
		}
	}
}
void AppServerImpl::processTimeout()
{
	LOG4_INFO("id=%d,process timeout thread start", id);
	while(isRun)
	{
		DWORD msTimeout=100;
		//LOG4_INFO("id=%d,wait timeout 100ms", id);
		int ret=sp_thread_semaphore_timedwait(&timeoutSem,msTimeout);
		//LOG4_INFO("wait timeout 100ms end,ret=%d", ret);
		if(!isRun) break;
		//if( ret==0 ) 
		{
			popAndCallback();
		}
	}
	LOG4_INFO("id=%d,process timeout thread exit",id);
}
void AppServerImpl::processEvent()
{
	if(clientCfg.udpReceiverCfg.isValid)
	{
#ifdef __linux__
		static const int SIZE=4096;
		char buffer[ SIZE ] = { 0 };
		size_t byteRead = 0;
		LOG4_INFO("Begin receive udp data");
		while(isRun)
		{
			byteRead=SIZE;
		}
#endif
		return;
	}
#ifdef _WIN32
	DWORD bytesTransferred = 0;
	ClientInfo * client = NULL;
	IocpEvent * event = NULL;

	while(isRun)
	{
		if(!IsOnLine())
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
			continue;
		}
		on_read( &clientInfo, event );

	}
#elif __linux__
	while(isRun) 
	{
		if(needCreatEP)
		{
			epfd = epoll_create(100000);
			ev.data.fd =  clientInfo.mFd; 
			ev.events = EPOLLIN ; 
			epoll_ctl(epfd, EPOLL_CTL_ADD, clientInfo.mFd, &ev);
		    needCreatEP=false;
		}
		if(epfd == -1)
		{
			usleep(5*1000);
			continue;
		}
		DWORD msTimeout=100;//timeoutNext(minheap);
		int nfds=epoll_wait(epfd,events,20,msTimeout);
		//LOG4_DEBUG("nfds = %d,epfd = %d",nfds,epfd);
		if(nfds==0)
		{
			//popAndCallback(minheap);
			continue;
		}
		for(int i=0; i<nfds; ++i)  
		{
			if(events[i].events&EPOLLIN)     
			{ 
				if ( (events[i].data.fd) < 0)  
						continue; 
				char buffer[ 4096 ] = { 0 };
				int byteRead = 0;
				if ( (byteRead = read(events[i].data.fd, buffer, sizeof(buffer))) < 0) 
				{  
					if (errno == ECONNRESET) 
					{  
						LOG4_ERROR("id=%d,ECONNRESET: %s\n",id, strerror(ECONNRESET));  
						epoll_ctl(epfd,EPOLL_CTL_DEL,events[i].data.fd,NULL); 
						Close(); 
						break;
					}  
					else 
					{  
						LOG4_ERROR("id=%d,readline error,errno :%d, %s\n",id,errno,strerror( errno ));  
						Close();
						break;
					}  
				}  
				else if (byteRead == 0)   
				{  
					LOG4_ERROR("id=%d,in epoll recv data size = 0",id);
					epoll_ctl(epfd,EPOLL_CTL_DEL,events[i].data.fd,NULL);
					Close(); 
					//usleep(5*1000);
				}  
				else  
				{ 
					//LOG4_DEBUG("in epoll recv data size = %d",byteRead);
					
					std::unique_lock<mutex> lock(mBufferMutex);
					evbuffer_add(databuff, buffer, byteRead);
					int ret=decoder->decode2(databuff);
					lock.unlock();
				}  
			}  
			else if(events[i].events&EPOLLOUT)     
			{ 
				//LOG4_DEBUG("in epoll write");
				char buffer[4096];
				write(events[i].data.fd, buffer, sizeof(buffer));  
				ev.data.fd=events[i].data.fd;  
				ev.events=EPOLLIN ; 
				epoll_ctl(epfd,EPOLL_CTL_MOD,events[i].data.fd,&ev);  
			}  
		}
	}
#endif
	LOG4_INFO("id=%d,process event thread exit", id);
}
#ifdef _WIN32
void AppServerImpl::on_read( ClientInfo * client, IocpEvent * event )
{
	char buffer[MAXPACKSIZE] = { 0 };
	int bytesTransferred = recv( (int)client->mFd, buffer, sizeof( buffer ), 0 );
	if (!isRun)
		return;
	if( bytesTransferred <= 0) {
		int error = WSAGetLastError();
		if (error == WSAETIMEDOUT)
		{
			return;
		}
		if (error == WSAECONNRESET)
		{
			LOGGER_ERROR("id = " << id << ",A existing connection was forcibly closed by the remote host, addr=" << ((AppAddress)this->GetAddress()).toString().c_str() << " errno=" << WSAGetLastError() );
			Close();
		}  
		else if (error == WSAECONNABORTED)
		{
			LOGGER_ERROR("id = " << id << ",An established connection was aborted by the software in your host machine, addr=" << ((AppAddress)this->GetAddress()).toString().c_str() << " errno=" << WSAGetLastError());
			Close();
		}
		else
		{
			LOGGER_ERROR("id = " << id << ",addr=" << ((AppAddress)this->GetAddress()).toString().c_str() << " errno=" << WSAGetLastError());
			Close();
		}
		return;
	}
	//LOG4_INFO("id=%d,read data,size= %d", id, bytesTransferred);
	if (bytesTransferred >= MAXPACKSIZE)
	{
		LOGGER_ERROR("id=" << id << ",recv big data,size= " << bytesTransferred);
	}
	std::unique_lock<mutex> lock(mBufferMutex);
	if (evbuffer_add(databuff, buffer, bytesTransferred) != 0)
	{
		LOGGER_ERROR("id = " << id << ",evbuffer_add fail, size=" << bytesTransferred << " buffer size=" << evbuffer_get_length(databuff));
	}

	int ret=decoder->decode2(databuff);
	lock.unlock();

}

void AppServerImpl::on_write( ClientInfo * client, IocpEvent * event )
{
	DWORD sendBytes = 1;

	event->mType = IocpEvent::eEventSend;
	memset( &( event->mOverlapped ), 0, sizeof( OVERLAPPED ) );
	event->mWsaBuf.buf = NULL;
	event->mWsaBuf.len = 0;
	if( SOCKET_ERROR == WSASend( (SOCKET)client->mFd, &( event->mWsaBuf ), 1,
			&sendBytes, 0,	&( event->mOverlapped ), NULL ) ) {
		if( ERROR_IO_PENDING != WSAGetLastError() ) {
			LOG4_ERROR( "id=%d,WSASend fail, errno %d",id, WSAGetLastError() );
			Close();
		}
	}
}
#endif
sp_thread_result_t SP_THREAD_CALL AppServerImpl::eventThread(void* arg)
{
	AppServerImpl* appServer=(AppServerImpl*)arg;
	appServer->processEvent();
	return 0;
}
sp_thread_result_t SP_THREAD_CALL AppServerImpl::timeoutThread(void* arg)
{
	AppServerImpl* appServer=(AppServerImpl*)arg;
	appServer->processTimeout();
	return 0;
}
bool AppServerImpl::postRawData(const char* data,unsigned int size)
{
	if(!IsOnLine())
	{
		SETLASTERROR(NOTCONNECTED);
		return false;
	}
	SendDataItem* item=new SendDataItem(data,size);
	if(!sendMsgQueue->push(item))
	{
		delete item;
		SETLASTERROR(SENDQUEUEFULL);
		return false;
	}
	return true;
}
bool  AppServerImpl::postRawData(SendDataItem* item)
{
	//LOG4_DEBUG( "id=%d,postRawData.",id );
	if(!IsOnLine())
	{
		SETLASTERROR(NOTCONNECTED);
		return false;
	}
	if(clientCfg.sendmode==1)
	{
		//LOG4_ERROR( "id=%d,directSend.",id );
		return directSend(item);
	}
	int oldSize=sendMsgQueue->getLength();
	if(!sendMsgQueue->push(item))
	{
		SETLASTERROR(SENDQUEUEFULL);
		return false;
	}
	int size=sendMsgQueue->getLength();
	//LOG4_DEBUG( "id=%d,send queue size=%d/%d.",id,oldSize,size );
	return true;
}

bool AppServerImpl::postRawData(const vector<SendDataItem*>& vecItems)
{
	if(!IsOnLine())
	{
		SETLASTERROR(NOTCONNECTED);
		return false;
	}
	int oldSize=sendMsgQueue->getLength();
	for(int i=0;i<vecItems.size();i++)
	{
		if(!sendMsgQueue->push(vecItems[i]))
		{
			SETLASTERROR(SENDQUEUEFULL);
			return false;
		}
	}
	int size=sendMsgQueue->getLength();
	return true;
}

bool  AppServerImpl::directSend(SendDataItem* item)
{
	if(!IsOnLine())
	{
		SETLASTERROR(NOTCONNECTED);
		return false;
	}
	static struct sockaddr_in addr;
	static SOCKET sock=INVALID_SOCKET;
	if(clientCfg.udpSenderCfg.isValid && sock==INVALID_SOCKET)
	{
		/* create what looks like an ordinary UDP socket */  
		if ((sock=socket(AF_INET,SOCK_DGRAM,0)) < 0)   
		{  
			LOG4_ERROR( "create udp socket failed." );
			return false;
		}  
		/* set up destination address */  
		memset(&addr,0,sizeof(addr));  
		addr.sin_family=AF_INET;  
		addr.sin_addr.s_addr=inet_addr(clientCfg.udpSenderCfg.serverIP.c_str());  
		addr.sin_port=htons(clientCfg.udpSenderCfg.port);
	}
	if(clientCfg.udpSenderCfg.isValid)
	{
		if(item->size()>0)
		{
#ifdef __linux__
			//if(!pgmSend->pgm_send(item->data(),item->size()))
			//	LOG4_ERROR("Multicast failed.");
#endif
		}
	}
	delete item;
	return true;
}
bool AppServerImpl::sendmsg(const Message& msg,int cmd,unsigned int serialNum,Options options)
{	
	if(!IsOnLine())
	{
		SETLASTERROR(NOTCONNECTED);
		return false;
	}
	cmd = CommandConfig::getInstance().GetCommand(msg.GetTypeName().c_str());
    PackageHeader header;
	header.set_command(cmd);
	header.set_type(options.type);
	header.set_protocol(options.protocol);
	header.set_serialnum (serialNum);
	header.set_srcaddr( address);
	header.set_dstaddr(options.dstaddr);
	header.set_ismulticast(options.multicast);
	header.set_issequence(options.sequence);
	header.set_code (options.code);
	header.set_needreply(options.needreply);
	header.set_issync(options.issync);

	if (header.protocol() == PackageHeader::CodeType::JSON)
	{
		string body = pb2json(msg);
		SendDataItem * item = new SendDataItem();
		MessageUtil::serializePackageToString(header, body.data(), (unsigned int)(body.size()), false, 0, &item->strData);
		bool ret = postRawData(item);
		if (!ret)
			delete item;
		return ret;
	}
	
	std::string data;
	try
	{
	    if(!msg.AppendToString(&data))
		{
			SETLASTERROR(SERIALIZEFAILE);
		    return false;
		}
	}
	catch(FatalException err)
	{
		SETLASTERROR(SERIALIZEFAILE);
		LOG4_ERROR("Serialize error: %s",err.message().c_str());
		return false;
	}
	const int SIZE = clientCfg.multipagesize;
	if (data.size()<SIZE || header.type() != PackageHeader::Response || clientCfg.multipageswitch==false)
	{
	    SendDataItem * item=new SendDataItem();
		MessageUtil::serializePackageToString(header,data.data(),(unsigned int)(data.size()),(bool)(clientCfg.zlibswitch>0),clientCfg.zlibthreshold,&item->strData);
		bool ret= postRawData(item);
		if(!ret)
			delete item;
		return ret;
	}
	else
	{
		int num=data.size()/SIZE;
		if(num>10000)
		{
			LOG4_ERROR("Package is too big,size=%d", data.size());
			return false;
		}
		vector<SendDataItem *> vec;
		for(int i=0;i<num;i++)
		{
			SendDataItem * item=new SendDataItem();
			if(num>1)
			{
				header.set_ismultipage(true);
				header.set_issequence(true);
			}
			if(i<num-1)
			{
				header.set_pageno(i+1);
				MessageUtil::serializePackageToString(header,data.data()+i*SIZE,SIZE,(bool)(clientCfg.zlibswitch>0),clientCfg.zlibthreshold,&item->strData);
			}
			else
			{
				header.set_pageno(0);
				MessageUtil::serializePackageToString(header,data.data()+i*SIZE,(unsigned int)(data.size()-i*SIZE),(bool)(clientCfg.zlibswitch>0),clientCfg.zlibthreshold,&item->strData);
			}
			bool ret=false;
			ret= postRawData(item);
		    if(!ret)
			{
			    delete item;
				return false;
			}
		}
		return true;
	}
	return false;
}


///@brief 客户端发送异步消息的接口
unsigned int  AppServerImpl::PostMessage(const Message& msg,int appId,unsigned int dstaddr)
{
	if(!IsOnLine())
	{
		SETLASTERROR(NOTCONNECTED);
		return 0;
	}
	unsigned int SerialNum = asynnum();
	int cmd=CommandConfig::getInstance().GetCommand(msg.GetTypeName().c_str());
	if(!cmd)
	{
		SETLASTERROR(COMMANDDEFINEDERR);
		return 0;
	}
	Options opt;
	opt.dstaddr=dstaddr;
	if(sendmsg(msg,cmd,SerialNum,opt))
		return SerialNum;
	return 0;
}

AppServerImpl::TimeoutItem AppServerImpl::createTimeout(unsigned int timeval,int serial)
{
	TimeoutItem itm;
	struct timeval curr;
	sp_gettimeofday(&curr, NULL);
	curr.tv_sec+=timeval/1000;
	curr.tv_usec+=(timeval % 1000)*1000;
	if(curr.tv_usec>1000000)
	{
		curr.tv_sec++;
		curr.tv_usec-=1000000;
	}
	itm.msTimeout=timeval;
	itm.tv=curr;
	itm.serial=serial;
	return itm;
}
bool  AppServerImpl::PostMessage(MessagePtr msg,function<void(const MsgParams&)> msgcb,void* arg,unsigned int msTimeout,Options options)
{
	if(!IsOnLine())
	{
		SETLASTERROR(NOTCONNECTED);
		return false;
	}
	if (msTimeout ==0)
		msTimeout = clientCfg.msgtimeout;

	MsgCallbackInfo* cbInfo=NULL;
	unsigned int SerialNum = asynnum();
	if(msgcb!=nullptr)
	{
		cbInfo=new MsgCallbackInfo();
		cbInfo->cb=msgcb;
		cbInfo->params.req=msg;
		cbInfo->params.arg=arg;
		std::unique_lock<std::mutex> lock(cbMutex);
		mMapAsyncInfo[SerialNum]=MsgCallbackPtr(cbInfo);
	}
	//else
	//	LOG4_WARN("Callback not set,serial=%d",SerialNum);
	int cmd=CommandConfig::getInstance().GetCommand(msg->GetTypeName().c_str());
	if(!cmd)
	{
		SETLASTERROR(COMMANDDEFINEDERR);
		return false;
	}
	if(!sendmsg(*msg.get(),cmd,SerialNum,options))
	{
		this->getCallbackInfo(SerialNum,true);
		LOG4_ERROR("PostMessage failed,serial=%d",SerialNum);
		return false;
	}
	if(msgcb!=NULL)
	{
		TimeoutItem itm=createTimeout(msTimeout,SerialNum);
		std::unique_lock<std::mutex> lock(mMutex);
		minheap.push(itm);
		LOG4_DEBUG("Mini heap push,serial=%d,timeout=%d,heapSize=%d", SerialNum, msTimeout, minheap.size());
	}
	return true;
}

///@brief 客户端发送应答消息的接口
bool AppServerImpl::Reply(PackagePtr requestPackage,const Message& replyMsg)
{
	Options opt;
	opt.dstaddr=requestPackage->GetSrcAddr();
	opt.type=PackageHeader::Response;
	opt.version = requestPackage->header().version();
	opt.issync = requestPackage->header().issync();
	if(requestPackage->header().issequence())
		opt.sequence=true;

	return sendmsg(replyMsg,requestPackage->GetCommand(),requestPackage->GetSerialNum(),opt);
}
bool AppServerImpl::Reply(PackagePtr requestPackage,unsigned int errCode,string errMsg)
{
	MsgExpress::ErrMessage resp;
	resp.set_errcode(errCode);
	resp.set_errmsg(errMsg);
	Options opt;
	opt.type=PackageHeader::Response;
	opt.dstaddr=requestPackage->GetSrcAddr();
        opt.protocol = requestPackage->header().protocol();
	opt.version = requestPackage->header().version();
	opt.issync = requestPackage->header().issync();
	if(requestPackage->header().issequence())
		opt.sequence=true;
	return sendmsg(resp,0,requestPackage->GetSerialNum(),opt);
}

///@brief 客户端发送同步消息的接口
bool AppServerImpl::SendMessage(const Message& request, MessagePtr& response,unsigned int milliseconds,Options options)
{
	bool err=false;
	unsigned int SerialNum=0;
	do{
		if(!IsOnLine())
		{
			SETLASTERROR(NOTCONNECTED);
			err=true;
			break;
		}
		int cmd = CommandConfig::getInstance().GetCommand(request.GetTypeName().c_str());
		if(!cmd)
		{
			SETLASTERROR(COMMANDDEFINEDERR);
			err=true;
			break;
		}
		using namespace std::chrono;
		std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds> tp = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now());

		SerialNum = syncnum();
		if (!m_pSyncMsgArr->RegistSyncMsg(SerialNum))
		{
			SETLASTERROR(REGISTERR);
			err=true;
			break;
		}
		options.issync = true;
		if(!sendmsg(request,cmd,SerialNum,options))
		{
			LOG4_ERROR("send message error");
			m_pSyncMsgArr->UnRegistSyncMsg(SerialNum);
			err=true;
			break;
		}
		PackagePtr package;
		int ret=m_pSyncMsgArr->WaitForResponse(SerialNum,package, milliseconds);
		if(ret!=0 || package==NULL)
		{
			SETLASTERROR(TIMEOUT);
			err=true;
			LOG4_ERROR("send message error,code=%d",ret);
			break;
		}
		response=package->message();
		if(response.get()==NULL)
		{
			err=true;
			LOG4_ERROR("Null response,class:%s", package->classtype().c_str());
			break;
		}
		else if(response.get() && response->GetTypeName()==MsgExpress::ErrMessage::default_instance().GetTypeName())
		{
			MsgExpress::ErrMessage* errmsg=GetErrorMsg();
			errmsg->set_errcode(((MsgExpress::ErrMessage*)response.get())->errcode());
			errmsg->set_errmsg(((MsgExpress::ErrMessage*)response.get())->errmsg());
			err=true;
			response.reset();
			break;
		}
		std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds> tp1 = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now());
		auto tmp = std::chrono::duration_cast<std::chrono::milliseconds>(tp1 - tp);
		int off = (int)(tmp.count());
		if (off > 100)
			LOGGER_WARN("id=" << id << ",Send message cost too many time(" << off << " ms):" << package->DebugString().c_str());
			//LOG4_WARN("id=%d,Send message cost too many time(%d ms):%s",id,off,package->DebugString().c_str());
	}while(false);
	if(err)
	{
		MsgExpress::ErrMessage* errmsg=GetErrorMsg();
		LOG4_ERROR("id=%d,send message error,errinfo={errcode:0x%x,errmsg:%s},serial:%d,type:%s",id,errmsg->errcode(),errmsg->errmsg().c_str(),SerialNum,request.GetTypeName().c_str());
	}
	return !err;
}
bool AppServerImpl::Reply(const PackageHeader& reqHeader,const char* buff,unsigned int size)
{
	if(!IsOnLine())
	{
		SETLASTERROR(NOTCONNECTED);
		return false;
	}
	PackageHeader header;
	header.set_protocol(reqHeader.protocol());
	header.set_command(reqHeader.command());
	header.set_type ( PackageHeader::Response);
	header.set_serialnum(reqHeader.serialnum());
	header.set_srcaddr( address);
	header.set_dstaddr( reqHeader.srcaddr());
	header.set_options( reqHeader.options());
	SendDataItem* item=new SendDataItem();
	MessageUtil::serializePackageToString(header,buff,size,(bool)(clientCfg.zlibswitch>0),clientCfg.zlibthreshold,&item->strData);
	bool ret= postRawData(item);
	if(!ret)
	{
		delete item;
		LOG4_ERROR("Reply error,info:%s",reqHeader.DebugString().c_str());
	}
	return ret;
}
unsigned int  AppServerImpl::PostMessage(int cmd,const char* buff,unsigned int size,Options options)
{
	bool err=false;
	unsigned int SerialNum;
	do{
		if(!IsOnLine())
		{
			SETLASTERROR(NOTCONNECTED);
			err=true;
			break;
		}
		if(options.serial!=0)
			SerialNum=options.serial;
		else
			SerialNum = asynnum();

		PackageHeader header;
		header.set_protocol(options.protocol);
		header.set_command(cmd);
		header.set_type( options.type);
		header.set_serialnum (SerialNum);
		header.set_srcaddr ( address);
		header.set_dstaddr( options.dstaddr);
		header.set_hasext( options.ext);
		header.set_ismulticast(options.multicast);
		SendDataItem* item=new SendDataItem();
		if(MessageUtil::serializePackageToString(header,buff,size,(bool)(clientCfg.zlibswitch>0),clientCfg.zlibthreshold,&item->strData)==false)
		{
			SETLASTERROR(SERIALIZEFAILE);
			err=true;
		    break;
		}
		bool ret= postRawData(item);
	    if(!ret)
		{
			delete item;
			SETLASTERROR(SENDQUEUEFULL);
			err=true;
		    break;
		}
	}while(false);
	if(err)
		return 0;
	return SerialNum;

}
bool AppServerImpl::SendMessage(unsigned int cmd, const char* srcBuff, unsigned int srcSize, PackageHeader& retHeader, string& retData, unsigned int msTimeout, unsigned int dstaddr)
{
	if (msTimeout == 0)
		msTimeout = clientCfg.msgtimeout;
	bool err=false;
	do{
		if(!IsOnLine())
		{
			SETLASTERROR(NOTCONNECTED);
			err=true;
			break;
		}
		unsigned int SerialNum = syncnum();
		if (!m_pSyncMsgArr->RegistSyncMsg(SerialNum))
		{
			SETLASTERROR(REGISTERR);
			err=true;
			break;
		}
		PackageHeader header;
		header.set_command(cmd);
		header.set_type ( PackageHeader::Request);
		header.set_serialnum ( SerialNum);
		header.set_srcaddr ( address);
		header.set_dstaddr ( dstaddr);
		SendDataItem* item=new SendDataItem();
		if(MessageUtil::serializePackageToString(header,srcBuff,srcSize,(bool)(clientCfg.zlibswitch>0),clientCfg.zlibthreshold,&item->strData)==false)
		{
			m_pSyncMsgArr->UnRegistSyncMsg(SerialNum);
			SETLASTERROR(SERIALIZEFAILE);
			err=true;
		    break;
		}
		if(postRawData(item)==false)
		{
			m_pSyncMsgArr->UnRegistSyncMsg(SerialNum);
			delete item;
			SETLASTERROR(SENDQUEUEFULL);
			err=true;
		    break;
		}
		PackagePtr package;
		int ret = m_pSyncMsgArr->WaitForResponse(SerialNum, package, msTimeout);
		if(ret!=0 || package==NULL)
		{
			LOG4_ERROR("id=%d,send message error,code=%d",id,ret);
			SETLASTERROR(TIMEOUT);
			err=true;
		    break;
		}
		MessagePtr msg=package->message();
		if(msg && msg->GetTypeName()==MsgExpress::ErrMessage::default_instance().GetTypeName())
		{
			MsgExpress::ErrMessage* errmsg=GetErrorMsg();
			errmsg->set_errcode(((MsgExpress::ErrMessage*)msg.get())->errcode());
			errmsg->set_errmsg(((MsgExpress::ErrMessage*)msg.get())->errmsg());
			err=true;
		}
		const char* bodyData=package->GetBodyData();
		unsigned int bodySize=package->GetBodySize();
		memcpy(&retHeader,&package->header(),sizeof(retHeader));
		if(package->header().iszip())
		{
			bool unzipOK = MessageUtil::unCompress(bodyData,bodySize,package->header().compratio(),retData);
			if(!unzipOK)
			{
				SETLASTERROR(UNZIPFAILED);
				err=true;
				break;
			}
		}
		else
		{
			retData.assign(bodyData,bodySize);
		}
	}while(false);
	if(err)
	{
		MsgExpress::ErrMessage* errmsg=GetErrorMsg();
		LOG4_ERROR("id=%d,send message error,errinfo={errcode:0x%x,errmsg:%s},cmd=%d",id,errmsg->errcode(),errmsg->errmsg().c_str(),cmd);
	}
	return !err;
}
bool AppServerImpl::Subscribe2Broker(const MsgExpress::SubscribeData& subData, MessagePtr& response)
{
	return SendMessage(subData,response,clientCfg.msgtimeout);
}
//订阅消息
bool AppServerImpl::Subscribe(const MsgExpress::SubscribeData& subData, MessagePtr& response)
{
	if(clientCfg.udpReceiverCfg.isValid)//本地订阅
	{
		int topic=subData.topic();
		if(subData.condition_size()==0 && subData.excondition_size()==0)
		    mSimpleTopics.insert(topic);
		else
		{
			mOrderedTopics.insert(topic);
			mSubscriptionManager.subscribe(subData,0);
		}
		MsgExpress::CommonResponse * resp=new MsgExpress::CommonResponse();
		resp->set_retcode(0);
		resp->set_msg("");
		response.reset(resp);
		return true;
	}
	else
	    return Subscribe2Broker(subData,response);
}
bool AppServerImpl::UnSubscribe(const MsgExpress::UnSubscribeData& subData, MessagePtr& response)
{
	return SendMessage(subData,response,clientCfg.msgtimeout);
}
bool AppServerImpl::ComplexSubscribe(const MsgExpress::ComplexSubscribeData& subData, MessagePtr& response)
{
	return SendMessage(subData,response,clientCfg.msgtimeout);
}

bool AppServerImpl::Subscribe(int subid, const Message& msg, MessagePtr& response, AppAddress addr)
{
	MsgExpress::SimpleSubscription simpleSub;
	if (addr.Id > 0)
		simpleSub.set_useraddr(addr.Id);
	simpleSub.set_subid(subid);
	int cmd = CommandConfig::getInstance().GetCommand(msg.GetTypeName().c_str());
	simpleSub.set_topic(cmd);
	simpleSub.add_submsg(msg.SerializeAsString());
	return SendMessage(simpleSub,response,clientCfg.msgtimeout);
}
bool AppServerImpl::Subscribe(int subid, const vector<string>& topics, MessagePtr& response, AppAddress addr)
{
	if (topics.size()<1)
		return false;
	MsgExpress::ComplexSubscribeData *comSub = new MsgExpress::ComplexSubscribeData();
	for (size_t i = 0; i < topics.size(); i++)
	{
		MsgExpress::SubscribeData* sub = comSub->add_sub();
		sub->set_subid(subid++);
		sub->set_topic(CommandConfig::getInstance().GetCommand(topics[i].c_str()));
		if (addr.Id > 0)
			sub->set_useraddr(addr.Id);
	}
	return ComplexSubscribe(*comSub, response);

}
bool AppServerImpl::Subscribe(int subid, const vector<MessagePtr>& vecSubmsg, MessagePtr& response, AppAddress addr)
{
	MsgExpress::SimpleSubscription simpleSub;
	if (addr.Id > 0)
		simpleSub.set_useraddr(addr.Id);
	simpleSub.set_subid(subid);
	for(int i=0;i<vecSubmsg.size();i++)
	{
		int cmd = CommandConfig::getInstance().GetCommand(vecSubmsg[i]->GetTypeName().c_str());
		simpleSub.set_topic(cmd);
		simpleSub.add_submsg(vecSubmsg[i]->SerializeAsString());
	}
	return SendMessage(simpleSub,response,clientCfg.msgtimeout);
}
bool AppServerImpl::UnSubscribe(int subid, MessagePtr& response)
{
	MsgExpress::UnSubscribeData unSub;
	unSub.set_subid(subid);
	return SendMessage(unSub,response,clientCfg.msgtimeout);
}


bool AppServerImpl::Publish(MessagePtr msg,function<void(const MsgParams&)> msgcb,void* arg,unsigned int msTimeout)
{
	Options opt;
	opt.dstaddr=0;
	opt.type=PackageHeader::Publish;
	opt.sequence=true;
	if (msgcb != nullptr)
		opt.needreply = true;
	if (msTimeout == 0)
		msTimeout = clientCfg.msgtimeout;
	return PostMessage(msg,msgcb,arg,msTimeout,opt);
}
MessagePtr AppServerImpl::CreateMessage(const string& clazz)
{
	if(this->onCreateMessage)
	{
		MessagePtr msg = this->onCreateMessage(clazz);
		if(msg)
			return msg;
	}
	google::protobuf::Message* message=NULL;
	do
	{
		const google::protobuf::Descriptor* des=google::protobuf::DescriptorPool::generated_pool()->FindMessageTypeByName(clazz);
		if(!des)
			break;
		const google::protobuf::Message* prototype = google::protobuf::MessageFactory::generated_factory()->GetPrototype(des);
		if (!prototype)
			break;
		message = prototype->New();
	}while(false);
	return MessagePtr(message);
}
AppServerImpl::MsgCallbackPtr AppServerImpl::getCallbackInfo(unsigned int serialNum,bool erase)
{
	MsgCallbackPtr cbInfo;
	std::unique_lock<std::mutex> lock(cbMutex);
	unordered_map<unsigned int,MsgCallbackPtr>::const_iterator it=mMapAsyncInfo.find(serialNum);
	if(it!=mMapAsyncInfo.end())
	{
		cbInfo= it->second;
		if(erase)
		    mMapAsyncInfo.erase(it);
		LOG4_DEBUG("id=%d,callback,serial=%d ",id,serialNum);
	}
	return cbInfo;
}
void AppServerImpl::OnRawPackage(PackagePtr package)
{
    m_iHeartBeatTick=0;//收到包就认为网络是好的

	if (package->header().ismultipage() && package->IsResponse())//同步消息，要先处理,否则到上层函数OnRawPackage()里面处理
	{
		ProcessMultiPackage(package);
		if (!package->message())
			return;
	}

	if (package->IsResponse() && package->GetSerialNum()<MaxSyncSerialNum)
	{
		bool ret = ParseMessage(package);
		if (!ret)
			return;
		if (!m_pSyncMsgArr->CopyMessageAndSendSignal(package->GetSerialNum(), package))
		{
			LOGGER_ERROR("id=" << id << ",Response Package has no request package," << package->DebugString().c_str());
		}
		return;
	}
	if(package->header().protocol()!=0)
	{
		OnMessage(package);
		return;
	}

	MsgCallbackPtr cbInfo;
	if (package->IsResponse())
	{
		bool erase=(!package->header().ismultipage() || package->header().pageno()==0);
		cbInfo = getCallbackInfo(package->GetSerialNum(), true);
	}
    string errmsg;
	bool ok = ParseMessage(package, errmsg);
	if (!ok)
	{
		LOGGER_ERROR("Parse message failed,package:" << package->DebugString().c_str());
		if (package->IsRequest())
			this->Reply(package, 100, errmsg);
		else if (package->IsResponse())
		{
		}
		return;
	}
	if (package->classtype() == MsgExpress::KickOffApp::default_instance().GetTypeName())
	{
		LOG4_ERROR("id=%d was kick off",id);
		Close();
		return;
	}
	
	if(package->IsPublish())
	{
		OnPublish(package);
	}
	else if (package->IsResponse())
	{
		if(cbInfo!=NULL)
		{
			if(package->message()->GetTypeName()==MsgExpress::ErrMessage::default_instance().GetTypeName())
			{
				cbInfo->params.result=((MsgExpress::ErrMessage*)package->message().get())->errcode();
				cbInfo->params.errmsg=((MsgExpress::ErrMessage*)package->message().get())->errmsg();
			}
			else
			{
			    cbInfo->params.result=0;
				cbInfo->params.resp=package;
			}
			LOG4_DEBUG("id=%d Response ok,now callback, serial;%d", id, package->GetSerialNum());
			cbInfo->cb(cbInfo->params);
		}
		else
		    OnMessage(package);
	}
	else if (package->IsRequest())
	{
		if (clientCfg.udpReceiverCfg.isValid && clientCfg.udpReceiverCfg.groupNum > 1 && (package->GetSerialNum() - clientCfg.udpReceiverCfg.id) % clientCfg.udpReceiverCfg.groupNum != 0)
		{
		}
		else
			OnMessage(package);
	}
	else
		LOGGER_ERROR("No type:" << package->DebugString().c_str());
}
void AppServerImpl::OnMessage(PackagePtr package)
{
	if (package->IsRequest() && this->onRequest)
		this->onRequest(package);
	if(this->onMessage)
		this->onMessage(package);
	LOGGER_DEBUG("id=" << id << ",OnMessage:" << package->DebugString().c_str());
}

void AppServerImpl::OnPublish(PackagePtr package)
{
        if (package->message()->GetTypeName().compare(MsgExpress::RegisterService::default_instance().GetTypeName()) == 0)
	{
		MsgExpress::RegisterService* regservice = (MsgExpress::RegisterService*)package->message().get();
		CommandConfig::getInstance().Register(dynamic_pointer_cast<MsgExpress::RegisterService>(package->message()));
	}
        else if(package->message()->GetTypeName().compare(MsgExpress::PublishData::default_instance().GetTypeName())==0)
	{
		PublishPtr pubData=dynamic_pointer_cast<MsgExpress::PublishData>(package->message());
		if(clientCfg.udpReceiverCfg.isValid)//本地订阅
		{
			int topic=pubData->topic();
			bool isMyFood=false;
			if(mSimpleTopics.find(topic)!=mSimpleTopics.end())
				isMyFood=true;
			else if(mOrderedTopics.find(topic)!=mOrderedTopics.end())
			{
				isMyFood=mSubscriptionManager.IsMyFood(pubData.get(),0);
			}
			if(!isMyFood)
				return;
		}
		if(pubData->topic()==MsgExpress::TOPIC_TEST/* || pubData->topic()==11534340*/)
		{
			PublishHelperPtr helper=CreatePublishHelper(pubData.get(),true);
			const MsgExpress::DataItem* item=helper->GetItem(MsgExpress::KEY_TIME+1);
			if(item && item->ulval_size()>0)
			{
				uint64 sendtime=item->ulval(0);
				uint64 brokertime=this->GetBrokerTime();
				long off=(long)(brokertime-sendtime);
				//string str=pubData->ShortDebugString();
				if(off>1000)
					LOG4_WARN("Off:%d,brokertime:%lld,sendtime:%lld,content:",off,brokertime,sendtime);
			}
		}
	}
	else if (package->message()->GetTypeName().compare(MsgExpress::AppServerList::default_instance().GetTypeName()) == 0)
	{
		MsgExpress::AppServerList* list = (MsgExpress::AppServerList*)package->message().get();
		//LOG4_INFO("AppServerList:%s", list->ShortDebugString().c_str())
		if (list->ismasterslavemode())
		{
			unsigned int addr = this->GetAddress();
			if (list->brokerismaster())
			{
				if (list->addrs_size() == 1 || list->addrs(0) == addr)
				{
					if (serverMode != Event::Upgrade2Master)
					{
						serverMode=Event::Upgrade2Master;
						LOG4_INFO("I am master server");
						ReportServerEvent(1, "I am master server");
						OnEvent(Event::Upgrade2Master);
					}
				}
				else if (list->addrs_size() > 1 && list->addrs(0) != addr)
				{
					if (serverMode != Event::Downgrade2Slave)
					{
						serverMode = Event::Downgrade2Slave;
						LOG4_INFO("I am slave server");
						ReportServerEvent(1, "I am slave server");
						OnEvent(Event::Downgrade2Slave);
					}
				}
			}
			else
			{
				if (serverMode != Event::Downgrade2Standby)
				{
					serverMode = Event::Downgrade2Standby;
					LOG4_INFO("I am standby server");
					ReportServerEvent(1, "I am standby server");
					OnEvent(Event::Downgrade2Standby);
				}
			}
			return;
		}
	}
	if(this->onPublish)
	{
	    this->onPublish(package);
	}
}

void AppServerImpl::OnEvent(int eventId)
{
	if(this->onEvent)
	    this->onEvent(eventId);
	LOG4_INFO("id=%d,OnEvent:%d",id,eventId);
}
