
#include <time.h>
#include "desktopGateway.h"
#include "../inc/logger.h"
#include "../core/command_config.h"
#include "../core/messageutil.h"
#include "../client/publishdata.h"
#include "../proxy/proxyclient_config.h"

DesktopGatewayProxy::DesktopGatewayProxy()
{
	sp_thread_mutex_init(&mMutex,NULL);
}

DesktopGatewayProxy::~DesktopGatewayProxy()
{
	sp_thread_mutex_destroy(&mMutex);
}
bool DesktopGatewayProxy::StartA(const char* configFile,ProxyCFG* proxyConfig)
{
	string xml_file(configFile);
	ProxyClientConfig &pclientconf=ProxyClientConfig::Instance();
	if(!pclientconf.LoadFile(xml_file))
	{   
		LOG4_ERROR("read proxy client xml config file error");
		return false;
	} 

	ClientCFG cfg = pclientconf.clientCfg;
	if(m_pServerA->Initialize(&cfg,proxyConfig)!=0)
	{
		LOG4_ERROR("A Connect to Gateway[%s:%d] failed.",cfg.serverGroup[0].serverIP.c_str(),cfg.serverGroup[0].port);
		return false;
	}
	return true;
}
bool DesktopGatewayProxy::StartB(const char* configFile)
{
	string xml_file(configFile);
	cfgfile=xml_file;
	ProxyClientConfig &pclientconf=ProxyClientConfig::Instance();
	if(!pclientconf.LoadFile(xml_file))
	{   
		LOG4_ERROR("read proxy client xml config file error");
		return false;
	} 

	ClientCFG cfg = pclientconf.clientCfgB;
	if(m_pServerB->Initialize(&cfg,NULL)!=0)
	{
		LOG4_ERROR("B Connect to Data bus[%s:%d] failed.",cfg.serverGroup[0].serverIP.c_str(),cfg.serverGroup[0].port);
		return false;
	}
	return true;
}
void DesktopGatewayProxy::OnRawPackageA(PackagePtr package)
{
	if(package->IsRequest())
	{
		LOG4_ERROR("Request from side A,content:%s",package->DebugString().c_str());
		return;
	}
	MsgExpressProxy::OnRawPackageA(package);
}
void DesktopGatewayProxy::ProcessMyMessage(PackagePtr package)
{
	if(!MessageUtil::parse(package.get()))
	{
		LOG4_ERROR("Parse failed,cmd:0x%x,class name:%s", package->GetCommand(), package->classtype().c_str());
		m_pServerB->Reply(package,Gateway::ERRCODE_PARSEFAILED,Gateway::ERRMSG_PARSEFAILED);
		return;
	}
	/*if(package->classtype()==DesktopGateway::ProxySettings::default_instance().GetTypeName())
	{
		DesktopGateway::ProxySettings* proxy=(DesktopGateway::ProxySettings*)package->message().get();
		ProxyCFG cfg;
		cfg.enable=proxy->enable();
		cfg.type=(ProxyType)proxy->type();
		cfg.address=proxy->address();
		cfg.port=proxy->port();
		cfg.user=proxy->username();
		cfg.pass=proxy->passwd();
		cfg.domain=proxy->domain();
		bool ret=true;
		LOG4_INFO("Begin connect to gateway");
		if(!m_pServerA->IsLogin())
		    ret=StartA(cfgfile.c_str(),&cfg);
		else
			LOG4_INFO("Has connected to gateway before");
		DesktopGateway::CommonResponse resp;
		resp.set_msg("");
		if(ret)
		{
			LOG4_INFO("Connect to gateway success");
			resp.set_retcode(0);
		}
		else
		{
			LOG4_ERROR("Connect to gateway failed,info:%s",proxy->ShortDebugString().c_str());
			resp.set_retcode(-1);
			resp.set_msg("Connect to gateway failed");
		}
		m_pServerB->Reply(package,resp);
	}*/
}

void DesktopGatewayProxy::ProcessPublish(PackagePtr package)
{
	bool ret=MessageUtil::parse(package.get());
	if(!ret)
	{
		LOG4_ERROR("Parse failed,cmd:0x%x,class name:%s",package->GetCommand(),package->classtype().c_str());
		return;
	}
	MsgExpress::PublishData* pubData=(MsgExpress::PublishData*)package->message().get();
}
void DesktopGatewayProxy::OnRawPackageB(PackagePtr package)
{
	if(package->IsResponse())
	{
		LOG4_ERROR("Response from side B,content:%s",package->DebugString().c_str());
		return;
	}
	else if(package->IsRequest())
	{
		if (CommandConfig::getInstance().GetServiceId(package->GetCommand()) == (unsigned int)DesktopGateway::APPID)
		{
			ProcessMyMessage(package);
			return;
		}
		else
		{
			MsgExpressProxy::OnRawPackageB(package);
		}
	}
	else if(package->IsPublish())
	{
		return;//B端不向A端推送东西
	}
}
void DesktopGatewayProxy::OnEventA(int eventId)
{
	LOG4_ERROR("GatewayProxy:status changed on side A,status:%d",eventId);
}
void DesktopGatewayProxy::OnEventB(int eventId)
{
	LOG4_ERROR("GatewayProxy:status changed on side B,status:%d",eventId);
}
