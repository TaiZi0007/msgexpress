#include "log.h"
#include <stdarg.h>
#include <signal.h>
#include <tchar.h>
#include <io.h>
#include <direct.h>

#if defined(_WIN32) || defined(_WINDOWS)
#include <windows.h>
#else
#include <syscall.h>
#endif


namespace sslog{

unsigned int current_thread_id()
{
#if defined(_WIN32) || defined(_WINDOWS)
		return  ::GetCurrentThreadId() ;
#else
		return (uint32_t) syscall(__NR_gettid) ;
#endif			
}

static char hex_map[16] = {
	'0'	,	'1'	,	'2'	,	'3'	,
	'4'	,	'5'	,	'6'	,	'7'	,
	'8'	,	'9'	,	'A'	,	'B'	,
	'C'	,	'D'	,	'E'	,	'F'
} ;

int hex_sprintf(char * strbuf , char val)
{
	strbuf[0] = hex_map[(val >> 4) & 0XF] ;
	strbuf[1] = hex_map[val & 0XF] ;
	strbuf[2] = '\0' ;
	return 2 ;
}

////////////////////////////////////////////////////////////////////////////////
class CScopedLock{
	LPCRITICAL_SECTION	m_pSection;
public:
	CScopedLock(LPCRITICAL_SECTION section):m_pSection(section)
	{
		if(m_pSection)
			EnterCriticalSection(m_pSection);
	}
	~CScopedLock()
	{
		if(m_pSection)
			LeaveCriticalSection(m_pSection);
	}
};
void logger::init()
{
	file_ = NULL ;
	level_ = LOG_LEVEL::LOG_INFO ;
	tty_level_ = LOG_LEVEL::LOG_INFO ;

	name_ = "databus" ;

	max_size_ = 0x70123456 ;
}

logger::logger()
{
	InitializeCriticalSection(&guard_);
	init() ;
	open() ;
	
}

logger::logger(const char *name)
{
	InitializeCriticalSection(&guard_);
	init() ;
	name_ = name ;

	open() ;
	
}
void logger::set_name(const char *name)
{
	name_ = name ;
}
logger::~logger()
{
	close() ;
	DeleteCriticalSection(&guard_);
}

void logger::clear_log()
{
	wchar_t szAppPath[MAX_PATH]={0};
	if(!GetModuleFileName(NULL,szAppPath,MAX_PATH))
		return;

	size_t nlen = _tcslen(szAppPath)-1;
	while(szAppPath[nlen--]!='\\');
	szAppPath[nlen+2]='\0';
	_tcscat(szAppPath,_T("Log\\"));
	
	//calculate the date 30 days ago
	time_t t = time(&t);
	t -= 30*86400;
	struct tm *pt = localtime(&t);
	TCHAR szLastLog[16]={0};
	_stprintf_s(szLastLog,_countof(szLastLog),_T("databus%04d%02d%02d.log"),pt->tm_year+1900,pt->tm_mon+1,pt->tm_mday);

	//search all log files less than 30 days
	TCHAR szSearchPattern[MAX_PATH]={0};
	TCHAR szFullFilePath[MAX_PATH]={0};
	_stprintf_s(szSearchPattern,MAX_PATH,_T("%s*.log"),szAppPath);	//make search pattern

	WIN32_FIND_DATA fdata;
	HANDLE hFind;
	hFind = FindFirstFile(szSearchPattern,&fdata);
	if(hFind == INVALID_HANDLE_VALUE)
		return;
	do 
	{
		if(_tcscmp(fdata.cFileName,szLastLog)<=0)
		{
			_stprintf_s(szFullFilePath,MAX_PATH,_T("%s%s"),szAppPath,fdata.cFileName);
			DeleteFile(szFullFilePath);
		}
	} while (FindNextFile(hFind,&fdata));
	FindClose(hFind);
}

void logger::open()
{
	char file_name[256] ;

	today_.calc_now() ;
	if( 0 != _access( "Log", 0 ) )
		_mkdir( "Log" );

	sprintf_s(file_name , sizeof(file_name), ".\\Log\\%s%04d%02d%02d.log" ,name_.c_str() , today_.year() , today_.month() , today_.day()) ;

    //boost::unique_lock<boost::shared_mutex> lock(guard_) ;
	CScopedLock		lock(&guard_);
	if(file_ != NULL)
		return ;

	file_name_ = file_name ;
	file_ = fopen(file_name , "a+b") ;	
}

void logger::close()
{
	CScopedLock		lock(&guard_);
	if(file_)
	{
		fflush(file_) ;
		fclose(file_) ;

		file_ = NULL ;
	}
}

int logger::set_level(int level)
{
	level_ = level ;
	return level_ ;
}

int logger::set_stdout_level(int level)
{
	return set_tty_level(level) ;
}

int logger::set_tty_level(int level)
{
	tty_level_ = level ;
	return tty_level_ ;	
}

int logger::get_level() const
{
	return level_ ;
}

int logger::get_tty_level() const
{
    return tty_level_ ;
}

int logger::set_max_size(size_t max_size)
{
	max_size_ = max_size ;
	return max_size_ ;	
}

const char *trim_file_name(const char *file_name)
{
	const char *pstr ;

	if(file_name == NULL)
		return NULL ;

	pstr = file_name + strlen(file_name) ;
	while(pstr != file_name)
	{
		if(*pstr == '/' || *pstr == '\\')
		{
			pstr++ ;
			break ;
		}
		pstr-- ;
	}

	return pstr ;
}

const char *log_level[8] =
{
	"emerg",
	"alert",
	"crit",
	"error",
	"warn",
	"notic",
	"info",
	"debug"
};

void logger::write_message(const char * message)
{
	//boost::shared_lock<boost::shared_mutex> lock(guard_) ;
	CScopedLock		lock(&guard_);
	if(file_ == NULL || message == NULL)
		return ;


	int fsize = fprintf(file_ ,"%s \n", message);
	if(fsize > 0)
	{
		fflush(file_);
	}
}

int logger::write(const char *file_name , int line , int level , const char* func, const char *format , ...)
{
	va_list ap;
	char message[MAX_LOG_LEN] = {'\0'} , *pos ;
	int len = 0 ;

	if(level > level_)
		return 0 ;

	//实现日志按每日存放的功能！
	if(today_ != date::today())
	{
		close() ;
		open() ;
	}

	utime now(true) ;
	if(level >= 0 && level <= LOG_LEVEL::LOG_LEVEL_MAX)
	{
		 len = sprintf_s(message , sizeof(message), "%s [%s][%u]" , 
			 now.to_string().c_str() , log_level[level] , current_thread_id());
	}
	else
	{
		 len = sprintf_s(message , sizeof(message), "%s [%u]" , 
			 now.to_string().c_str() ,current_thread_id() );
	}

	pos = message + len ;
	len = MAX_LOG_LEN - len - 30;

	va_start(ap, format);
	vsnprintf(pos, len , format, ap);
	va_end(ap);

	write_message(message) ;

	if(tty_level_ >= level)
	{
		printf("%s \n", message) ;
	}

	return 0 ;		
}

int logger::writebuf(const char *file_name , int line , int level , const void *buf , int size)
{
	char message[MAX_LOG_LEN] = {'\0'} , *pos ;
	int len = 0 ;

	if(level > level_)
		return 0 ;

	//实现日志按每日存放的功能！
	if(today_ != date::today())
	{
		close() ;
		open() ;
	}

	utime now(true) ;
	if(level >= 0 && level <= LOG_LEVEL::LOG_LEVEL_MAX)
	{
		 len = sprintf_s(message , sizeof(message), "%s [%s][%u] " , 
			 now.to_string().c_str() , log_level[level] , current_thread_id() );
	}
	else
	{
		 len = sprintf_s(message , sizeof(message), "%s [%u] " , 
			 now.to_string().c_str() ,current_thread_id() );
	}

	pos = message + len ;
	len = MAX_LOG_LEN - len ;

	const unsigned char * pbuf = (const unsigned char *)buf ;
	int i , mlen = 0 , lineno = 0 ;
	int last_line_pos = 0 , tlen = 0;
	int fsize = 0 ;
	/*
		2011-07-01
		必须限制最大缓冲区大小，为1M
	*/
	for(i = 0 ; i < size && i < 0x100000 ; i++)
	{
		if((i % 32) == 0 || mlen == 0)
		{
			if((i % 1024) == 0)
			{
				//每32行，共1024个字节，打印一段
				tlen = sprintf_s(pos + mlen , message+MAX_LOG_LEN-pos-mlen,"[%d / %d] \n" , i , size) ;
				mlen += tlen ;
			}
			sprintf_s(pos + mlen , message+MAX_LOG_LEN-pos-mlen, "\t[%03d] " , lineno) ;
			mlen += 7 ;
		}

		//sprintf(pos + mlen , " %02hhX" , pbuf[i]) ;
		hex_sprintf(pos + mlen , pbuf[i]) ;
		mlen += 2 ;
		pos[mlen++] = ' ' ;

		if((i + 1) % 32 == 0)
		{
			last_line_pos = mlen ;
			pos[mlen++] = '\n' ;
			lineno++ ;
		}

		if(((i + 1) % 1024) == 0 || mlen >= (len - 10))
		{
			//缓冲区不足，直接打印到最后一行
			pos[last_line_pos] = '\0' ;

			/*
			fsize = fprintf(file_ ,"%s \n", message);
			if(fsize > 0)
			{
				fflush(file_);
			}
			*/
			write_message(message) ;

			if(tty_level_ >= level)
			{
				printf("%s \n", message) ;
			}

			mlen -= last_line_pos + 1 ;
			if(mlen > 0)
			{
				memcpy(pos , pos + last_line_pos + 1 , mlen) ;
			}
			last_line_pos = 0 ;
		}
	}

	if(mlen > 0)
	{
		pos[mlen] = '\0' ;

		/*
		fsize = fprintf(file_ ,"%s \n", message);
		if(fsize > 0)
		{
			fflush(file_);
		}
		*/
		write_message(message) ;

		if(tty_level_ >= level)
		{
			printf("%s \n", message) ;
		}
	}

	if(i >= 0x100000)
	{
		sprintf_s(pos , message+MAX_LOG_LEN-pos, "buf size [%d] , overflow 1M" , size) ;
		/*
		fprintf(file_ , "%s \n" , message) ;
		fflush(file_) ;
		*/
		write_message(message) ;
	}

	return 0 ;			
}

}

sslog::logger *default_logger = NULL;

sslog::logger * get_logger() 
{
    return default_logger ;
}

class default_logger_auto_singleton {
public:
    default_logger_auto_singleton()
    {
	    if(default_logger == NULL)
	    {
		    default_logger = new sslog::logger() ;
	    }
    }

    ~default_logger_auto_singleton()
    {
	    if(default_logger != NULL)
	    {
		    delete default_logger ;
		    default_logger = NULL ;
	    }
    }
} ;
void MakeAssertText(char* buf,int len,int cap,char* smt,char* stacks,char* file,int line){
	// 假设cap 足够大,通过宏ASSERTBUFSIZE来控制,默认是2048
	char fmt[1024];memset(fmt,0,1024);
	memmove(fmt,buf,len);

	char progname[MAX_PATH];memset(progname,0,MAX_PATH);
	if ( !GetModuleFileNameA( NULL, progname, MAX_PATH ))
		strcpy_s( progname, MAX_PATH,"<program name unknown>");
	char linestr[32];memset(linestr,0,32);
	_snprintf_s(linestr,32,"%d/%d",line,GetCurrentThreadId());

	memset(buf,0,cap);
	strcat_s(buf,cap,"Assertion failed!\n");

	strcat_s(buf,cap,"\nProgram\t: ");
	char* pname = strrchr(progname,'\\');pname=pname==NULL?strrchr(progname,'/'):pname;
	pname = pname==NULL?progname:pname;
	strcat_s(buf,cap,pname);

	strcat_s(buf,cap,"\nFile\t: ");
	char* fname=strrchr(file,'\\');fname=fname==NULL?strrchr(file,'/'):fname;
	fname = fname==NULL?file:fname;
	strcat_s(buf,cap,fname);

	strcat_s(buf,cap,"\nThread\t: ");strcat_s(buf,cap,linestr);

	strcat_s(buf,cap,"\nExpr\t: ");strcat_s(buf,cap,smt);
	strcat_s(buf,cap,"\nUser Text\t: ");strcat_s(buf,cap,fmt);
	strcat_s(buf,cap,"\nCallStack\t: \n");strcat_s(buf,cap,stacks);

	strcat_s(buf,cap,"\nFor information on how your program can cause an assertion");
	strcat_s(buf,cap,"\nfailure, see the Visual C++ documentation on asserts");

	strcat_s(buf,cap,"\n\n(Press Retry to debug the application - JIT must be enabled)");
}
void DbgError(char* smt,char* file,char* func,int line,const char* format,...)
{
#ifndef _DEBUG
	LOGEMERG("ASSERTS FAILED AT %s:%d",file,line);
	return;
#endif
#define ASSERTBUFSIZE 2048
	const size_t tsize = 1024;
	char buffer[tsize];
	char assertbuf[ASSERTBUFSIZE];

	memset(buffer,0,tsize);
	memset(assertbuf,0,ASSERTBUFSIZE*sizeof(char));

	va_list args;
	int ret;  
	va_start (args, format);                         
	const char* pFileShort = strrchr(file,'\\');
	if(pFileShort==0)
		pFileShort = file;
	else 
		pFileShort++;

	ret = _snprintf_s(buffer,tsize,"%s:%d:%s() ",pFileShort,line,func);


	ret = vsprintf_s(assertbuf,ASSERTBUFSIZE, format, args); 
	memmove_s(buffer+ret,tsize-ret,assertbuf,ret);


	va_end (args);                                   
	

	char call_stacks[1024];memset(call_stacks,0,1024);
	MakeAssertText(assertbuf,ret,ASSERTBUFSIZE,smt,call_stacks,file,line);
	LOGEMERG(assertbuf);

	UINT utype = MB_ABORTRETRYIGNORE|MB_ICONHAND|MB_SETFOREGROUND|MB_TASKMODAL;
	const char* title = "Microsoft Visual C++ Runtime Library";

	int nCode = MessageBoxA(NULL,assertbuf,title,utype);
	if(nCode==IDABORT){
		LOGEMERG("You choose abort and exit");
		raise(SIGABRT);
		_exit(3);
	}
	if(nCode==IDRETRY){
		LOGEMERG("You choose retry");
		DebugBreak();
		return;
	}
	if(nCode==IDIGNORE)
	{
		LOGEMERG("this assert failure has been igored");
		return;
	}
	abort();
}
default_logger_auto_singleton default_logger_singleton ;
