
#include "datetime.h"

#if defined(_WIN32) || defined(_WINDOWS) 
#include <windows.h>

#define localtime_r(ti , tm) localtime_s(tm , ti)
#define gmtime_r(ti , tm) gmtime_s(tm , ti)

#else
#include <sys/time.h>
#endif


namespace sslog{

time_t make_time(int year , int month , int day , int hour = 0, int minute = 0, int second = 0)
{
	struct tm tm ;
	memset(&tm , 0 , sizeof(tm)) ;

	tm.tm_year = year - 1900 ;
	tm.tm_mon = month - 1 ;
	tm.tm_mday = day ;

	tm.tm_hour = hour ;
	tm.tm_min = minute ;
	tm.tm_sec = second ;	

	return ::mktime(&tm) ;
}

time_t date::calc_date(time_t time , int& year , int& month , int& day , int& wday)
{
	struct tm tm ;

	::localtime_r(&time , &tm) ;

	year = tm.tm_year + 1900 ;
	month = tm.tm_mon + 1 ;
	day = tm.tm_mday ;
	wday = tm.tm_wday ;
	if(wday == 0)
		wday = 7 ;

	tm.tm_hour = 0 ;
	tm.tm_min = 0 ;
	tm.tm_sec = 0 ;

	return mktime(&tm) ;
}

date::date(int year , int month , int day)
{
	reset() ;
	time_t t = make_time(year , month , day) ;
	time_ = calc_date(t , year_ , month_ , day_ , wday_) ;
	calc_stage_ = 1 ;	
}

void date::reset()
{
	time_ = 0 ;
	year_ = 0 ;
	month_ = 0 ;
	day_ = 0 ;
	wday_ = 0 ;
	calc_stage_ = 0 ;
}

time_t date::time()
{
	return time_ ;
}

time_t date::gmttime()
{
	return (time_ - (date::timezone() * 3600)) ;
}

bool date::calc_now()
{
	time_ = calc_date(::time(NULL) , year_ , month_ , day_ , wday_) ;
	calc_stage_ = 1 ;

	return true ;
}

date::date()
{
	calc_now() ;
}

date::date(time_t time)
{
	reset() ;

	time_ = calc_date(time , year_ , month_ , day_ , wday_) ;
	calc_stage_ = 1 ;	
}

date& date::operator=(time_t ts)
{
	reset() ;
	time_ = calc_date(ts , year_ , month_ , day_ , wday_) ;	
	calc_stage_ = 1 ;

	return (*this) ;
}

bool date::operator==(time_t ts)
{
	date d(ts) ;
	return (*this) == d ;
}

bool date::operator!=(time_t ts)
{
	date d(ts) ;
	return (*this) != d ;	
}

bool date::operator==(const date& d)
{
	return ((year_ == d.year_) && (month_ == d.month_) && (day_ == d.day_)) ;
}

bool date::operator!=(const date& d)
{
	return !((*this) == d) ;
}

bool date::operator<(const date& d)
{
	return (time_ < d.time_) ;
}

bool date::operator<=(const date& d)
{
	return (time_ <= d.time_) ;
}

bool date::operator>(const date& d)
{
	return (time_ > d.time_) ;
}

bool date::operator>=(const date& d)
{
	return (time_ >= d.time_) ;
}


std::string& date::to_string()
{
	if(calc_stage_ < 2)
	{
		char str[256] = {'\0'} ;
		sprintf_s(str , sizeof(str),"%04d%02d%02d" , year_ , month_ , day_) ;
		datestr_ = str ;

		calc_stage_ = 2 ;
	}

	return datestr_ ;
}

date& date::today()
{
	static date date_today ;
	static time_t last_time = date_today.time() ;

	time_t now = ::time(NULL) ;
	//if((now / 86400) > (last_time / 86400))
	if(now - last_time >= 86400)
	{
		date_today.calc_now() ;
		last_time = date_today.time() ;
	}

	return date_today ;
}


int date::timezone()
{
	static int our_timezone = - 1 ;

	if(our_timezone < 0)
	{
		time_t now = ::time(NULL) ;
		struct tm ltm , gtm ;

		localtime_r(&now , &ltm) ;
		gmtime_r(&now , &gtm) ;


		our_timezone = (ltm.tm_hour + 24 - gtm.tm_hour) %24 ;
	}

	return our_timezone ;
}

date& date::next_day(int dc)
{
	time_t t = time_ + (86400 * dc) ;
	time_ = calc_date(t , year_ , month_ , day_ , wday_) ;	

	calc_stage_ = 1 ;
	return (*this) ;
}

utime::utime(bool need_calc)
{
	if(need_calc)
		calc_now() ;
	else
		reset() ;
}

void utime::reset()
{
	hour_ = 0 ;
	minute_ = 0 ;
	second_ = 0 ;
	usec_ = 0 ;

	calc_stage_ = 0 ;
}

utime::~utime()
{
	//
}

void utime::calc_now(int64_t micro)
{
	time_t sec = 0 ;	
	if(micro == 0)
	{
#if defined(WIN32) || defined(_WINDOWS) 
		FILETIME ft ;

		GetSystemTimeAsFileTime(&ft) ;

		/*
			2011-03-08
			参考BOOST的microsec_time_clock.hpp	将FILETIME转化为毫秒

			shift is difference between 1970-Jan-01 & 1601-Jan-01
			in 100-nanosecond intervals 
		*/
		const uint64_t shift = 116444736000000000ULL; // (27111902 << 32) + 3577643008
		uint64_t caster = ft.dwHighDateTime  ;
		caster = (( caster << 32 ) + ft.dwLowDateTime - shift) / 10 ;

		usec_ = (int64_t)(caster % 1000000UL) ;
		sec = (int64_t)(caster  / 1000000) ;
#else
		struct timeval tv ;
		::gettimeofday(&tv , NULL) ;

		sec = tv.tv_sec ;
		usec_ = tv.tv_usec ;
#endif
	}
	else
	{
		sec = (time_t)(micro / 1000000) ;
		usec_ = (int)(micro % 1000000) ;
	}

	if(sec > 864000)
	{
		date d(sec) ;
		sec -= d.time() ;
	}

	hour_ = (int)(sec / 3600) ;
	minute_ = (int)((sec % 3600) / 60) ;
	second_ = (int)(sec % 60) ;		

	calc_stage_ = 1;
}

std::string& utime::to_string()
{
	if(calc_stage_ < 2)
	{
		char str[256] ;
		sprintf_s(str , sizeof(str), "%02d:%02d:%02d.%06d" , hour_ , minute_ , second_ , usec_) ;
		usecstr_ = str ;		
	}

	return usecstr_ ;
}

int64_t utime::now()
{
	utime ut(true) ;
	return (((ut.hour() * 3600 + ut.minute() * 60 + ut.second())) * 1000000 + ut.usec()) ;
}

const utime& utime::operator=(const utime& src)
{
	hour_ = src.hour_ ;
	minute_ = src.minute_ ;
	second_ = src.second_ ;
	usec_ = src.usec_ ;

	usecstr_ = src.usecstr_ ;
	calc_stage_ = src.calc_stage_ ;

	return (*this) ;	
}

}

