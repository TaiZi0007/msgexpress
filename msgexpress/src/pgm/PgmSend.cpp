#include "PgmSend.h"

PgmSend::PgmSend() :  use_multicast_loop(TRUE), port(0), use_fec(TRUE), rs_k(8), rs_n(255), max_tpdu(1500), max_rte(400*1000), sqns(100), endpoint(NULL), sock(NULL) {
	
	std::setlocale(LC_ALL, "");
}
bool PgmSend::init(const char *address, unsigned int port, unsigned int rate_times )
{
    network=address;
	udp_encap_port=port;
	max_rte=rate_times*400*1000;
	cpgm::pgm_error_t *pgm_err = NULL;
	if (!cpgm::pgm_init(&pgm_err)) {
		std::cerr << "Unable to start PGM engine: " << pgm_err->message << std::endl;
		cpgm::pgm_error_free(pgm_err);
		return false;
	}
	
	if (use_fec && ( !rs_n || !rs_k )) {
		std::cerr << "Invalid Reed-Solomon parameters RS(" << rs_n << "," << rs_k << ")." << std::endl;
		return false;
	}

	if (!create_sock()) {
		std::cerr << "Create PGM send sock failed." << std::endl;
		return false;
	}
	return true;
}
PgmSend::~PgmSend() {
	if (sock) {
		sock->close (TRUE);
		sock = NULL;
	}
	cpgm::pgm_shutdown();
}

bool PgmSend::pgm_send(const char *buf,unsigned int size) {
	int status = sock->send(buf, size, NULL);
	if (cpgm::PGM_IO_STATUS_NORMAL != status) {
		std::cerr << "pgm_send() failed.." << std::endl;
		return false;
	} else {
		return true;
	}
}

bool PgmSend::create_sock(void) {
	struct cpgm::pgm_addrinfo_t *res = NULL;
	cpgm::pgm_error_t *pgm_err = NULL;
	//sa_family_t sa_family = AF_UNSPEC;
	sa_family_t sa_family = AF_INET;
	
	/* parse network parameter into PGM socket address structure */
	if (!cpgm::pgm_getaddrinfo (network.c_str(), NULL, &res, &pgm_err)) {
		std::cerr << "Parsing network parameter: " << pgm_err->message << std::endl;
		goto err_abort;
	}

	sa_family = res->ai_send_addrs[0].gsr_group.ss_family;

	sock = new ip::pgm::socket();

	if (udp_encap_port) {
		if (!sock->open(sa_family, SOCK_SEQPACKET, IPPROTO_UDP, &pgm_err)) {
			std::cerr << "Creating PGM/UDP socket: " << pgm_err->message << std::endl;
			goto err_abort;
		}
		sock->set_option(IPPROTO_PGM, cpgm::PGM_UDP_ENCAP_UCAST_PORT, &udp_encap_port, sizeof(udp_encap_port));
		sock->set_option(IPPROTO_PGM, cpgm::PGM_UDP_ENCAP_MCAST_PORT, &udp_encap_port, sizeof(udp_encap_port));
	} else {
		if (!sock->open(sa_family, SOCK_SEQPACKET, IPPROTO_PGM, &pgm_err)) {
			std::cerr << "Creating PGM/IP socket: " << pgm_err->message << std::endl;
			goto err_abort;
		}
	}

	{
		/* Use RFC 2113 tagging for PGM Router Assist */
		const int no_router_assist = 0;
		sock->set_option (IPPROTO_PGM, cpgm::PGM_IP_ROUTER_ALERT, &no_router_assist, sizeof(no_router_assist));
	}

	cpgm::pgm_drop_superuser();

	{
		/* set PGM parameters */
		const int send_only = 1,
			  ambient_spm = pgm_secs (30),
			  heartbeat_spm[] = { pgm_msecs (100),
					      pgm_msecs (100),
       		                              pgm_msecs (100),
					      pgm_msecs (100),
					      pgm_msecs (1300),
					      pgm_secs  (7),
					      pgm_secs  (16),
					      pgm_secs  (25),
					      pgm_secs  (30) };

		sock->set_option(IPPROTO_PGM, cpgm::PGM_SEND_ONLY, &send_only, sizeof(send_only));
		sock->set_option(IPPROTO_PGM, cpgm::PGM_MTU, &max_tpdu, sizeof(max_tpdu));
		sock->set_option(IPPROTO_PGM, cpgm::PGM_TXW_SQNS, &sqns, sizeof(sqns));
		sock->set_option(IPPROTO_PGM, cpgm::PGM_TXW_MAX_RTE, &max_rte, sizeof(max_rte));
		sock->set_option(IPPROTO_PGM, cpgm::PGM_AMBIENT_SPM, &ambient_spm, sizeof(ambient_spm));
		sock->set_option(IPPROTO_PGM, cpgm::PGM_HEARTBEAT_SPM, &heartbeat_spm, sizeof(heartbeat_spm));
	}
	if (use_fec) {
		struct cpgm::pgm_fecinfo_t fecinfo; 
		fecinfo.block_size = rs_n;
		fecinfo.proactive_packets = 0;
		fecinfo.group_size = rs_k;
		fecinfo.ondemand_parity_enabled	= TRUE;
		fecinfo.var_pktlen_enabled = TRUE;
		sock->set_option(IPPROTO_PGM, cpgm::PGM_USE_FEC, &fecinfo, sizeof(fecinfo));
	}
	
	/* create global session identifier */
	//endpoint = new ip::pgm::endpoint(DEFAULT_DATA_DESTINATION_PORT);
	endpoint = new ip::pgm::endpoint(udp_encap_port);
	
	/* assign socket to specified address */
	if (!sock->bind(*endpoint, &pgm_err)) {
		std::cerr << "Binding PGM socket: " << pgm_err->message << std::endl;
		goto err_abort;
	}
	
	/* join IP multicast groups */
	for (unsigned i = 0; i < res->ai_recv_addrs_len; i++) {
		sock->set_option(IPPROTO_PGM, cpgm::PGM_JOIN_GROUP, &res->ai_recv_addrs[i], sizeof(struct group_req));
	}
	sock->set_option(IPPROTO_PGM, cpgm::PGM_SEND_GROUP, &res->ai_send_addrs[0], sizeof(struct group_req));
	cpgm::pgm_freeaddrinfo(res);

	{
		/* set IP parameters */
		const int blocking = 0,
			  multicast_loop = use_multicast_loop ? 1 : 0,
			  multicast_hops = 16,
			  dscp = 0x2e << 2;		/* Expedited Forwarding PHB for network elements, no ECN. */

		sock->set_option(IPPROTO_PGM, cpgm::PGM_MULTICAST_LOOP, &multicast_loop, sizeof(multicast_loop));
		sock->set_option(IPPROTO_PGM, cpgm::PGM_MULTICAST_HOPS, &multicast_hops, sizeof(multicast_hops));
		sock->set_option(IPPROTO_PGM, cpgm::PGM_TOS, &dscp, sizeof(dscp));
		sock->set_option(IPPROTO_PGM, cpgm::PGM_NOBLOCK, &blocking, sizeof(blocking));
	}

	if (!sock->connect(&pgm_err)) {
		fprintf (stderr, "Connecting PGM socket: %s\n", pgm_err->message);
		goto err_abort;
	}

	return TRUE;

err_abort:
	if (NULL != sock) {
		sock->close(FALSE);
		sock = NULL;
	}
	if (NULL != res) {
		cpgm::pgm_freeaddrinfo(res);
		res = NULL;
	}
	if (NULL != pgm_err) {
		cpgm::pgm_error_free(pgm_err);
		pgm_err = NULL;
	}
	return FALSE;
}
