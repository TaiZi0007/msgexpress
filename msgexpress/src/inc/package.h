#pragma once

#ifdef _WIN32
#pragma warning(disable:4251)
#pragma warning(disable:4275)
#endif

#define LIBVERSION "2018-12-17"
#define MAXPACKSIZE 102400*5

#include <memory>
#include <stdio.h>
#include <stdarg.h>
#include "msgexpress.h"
#include "appaddress.h"
#include "logger.h"
#include "protobuf.h"
#include <sys/timeb.h>
#include <string.h>

#ifdef _WIN32
    #ifdef MSGEXPRESS_EXPORTS
    #include <winsock2.h>
    #endif
typedef __int64 int64_t;
typedef unsigned __int64 uint64_t;
#elif __linux__
#include <arpa/inet.h>
#include <sys/types.h>
#endif

using namespace std;

#ifdef _WIN32
    #define snprintf _snprintf
#else
    #define stricmp strcasecmp
    #define SOCKET int
    #define INVALID_SOCKET 0
#endif

#ifdef _WIN32
#ifndef ntohll
extern "C" uint64_t ntohll(uint64_t val);
#endif
#ifndef htonll
extern "C" uint64_t htonll(uint64_t val);
#endif
#else
#ifndef ntohll
uint64_t ntohll(uint64_t val);
#endif
#ifndef htonll
uint64_t htonll(uint64_t val);
#endif
#endif

uint64_t gettimepoint();//return ms

static const char PackageFlag=94;
static const unsigned char Version=1;

#pragma pack(push,1)
struct MSGEXPRESS_API PackageHeader
{
	friend class Package;
	enum MsgType
	{
		Request = 1,
		Response,
		Publish,
		Route,

	};
    enum CodeType
	{
		PROTOBUF = 0,
		JSON,
		JAVA,
	};
private:
	char flag1_;                //数据包开始标识
	char flag2_;                //数据包开始标识
	unsigned char crc_;         //crc校验
	unsigned char version_;     //版本号，1
	unsigned char type_;        //数据包类型MsgType
	unsigned char offset_;     //数据块的偏移量
	union {
		unsigned short options_;     //开关项
		struct {
			unsigned char hasext_ : 1; //是否有扩展数据
			unsigned char needreply_ : 1;//Publish消息时是否需要应答
			unsigned char protocol_ : 2;//数据序列化协议，0表示protobuf，1表示json,2表示java序列化
			unsigned char ismulticast_ : 1; //是否组播
			unsigned char issequence_ : 1; //是否时序消息
			unsigned char loadbalance_ : 2;
			unsigned char ispriority_ : 1;//是否优先处理
			unsigned char issync_ : 1;//是否是同步调用
			unsigned char reserve_ : 6;
		};
	};

	union
	{
		unsigned short codeinfo_;
		struct {
			unsigned short isencrypt_ : 1;//是否加密
			unsigned short iszip_ : 1;//是否压缩
			unsigned short encrypt_ : 4;//加密算法
			unsigned short compratio_ : 10;//压缩比
		};
	};
	union {
		unsigned short multipageinfo_;
		struct {
			unsigned short ismultipage_ : 1; //是否分包
			unsigned short pageno_ : 15;//包编号,从1开始编号，0表示最后一个包
		};
	};

	unsigned int serialnum_;    //流水号
	unsigned int bodysize_;     //数据包体大小
	unsigned int srcaddr_;    //源地址
	unsigned int dstaddr_;    //目标地址
	int command_;   //命令，全局唯一
	unsigned short code_;
	unsigned short other_;
	unsigned char brokertrace_[8];
public:
	PackageHeader();
	PackageHeader(const PackageHeader& header);
	void read(const char* buffer);
	void write(char* buffer)const;
	void static CalculateCrc(char* buffer);
	bool static CheckCrc(const char* buffer);
	string DebugString() const;
	
public:
	void set_version(unsigned char version) { version_ = version; }
	unsigned char version()const{ return version_; }
	void set_type(unsigned char type) { type_ = type; }
	unsigned char type()const{ return type_; }
	void set_offset(unsigned char offset) { offset_ = offset; }
	unsigned char offset()const{ return offset_; }
	void set_options(unsigned short options) { options_ = options; }
	unsigned short options()const{ return options_; }
	void set_serialnum(unsigned int serialnum) { serialnum_ = serialnum; }
	unsigned int serialnum()const{ return serialnum_; }
	void set_bodysize(unsigned int bodysize) { bodysize_ = bodysize; }
	unsigned int bodysize()const{ return bodysize_; }
	void set_srcaddr(unsigned int srcaddr) { srcaddr_ = srcaddr; }
	unsigned int srcaddr()const{ return srcaddr_; }
	void set_dstaddr(unsigned int dstaddr) { dstaddr_ = dstaddr; }
	unsigned int dstaddr()const{ return dstaddr_; }
	void set_command( int command) { command_ = command; }
    int command()const{ return command_; }
	void set_code(unsigned short code) { code_ = code; }
	unsigned short code()const{ return code_; }
	void set_ismultipage(bool ismultipage) { ismultipage_ = ismultipage; }
	bool ismultipage()const{ return ismultipage_; }
	void set_pageno(unsigned short pageno) { pageno_ = pageno; }
	unsigned short pageno()const{ return pageno_; }
	void set_hasext(bool hasext){ hasext_=hasext; }
	bool hasext() const { return hasext_; }
	bool needreply() const { return needreply_; }
	void set_needreply(bool needreply) { needreply_ = needreply; }
	void set_protocol(unsigned char protocol){ protocol_ = protocol; }
	unsigned char protocol() const { return protocol_; }
	void set_ismulticast(bool ismulticast){ ismulticast_ = ismulticast; }
	bool ismulticast() const { return ismulticast_; }
	void set_issequence(bool issequence){ issequence_ = issequence; }
	bool issequence() const { return issequence_; }
	void set_ispriority(bool ispriority){ ispriority_ = ispriority; }
	bool ispriority() const { return ispriority_; }
	void set_priority(bool ispriority) { ispriority_ = ispriority; }
	bool issync() const { return issync_; }
	void set_issync(bool issync){ issync_ = issync; }
	bool isencrypt() const { return isencrypt_; }
	void set_iszip(bool iszip) { iszip_ = iszip; }
	bool iszip() const { return iszip_; }
	void set_compratio(unsigned short compratio){ compratio_ = compratio; }
	unsigned short compratio() const { return compratio_; }
	bool add_brokertrace(unsigned char brokerid){
		for (int i = 0; i < sizeof(brokertrace_); i++){
			if (brokertrace_[i] == brokerid)
				return false;
			else if (brokertrace_[i] == 0){
				brokertrace_[i] = brokerid;
				return true;
			}
		}
		return false;
	}
};
#pragma pack(pop)

static const size_t PackageHeaderLength=sizeof(PackageHeader);

class Package;

typedef  shared_ptr<google::protobuf::Message> MessagePtr;
typedef  shared_ptr<Package> PackagePtr;

class MSGEXPRESS_API Package
{
private:
	PackageHeader header_;
	MessagePtr message_;
	MessagePtr extmessage_;
	std::string classtype_;
	char*  content_;
	size_t packagesize_;
	time_t time_;
private:
	Package();
public:
	Package(const PackageHeader& header, const std::string& classtype, const char* data, size_t packagesize, MessagePtr message);
	~Package();
public:
	const PackageHeader& header()const{ return header_; }
	void set_message(MessagePtr message){ message_ = message; }
	MessagePtr message()const{ return message_; }
	void set_extmessage(MessagePtr extmessage){ extmessage_ = extmessage; }
	MessagePtr extmessage()const{ return extmessage_; }
	void set_classtype(const std::string& classtype){ classtype_ = classtype; }
	std::string classtype()const{ return classtype_; }
	char* content() const { return content_; }
	size_t packagesize() const { return packagesize_; }
	time_t time(){ return time_; }

	int GetCommand();
	char* GetBodyData();
	unsigned int GetBodySize();
    void SetSrcAddr(unsigned int addr);
	unsigned int GetSrcAddr();
    void SetDstAddr(unsigned int addr); 
	unsigned int GetDstAddr();
	void SetCode(unsigned short code);
	void SetPriority(bool priority);
	void SetSerialNum(unsigned int serialnum);
	bool AddBrokerTrace(unsigned char brokerid);
	PackageHeader::MsgType GetPackageType();
	void SetPackageType(PackageHeader::MsgType packagetype);
	unsigned int GetSerialNum();
	bool IsMulticast();
	bool IsRequest();
	bool IsResponse();
	bool IsPublish();
	std::string DebugString(bool showHexContent = false);
	
};

