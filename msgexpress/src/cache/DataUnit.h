#pragma once

#include "../inc/logger.h"
#include "../inc/package.h"

#include "../inc/msgexpress.h"
#include "../inc/service_config.h"


using namespace google::protobuf;

class DataUnit
{
public:
	DataUnit(void);
	virtual ~DataUnit(void);

	virtual uint64 PutData(PublishPtr data,bool loaddb,string& err)=0;
	virtual void SyncData(PublishPtr data,vector<uint64>& vecLost)=0;
	virtual uint32 QueryData(Storage::QueryDataRequest*,vector<PublishPtr>& dataset)=0;
	virtual uint32 GetAllData(vector<PublishPtr>& dataset)=0;
	virtual int32 DeleteData(Storage::DeleteDataRequest*,bool deleteDB)=0;
	virtual void SaveDB()=0;
	virtual bool HasUnSavedData()=0;
	virtual void Print()=0;
	virtual string GetKey()=0;
};

