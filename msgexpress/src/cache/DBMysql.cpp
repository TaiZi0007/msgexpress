#include "DBMysql.h"
#include "ConfigSingle.h"
#include "../inc/logger.h"

#ifdef MYSQLSUPPORT

namespace Database
{
CMutexLocker DBMysql::tMutex;
CMutexLocker DBMysql::tReadMutex;
CMutexLocker DBMysql::tWriteMutex;
CMutexLocker DBMysql::tMutexOther;
CMutexLocker DBMysql::tTransactionMutex;

DBMysql::DBMysql():bConnect(false),bReadConnect(false),bWriteConnect(false),bConnectOther(false),bTransactionConnect(false)
{
	mysql_init(&mysql);
	mysql_thread_init();
	mysql_options(&mysql, MYSQL_SET_CHARSET_NAME, "utf8");	
}

DBMysql::~DBMysql(void)
{
	mysql_thread_end();
	CloseDB();
}

bool DBMysql::SetOpion( enum mysql_option option, const string& arg )
{
	if (!mysql_options(&mysql, option, arg.c_str()))
		return true;
	else
		return false;	
}

bool DBMysql::ConnectDB( const string& host, const string& username, const string& password, const string& dbName, unsigned int port /*= 3306*/, unsigned long client_flag)
{
	if (host.empty() || username.empty() || password.empty() || dbName.empty())
		return false;

	if (mysql_real_connect(&mysql, host.c_str(), username.c_str(), password.c_str(), dbName.c_str(), port, NULL, client_flag))
	{
		char reconnect = 1;
		mysql_options(&mysql, MYSQL_OPT_RECONNECT, &reconnect);
		return true;
	}
	else
		return false;	
}

bool DBMysql::CanWork()
{
	return mysql_ping(&mysql) == 0;
}

bool DBMysql::ExecuteSql( const string& strSql )
{
	if (strSql.empty())	
		return false;	
	bool ret = false;
	CMutexGuard guard(mutex);
	if (mysql_ping(&mysql) == 0)
	{
		if (!mysql_real_query(&mysql, strSql.data(),strSql.size()))	
			ret = true;		
	}
	return ret;
}

bool DBMysql::StartTransaction()
{
	if(!mysql_real_query(&mysql, "START TRANSACTION", (unsigned long)sizeof("START TRANSACTION")))
		return true;
	else		
		return false;
}

bool DBMysql::Commit()
{
	if(!mysql_real_query( &mysql, "COMMIT",	(unsigned long)sizeof("COMMIT") ) )	
		return true;	
	else     	
		return false;
}

bool DBMysql::Rollback()
{
	if(!mysql_real_query(&mysql, "ROLLBACK", (unsigned long)sizeof("ROLLBACK") ) )
		return true;
	else  		
		return false; 
}

bool DBMysql::SelectDB( const string& dbName )
{
	if (dbName.empty())
		return false;
	if (mysql_select_db(&mysql, dbName.c_str()))
		return true;
	else
		return false;
}

void DBMysql::CloseDB( void )
{
	mysql_close(&mysql);
}

bool DBMysql::GetRecordSet( CRecordSet& rs )
{
	MYSQL_RES* result;

	result = mysql_store_result(&mysql);
	if (result)
	{
		rs.SetColumnNum(mysql_num_fields(result));
		rs.SetRowNum((size_t)mysql_num_rows(result));

		for (size_t i = 0; i < rs.GetColumnNum(); ++i)
		{
			rs.InsertFieldName(mysql_fetch_field_direct(result, i)->name);
		}
		MYSQL_ROW row = NULL;
		string str;
		unsigned long *pLen = NULL;
		while (NULL != (row = mysql_fetch_row(result)) && (pLen = mysql_fetch_lengths(result)) != NULL)
		{		
			vector<string> vec;
			for (size_t i = 0; i < rs.GetColumnNum(); ++i)
			{
				if (row[i])
				{
					str.assign(row[i], pLen[i]);
					vec.push_back(str);
				}
				else
					vec.push_back("");
			}
			rs.InsertData(vec);
		}
		mysql_free_result(result);
		return true;
	}
	else
		return false;
}

string DBMysql::GetErrorInfo()
{
	return mysql_error(&mysql);
}

size_t DBMysql::GetErrorNo()
{
	return mysql_errno(&mysql);
}

bool DBMysql::QueryAndGetResult( const string& strSql, CRecordSet& rs )
{
	if (strSql.empty())
		return false;

	bool ret = false;
	CMutexGuard guard(mutex);
	if (mysql_ping(&mysql) == 0)
	{
		if (!mysql_query(&mysql, strSql.c_str()))	
		{
			if (GetRecordSet(rs))		
				ret = true;	
		}
	}	
	return ret;
}

size_t DBMysql::GetLastInsertId()
{
	return (size_t)mysql_insert_id(&mysql);
}

bool DBMysql::LockTable( const string& tableName, const string& priority )
{
	string strsql = "LOCK TABLES " + tableName + priority;
	if (!mysql_query(&mysql, strsql.c_str()))
		return true;
	return false;
}

bool DBMysql::UnLockTable()
{
	if (!mysql_query(&mysql, "UNLOCK TABLES"))
		return true;
	return false;
}

bool DBMysql::IsQueryResultEmpty( const string& strSql )
{
	if (strSql.empty())
		return true;

	bool ret = true;
	CRecordSet rs;
	CMutexGuard guard(mutex);
	if (mysql_ping(&mysql) == 0)
	{
		if (!mysql_query(&mysql, strSql.c_str()))	
			if (GetRecordSet(rs))		
				if (rs.GetRowNum() > 0)
				{
					ret = false;
				}
	}		
	return ret;
}

DBMysql* DBMysql::GetInstance()
{
	static DBMysql sql;
	if (!sql.bConnect)
	{
		CMutexGuard guard(tMutex);
		if (!sql.bConnect)
		{
			CConfigSingle *pConfig = CConfigSingle::GetInstance();
			if (sql.ConnectDB(pConfig->host, pConfig->username, pConfig->password, pConfig->dbName))
			{
				sql.bConnect = true;		
			}
			else
				LOG4_ERROR("DBMysql::GetInstance():connect to db fail");
		}		
	}
	return &sql;
}

DBMysql* DBMysql::GetReadInstance()
{
	static DBMysql sql;
	if (!sql.bReadConnect)
	{
		CMutexGuard guard(tReadMutex);
		if (!sql.bReadConnect)
		{
			CConfigSingle *pConfig = CConfigSingle::GetInstance();
			if (sql.ConnectDB(pConfig->host, pConfig->username, pConfig->password, pConfig->dbName))
			{
				sql.bReadConnect = true;		
			}
			else
				LOG4_ERROR("DBMysql::GetReadInstance():connect to db fail");
		}		
	}
	return &sql;
}

DBMysql* DBMysql::GetWriteInstance()
{
	static DBMysql sql;
	if (!sql.bWriteConnect)
	{
		CMutexGuard guard(tWriteMutex);
		if (!sql.bWriteConnect)
		{
			CConfigSingle *pConfig = CConfigSingle::GetInstance();
			if (sql.ConnectDB(pConfig->host, pConfig->username, pConfig->password, pConfig->dbName))
			{
				sql.bWriteConnect = true;		
			}
			else
				LOG4_ERROR("DBMysql::GetWriteInstance():connect to db fail");
		}		
	}
	return &sql;
}

DBMysql* DBMysql::GetInstanceOther()
{
	static DBMysql sql;
	/*if (!sql.bConnectOther)
	{
		CMutexGuard guard(tMutexOther);
		if (!sql.bConnectOther)
		{
			CConfigSingle *pConfig = CConfigSingle::GetInstance();
			if (sql.ConnectDB(pConfig->hostOther, pConfig->usernameOther, pConfig->passwordOther, pConfig->dbNameOther))
			{
				sql.bConnectOther = true;		
			}
			else
				LOG4_ERROR("DBMysql::GetInstanceOther():connect to db fail");
		}		
	}*/
	return &sql;
}

DBMysql* DBMysql::GetTransactionInstance()
{
	static DBMysql sql;
	if (!sql.bTransactionConnect)
	{
		CMutexGuard guard(tTransactionMutex);
		if (!sql.bTransactionConnect)
		{
			CConfigSingle *pConfig = CConfigSingle::GetInstance();
			if (sql.ConnectDB(pConfig->host, pConfig->username, pConfig->password, pConfig->dbName))
			{
				sql.bTransactionConnect = true;		
			}
			else
				LOG4_ERROR("DBMysql::GetTransactionInstance():connect to db fail");
		}		
	}
	return &sql;
}

char* DBMysql::GetConvertBytesString( const string& str )
{
	char *buf = new char[2 * str.length() + 2];
	mysql_real_escape_string(&mysql, buf, str.data(), str.length());

	return buf;
}

CRecordSet::CRecordSet():columnNum(0),rowNum(0)
{

}

CRecordSet::~CRecordSet()
{

}

void CRecordSet::InsertData( const vector<string>& vec )
{
	dataVec.push_back(vec);
}

void CRecordSet::SetColumnNum( size_t col )
{
	columnNum = col;
}

void CRecordSet::SetRowNum( size_t row )
{
	rowNum = row;
}

size_t CRecordSet::GetColumnNum() const
{
	return columnNum;
}

size_t CRecordSet::GetRowNum() const
{
	return rowNum;
}

void CRecordSet::Clear()
{
	dataVec.clear();
	rowNum = columnNum = 0;
	fieldNameVec.clear();
}

string CRecordSet::GetDataAt( size_t row, size_t col ) const
{
	if (row >= rowNum || col >= columnNum)
		return "";
	else
		return dataVec[row][col];
}

void CRecordSet::InsertFieldName( string fieldName )
{
	fieldNameVec.push_back(fieldName);
}

int CRecordSet::GetColumnIndex( const string& fieldName )
{
	for (size_t i = 0; i < columnNum; ++i)
	{
		if (fieldName == fieldNameVec[i])
			return i;
	}
	return -1;
}

string CRecordSet::GetFieldName( size_t col ) const
{
	if (col >= columnNum)
		return NULL;
	else
		return fieldNameVec[col];
}

bool CRecordSet::IsEmpty()
{
	return rowNum == 0;
}

}

#endif