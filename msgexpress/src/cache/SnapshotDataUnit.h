#pragma once

#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <list>
#include <unordered_map>
#include <map>
#include <queue>
#include <unordered_set>
#include "DataUnit.h"
#include "../core/spthread.h"

class SnapshotDataUnit: public DataUnit
{
private:
	class Data
	{
	public:
		enum Status:unsigned char{
			Syncing=1,//内存数据与数据库同步中
			Deleting,//删除中
			Synced,//内存数据与数据库里的记录一致
		};
		PublishPtr data;
		Status status;
		bool inDB;//数据库是否有记录
		Data()
		{
			inDB=false;
			status=Syncing;
		}
	};
typedef shared_ptr<Data> DataPtr ;
private:
	string unitkey;
	int topic;
	string tablename;
	sp_thread_mutex_t mutex;
	std::map<int,const MsgExpress::DataItem*> mapTemp;//temp
	std::unordered_map<string,DataPtr> mapDataItem;//<key,item>
	std::queue<string> queueUnprocess;//key
private:
	void CreateKey(const RepeatedPtrField<MsgExpress::DataItem>& datalist,bool isQuery,vector<string>& vecKeys);
	PublishPtr Merge(PublishPtr old,PublishPtr data);
public:
	SnapshotDataUnit(string unitkey,int topic,string tbl);
	virtual ~SnapshotDataUnit(void);
	virtual uint64 PutData(PublishPtr data,bool loaddb,string& err);
	virtual void SyncData(PublishPtr data,vector<uint64>& vecLost);
	virtual uint32 QueryData(Storage::QueryDataRequest*,vector<PublishPtr>& dataset);
	virtual uint32 GetAllData(vector<PublishPtr>& dataset);
	virtual int32 DeleteData(Storage::DeleteDataRequest*,bool deleteDB);
	virtual void SaveDB();
	virtual bool HasUnSavedData(){ return queueUnprocess.size()>0; }
	virtual void Print();
	virtual string GetKey(){return unitkey;}
};

