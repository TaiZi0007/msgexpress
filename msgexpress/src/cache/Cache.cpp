
#include <functional>
#include <algorithm>

#include "cache.h"
#include "../inc/service_config.h"
#include "../client/client_impl.h"
#include "../inc/logger.h"
#include "../client/client_config.h"

Cache::Cache(){ 
	database=0;
	connected=false;
}
Cache::~Cache()
{
}
int  Cache::Initialize(const char* configPath)
{
	if(configPath)
	    XMLConfig::SetPath(configPath);
	string xml_file = "cachecfg.xml";
	ClientConfig sconf;
	if(!sconf.LoadFile(xml_file))
	{   
		LOG4_ERROR("read client config file error");
		return 0;
	}  
	
	return DataBusClient::Initialize(&sconf.clientCfg,0);
}
int  Cache::Initialize(string addr,unsigned short port)
{
	ClientCFG cfg;
	ServerCFG server;
	server.serverIP=addr;
	server.port=port;
	cfg.serverGroup.push_back(server);
	return DataBusClient::Initialize(&cfg,0);
}

int Cache::Select(unsigned char db,string* errstr)
{
	database=db;
	return 0;
}

int Cache::ClearDB(string* errstr)
{
	if(!connected)
	{
		if(errstr)
			*errstr="Disconnected to cache service";
		return -1;
	}
	Storage::DeleteDataRequest query;
	query.set_setid(database);
	query.set_topic(0);
	MessagePtr resp;
    bool ret=SendMsg(query,resp,2000);
	if(!ret)
	{
		MsgExpress::ErrMessage err=this->GetLatestError();
		if(errstr)
			*errstr=err.errmsg();
		return err.errcode();
	}
	Storage::DeleteDataResponse* response=(Storage::DeleteDataResponse*)resp.get();
	if(response->result()!=0)
	{
		if(errstr)
			*errstr=response->msg();
		return response->result();
	}
	return 0;
}
int Cache::SaveData(const Storage::SaveDataRequest& request,Storage::SaveDataResponse& resp,string* errstr)
{
	if(!connected)
	{
		if(errstr)
			*errstr="Disconnected to cache service";
		return -1;
	}
	MessagePtr response;
	bool ret=SendMsg(request,response,1000);
	if(!ret)
	{
		MsgExpress::ErrMessage err=this->GetLatestError();
		if(errstr)
			*errstr=err.errmsg();
		return err.errcode();
	}
	resp.CopyFrom(*((Storage::SaveDataResponse*)response.get()));
	if(resp.result()!=0)
	{
		if(errstr)
			*errstr=resp.msg();
		return resp.result();
	}
	return 0;
}
int Cache::QueryData(const Storage::QueryDataRequest& request,Storage::QueryDataResponse& resp,std::string* errstr)
{
	if(!connected)
	{
		if(errstr)
			*errstr="Disconnected to cache service";
		return -1;
	}
	MessagePtr response;
	bool ret=SendMsg(request,response,1000);
	if(!ret)
	{
		MsgExpress::ErrMessage err=this->GetLatestError();
		if(errstr)
			*errstr=err.errmsg();
		return err.errcode();
	}
	resp.CopyFrom(*((Storage::QueryDataResponse*)response.get()));
	if(resp.result()!=0)
	{
		if(errstr)
			*errstr=resp.msg();
		return resp.result();
	}
	return 0;
}
int Cache::Set(string key,string value,string* errstr)
{
	if(!connected)
	{
		if(errstr)
			*errstr="Disconnected to cache service";
		return -1;
	}
	if(key.size()==0)
	{
		if(errstr)
			*errstr="Key is empty.";
		return -1;
	}
	Storage::SaveDataRequest idRequest;
	idRequest.set_setid(database);
	MsgExpress::PublishData* pub=new MsgExpress::PublishData();
	idRequest.set_allocated_content(pub);
	pub->set_topic(0);

	MsgExpress::DataItem* item=pub->add_item();
	item->set_key(1);
	//item->set_type(MsgExpress::STRING);
	item->add_strval(key);
	item->set_ispk(true);
	
	item=pub->add_item();
	item->set_key(2);
	//item->set_type(MsgExpress::BINARY);
	item->add_rawval(value);

	MessagePtr resp;
    bool ret=SendMsg(idRequest,resp,1000);
	if(!ret)
	{
		MsgExpress::ErrMessage err=this->GetLatestError();
		if(errstr)
			*errstr=err.errmsg();
		return err.errcode();
	}
	Storage::SaveDataResponse* response=(Storage::SaveDataResponse*)resp.get();
	if(response->result()!=0)
	{
		if(errstr)
			*errstr=response->msg();
		return response->result();
	}
	return 0;
}
int Cache::Get(string key,string& value,string* errstr)
{

	AppServerImpl* impl = (AppServerImpl*)GetAppServer();

	Storage::QueryDataRequest query;
	query.set_setid(database);
	query.set_topic(0);
	MsgExpress::DataItem* item=query.add_condition();
	item->set_key(1);
	//item->set_type(MsgExpress::STRING);
	item->add_strval(key);
	
	string err;
	vector<PublishPtr> dataset;
	//if(impl->GetDataManager()->QueryData(&query,err,dataset)==0 && dataset.size()>0)
	//{
	//	PublishPtr data=dataset.at(0);
	//	value=data->item(1).rawval(0);
	//	return 0;
	//}
	//else
	//{
	//	LOG4_WARN("Get data failed from local datastore,key=%s,err:%s",key.c_str(),err.c_str());
	//}
	if(!connected)
	{
		if(errstr)
			*errstr="Disconnected to cache service";
		return -1;
	}
	if(key.size()==0)
	{
		if(errstr)
			*errstr="Key is empty.";
		return -1;
	}
	MessagePtr resp;
    bool ret=SendMsg(query,resp,1000);
	if(!ret)
	{
		MsgExpress::ErrMessage err=this->GetLatestError();
		if(errstr)
			*errstr=err.errmsg();
		LOG4_ERROR("Get data failed from remote datastore,key=%s,err:%s",key.c_str(),err.errmsg().c_str());
		return err.errcode();
	}
	Storage::QueryDataResponse* response=(Storage::QueryDataResponse*)resp.get();
	if(response->result()!=0)
	{
		if(errstr)
			*errstr=response->msg();
		LOG4_WARN("Get data failed from remote datastore,key=%s,err:%s",key.c_str(),response->msg().c_str());
		return response->result();
	}
	if(response->data_size()<1)
	{
		if(errstr)
			*errstr="value is empty";
		return -1;
	}
	value=response->data(0).item(1).rawval(0);
	return 0;
}
int Cache::Del(std::string key,std::string* errstr)
{
	if(!connected)
	{
		if(errstr)
			*errstr="Disconnected to cache service";
		return -1;
	}
	if(key.size()==0)
	{
		if(errstr)
			*errstr="Key is empty.";
		return -1;
	}
	Storage::DeleteDataRequest query;
	query.set_setid(database);
	query.set_topic(0);
	MsgExpress::DataItem* item=query.add_condition();
	item->set_key(1);
	//item->set_type(MsgExpress::STRING);
	item->add_strval(key);
	MessagePtr resp;
    bool ret=SendMsg(query,resp,2000);
	if(!ret)
	{
		MsgExpress::ErrMessage err=this->GetLatestError();
		if(errstr)
			*errstr=err.errmsg();
		return err.errcode();
	}
	Storage::DeleteDataResponse* response=(Storage::DeleteDataResponse*)resp.get();
	if(response->result()!=0)
	{
		if(errstr)
			*errstr=response->msg();
		return response->result();
	}
	return 0;
}

void Cache::Set(unsigned char dataset,const string& key,const string& value,void* userdata, std::function<void(const CallbackArg&)> callback)
{
	int retcode=0;
	string errstr;
	if(!connected)
	{
		errstr="Disconnected to cache service";
		retcode= -1;
	}
	else if(key.size()==0)
	{
		errstr="Key is empty.";
		retcode= -1;
	}
	if(retcode==0)
	{
		Storage::SaveDataRequest* req=new Storage::SaveDataRequest();
		req->set_setid(dataset);
		MsgExpress::PublishData* pub=new MsgExpress::PublishData();
		req->set_allocated_content(pub);
		pub->set_topic(0);

		MsgExpress::DataItem* item=pub->add_item();
		item->set_key(1);
		//item->set_type(MsgExpress::STRING);
		item->add_strval(key);
		item->set_ispk(true);
	
		item=pub->add_item();
		item->set_key(2);
		//item->set_type(MsgExpress::BINARY);
		item->add_rawval(value);

		std::function<void(const MsgParams&)> cb = [](const MsgParams& params) {
			CallbackArg arg;
			arg.retcode=params.result;
			arg.errmsg=params.errmsg;
			Callback* data=(Callback*)params.arg;
			arg.userdata=data->userdata;
			if(params.result==0 && params.resp!=NULL)
			{
				shared_ptr<Storage::SaveDataResponse> resp=dynamic_pointer_cast<Storage::SaveDataResponse>(params.resp->message());
				if(resp->result()!=0)
				{
					arg.errmsg=resp->msg();
					arg.retcode=resp->result();
				}
			}
			data->cb(arg);
			delete data;
		};
		
		Callback* data=new Callback();
		data->cb=callback;
		data->userdata=userdata;
		bool ret=PostMsg(MessagePtr(req),cb,data,3000,Options());
		if(!ret)
		{
			delete data;
			MsgExpress::ErrMessage err=this->GetLatestError();
			retcode=err.errcode();
			errstr=err.errmsg();
		}
	}
	if(retcode!=0)
	{
		CallbackArg arg;
		arg.retcode=retcode;
		arg.errmsg=errstr;
		arg.userdata=userdata;
		callback(arg);
	}
}

void Cache::Get(unsigned char dataset,const string& key,void* userdata, std::function<void(const CallbackArg&)> callback)
{

	AppServerImpl* impl = (AppServerImpl*)GetAppServer();

	Storage::QueryDataRequest* req=new Storage::QueryDataRequest();
	req->set_setid(dataset);
	req->set_topic(0);
	MsgExpress::DataItem* item=req->add_condition();
	item->set_key(1);
	//item->set_type(MsgExpress::STRING);
	item->add_strval(key);
	
	string value;
	int retcode=0;
	string errstr;
	vector<PublishPtr> result;
	/*if(impl->GetDataManager()->QueryData(req,errstr,result)==0 && result.size()>0)
	{
		CallbackArg arg;
		arg.retcode=0;
		arg.value=result.at(0)->item(1).rawval(0);
		arg.userdata=userdata;
		callback(arg);
		return;
	}
	else*/
	{
		LOG4_WARN("Get data failed from local datastore,key=%s,err:%s",key.c_str(),errstr.c_str());
		if(!connected)
		{
			errstr="Disconnected to cache service";
			retcode=-1;
		}
		else if(key.size()==0)
		{
			errstr="Key is empty.";
			retcode= -1;
		}
		if(retcode==0)
		{
			std::function<void(const MsgParams&)> cb = [](const MsgParams& params) {
				CallbackArg arg;
				arg.retcode=params.result;
				arg.errmsg=params.errmsg;
				Callback* data=(Callback*)params.arg;
				arg.userdata=data->userdata;
				if(params.result==0 && params.resp!=NULL)
				{
					shared_ptr<Storage::QueryDataResponse> resp=dynamic_pointer_cast<Storage::QueryDataResponse>(params.resp->message());
					if(resp->result()!=0)
					{
						arg.errmsg=resp->msg();
						arg.retcode=resp->result();
					}
					else if(resp->data_size()>0)
						arg.value=resp->data(0).item(1).rawval(0);
					else
					{
						arg.errmsg="value is empty";
						arg.retcode=-1;
					}
				}
				data->cb(arg);
				delete data;
			};
		
			Callback* data=new Callback();
			data->cb=callback;
			data->userdata=userdata;
			bool ret=PostMsg(MessagePtr(req),cb,data,3000,Options());
			if(!ret)
			{
				delete data;
				MsgExpress::ErrMessage err=this->GetLatestError();
				retcode=err.errcode();
				errstr=err.errmsg();
			}
		}
		if(retcode!=0)
		{
			CallbackArg arg;
			arg.retcode=retcode;
			arg.errmsg=errstr;
			arg.userdata=userdata;
			callback(arg);
		}
	}
}

void Cache::OnMessage(PackagePtr package)
{
	if (package->IsResponse())
	{
	}
	else if (package->IsRequest())
	{
		LOG4_ERROR("I am not a service");
	}
}
//void Cache::OnPublish(PublishPtr pub)
//{
//	LOG4_INFO("Public,topic=%d",pub->topic());
//}
void Cache::OnPublish(PackagePtr pub)
{
	//LOG4_INFO("Public,topic=%d",pub->topic());
}
void Cache::OnEvent(int eventId)
{
	if (Event::Offline == eventId)
		connected=false;
	else if (Event::Online == eventId)
		connected=true;
}
