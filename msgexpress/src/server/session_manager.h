#pragma once

#include <unordered_map>
#include <queue>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include "session.h"

class SessionManager
{
private:
	int brokerId;
	std::unordered_map<AppAddress, SessionPtr, AppAddressHash> mMapConn;//key是地址
	std::queue<unsigned short> queAddress;
	int count;
	std::mutex mMutexService;
	// 条件阻塞
	std::condition_variable cv_task;
	// 是否关闭提交
	std::atomic<bool> stoped;
public:
	SessionManager();
	~SessionManager();
	void setBrokerId(int id){ brokerId = id;  }
	void addSession(SessionPtr);
	void removeSession(AppAddress addr);
	SessionPtr getSession(AppAddress addr);
	int getSessionCount(){ return count; }
	void clearAll();
};