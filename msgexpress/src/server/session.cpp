#include "session.h"
#include "session_manager.h"
#include "broker.h"
#include "../inc/logger.h"
#include "../core/command_config.h"
#include "../core/messageutil.h"
#include "../core/md5.h"
#include "../core/porting.h"
#include "../core/ioutils.h"

#include <time.h>
#include <unordered_map>
#include <unordered_set>
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#endif

using namespace std::placeholders;

Session::Session(event_base* base, const SockInfo& sockinfo, SessionManager* manager, BrokerServer* brokerServer, ServiceManager *serviceManager)
{
	hasLogin = false;
	hasSuperRights = false;
	stoped = false;
	memcpy(&sockInfo, &sockinfo, sizeof(sockInfo));
	connManager = manager;
	server = brokerServer;

	msgCount = 0;
	lastCountTime = gettimepoint();
	mps = brokerServer->getConfig().mps;
	mServiceManager = serviceManager;
	conn = new Connection(base, sockinfo, brokerServer->getConfig().sockbuffer, brokerServer->getConfig().connect_time_out_);
	conn->onPackage = bind(&Session::onPackage, this, _1);
	conn->onSend = bind(&Session::onSend, this, _1,_2);
	conn->onError = bind(&Session::onError, this, _1);
	connManager->addSession(SessionPtr(this));
	LOG4_INFO("New session addr = %s,ip=%s,sock=%d", mAddr.toString().c_str(), sockInfo.ip, sockInfo.sock);
	
}

Session::~Session()
{
	conn->close();
	delete conn;
	LOG4_INFO("Session::~Session ,addr=%s", mAddr.toString().c_str());
}

void Session::setAddress(AppAddress address)
{
	mAddr = address;
	conn->setAddress(address);
}

AppAddress Session::getAddress()
{
	return mAddr;
}

void Session::onError(short err)
{
	stop();
}
void Session::stop()
{
	MsgExpress::AppInfo info;
	if (mServiceManager->getAppInfo(mAddr, info))
	{
		LOG4_INFO("User offline,addr=%s,info:%s", mAddr.toString().c_str(), info.ShortDebugString().c_str());

		time_t tm;
		time(&tm);
		/*MsgExpress::PublishData pub;
		pub.set_topic(MsgExpress::TOPIC_LOGOUT);
		PublishHelperPtr helper = CreatePublishHelper(&pub);
		helper->AddString(MsgExpress::KEY_BROKER, mServiceManager->getBrokerName());
		helper->AddUInt32(MsgExpress::KEY_ADDR, mAddr);
		helper->AddDatetime(MsgExpress::KEY_TIME, tm);
		helper->AddString(MsgExpress::KEY_UUID, info.logininfo().uuid());
		helper->AddString(MsgExpress::KEY_NAME, info.logininfo().name());
		vector<shared_ptr<Response>> vecResp;
		publish(0, 0, server->getAddress(), &pub, &vecResp);
		server->dispatch(vecResp);*/
	}
	else
	{
		//LOG4_ERROR("get app info failed,addr=%d",mSid.value);
		LOG4_INFO("User offline,addr=%s", mAddr.toString().c_str());
	}
	mServiceManager->unregApp(mAddr);
	if (mServiceList.size()>0)
		mServiceManager->unregService(mServiceList, mAddr);
	mServiceManager->unSubscribeAll(mAddr);
	for (auto item : mServiceList)
	{
		server->updateService(item);
	}
	conn->close();
	LOG4_INFO("Close session, addr = %s", mAddr.toString().c_str());
	connManager->removeSession(mAddr);
}

struct Tmp
{
	SessionPtr session;
	void* other;
	int count;
};
void Session::executeSend(void* arg)
{
	conn->executeSend(arg);

}
void Session::onPackage(PackagePtr package)
{
	server->pushTask(PackagePtr(package), mAddr);
}
void Session::onSend(bool needsend, void* arg)
{
	if (needsend)
	{
		Tmp* tmp = new Tmp();
		tmp->session = connManager->getSession(mAddr);
		tmp->count = 0;
		server->addSendTask(bind(&Session::executeSend, this, std::placeholders::_1), tmp);
	}
	Tmp* tmp = (Tmp*)arg;
	if (tmp)
	    delete tmp;
}
void Session::sendMsg(const string& msg)
{
	conn->sendMsg(msg);
	
}

void Session::reply(Package* package, const google::protobuf::Message& replyMsg, vector<shared_ptr<Response>>* vecResponse)
{
	std::string msg;
	PackageHeader header;
	header.set_command(CommandConfig::getInstance().GetCommand(replyMsg.GetTypeName().c_str()));
	header.set_protocol(package->header().protocol());
	header.set_type ( PackageHeader::Response);
	header.set_serialnum ( package->GetSerialNum());
	header.set_srcaddr( server->getAddress());
	header.set_dstaddr( package->GetSrcAddr());
	shared_ptr<Response> resp = shared_ptr<Response>(new Response());
	if (MessageUtil::serializePackageToString(header, replyMsg, false, 30, &resp->msg))
	{
		resp->header = header;
		resp->vecDestAddr.push_back(header.dstaddr());
		vecResponse->push_back(resp);
	}
	else
	{
		LOG4_ERROR("Connection :: reply,serialze error");
	} 
}

void Session::reply(Package* package, unsigned int errCode, string errMsg, vector<shared_ptr<Response>>* vecResponse)
{
	MsgExpress::ErrMessage repMsg;
	repMsg.set_errcode(errCode);
	repMsg.set_errmsg(errMsg);
        reply(package, repMsg, vecResponse);
}

void Session::post(vector<shared_ptr<Response>>* vecResponse, const char * data, size_t size, AppAddress toAddr)
{
	//if (addr == toAddr)//不发给自己
	//	return;
	shared_ptr<Response> resp = shared_ptr<Response>(new Response());
	resp->header.read(data);
	resp->vecDestAddr.push_back(toAddr);
	resp->msg.assign(data, size);
	vecResponse->push_back(resp);
}
void Session::post(vector<shared_ptr<Response>>* vecResponse, const char * data, size_t size, unordered_set<AppAddress, AppAddressHash>* setIds)
{
	//if (setIds->size() == 0)
	//	return;
	shared_ptr<Response> resp = shared_ptr<Response>(new Response());
	resp->header.read(data);
	resp->msg.assign(data, size);
	std::vector<AppAddress> vecAddr;
	unordered_set<AppAddress, AppAddressHash>::const_iterator it = setIds->begin();
	while (it != setIds->end())
	{
		//if (*it != addr)//不发给自己
		vecAddr.push_back((*it));
		it++;
	}
	resp->vecDestAddr.reserve(vecAddr.size());
	bool firstpub = true;
	std::vector<int> vecType;
	mServiceManager->getAppType(vecAddr, vecType);
	for (int i = 0; i<vecAddr.size(); i++)
	{
		if (mInfo.type() == 3)
		{
			if (vecType.at(i) != 3)
				resp->vecDestAddr.push_back(vecAddr.at(i));
		}
		else
		{
			if (vecType.at(i) == 3)
			{
				if (firstpub)
				{
					resp->vecDestAddr.push_back(vecAddr.at(i));
					firstpub = false;
				}
			}
			else
				resp->vecDestAddr.push_back(vecAddr.at(i));
		}
	}
	while (it != setIds->end())
	{
		if (mInfo.type() == 3)
		{
			MsgExpress::AppInfo info;
			if (!mServiceManager->getAppInfo((unsigned int)(*it), info))
			{
				it++;
				continue;
			}
			if (info.logininfo().type() == 3 && mInfo.type() == 3 && info.logininfo().group() == mInfo.group())
			{
				it++;
				continue;
			}
		}
		resp->vecDestAddr.push_back(*it);
		it++;
	}
	vecResponse->push_back(resp);
}
#define COMPARE2(left,right,op,result) \
{ \
	if(op==MsgExpress::Bigger) \
	    result=( left>right); \
		else if(op==MsgExpress::BiggerOrEqual) \
	    result=( left>=right); \
		else if(op==MsgExpress::Less) \
	    result=( left<right); \
		else if(op==MsgExpress::LessOrEqual) \
	    result=( left<=right); \
		else if(op==MsgExpress::NotEqual) \
	    result=( left!=right); \
} 
bool Satisfy2(const google::protobuf::RepeatedPtrField<std::string>& value, const MsgExpress::ConditionValue& conValue, MsgExpress::DataType type)
{
	bool ok = false;
	for (int i = 0; i<value.size(); i++)
	{
		if (type == MsgExpress::INT32)
		{
			int left = (int)ntohl(*(int*)value.Get(i).data());
			int right = (int)ntohl(*(int*)conValue.value().data());
			COMPARE2(left, right, conValue.operator_(), ok);
		}
		else if (type == MsgExpress::UINT32)
		{
			unsigned int left = (unsigned int)ntohl(*(unsigned int*)value.Get(i).data());
			unsigned int right = (unsigned int)ntohl(*(unsigned int*)conValue.value().data());
			COMPARE2(left, right, conValue.operator_(), ok);
		}
		else if (type == MsgExpress::INT64)
		{
			int64_t left = (int64_t)ntohll(*(int64_t*)value.Get(i).data());
			int64_t right = (int64_t)ntohll(*(int64_t*)conValue.value().data());
			COMPARE2(left, right, conValue.operator_(), ok);
		}
		else if (type == MsgExpress::UINT64)
		{
			uint64_t left = (uint64_t)ntohll(*(uint64_t*)value.Get(i).data());
			uint64_t right = (uint64_t)ntohll(*(uint64_t*)conValue.value().data());
			COMPARE2(left, right, conValue.operator_(), ok);
		}
		else if (type == MsgExpress::FLOAT)
		{
			float left = *(float*)value.Get(i).data();
			float right = *(float*)conValue.value().data();
			COMPARE2(left, right, conValue.operator_(), ok);
		}
		else if (type == MsgExpress::DOUBLE)
		{
			double left = *(double*)value.Get(i).data();
			double right = *(double*)conValue.value().data();
			COMPARE2(left, right, conValue.operator_(), ok);
		}
		else if (type == MsgExpress::DATETIME)
		{
			time_t left = (time_t)ntohll(*(time_t*)value.Get(i).data());
			time_t right = (time_t)ntohll(*(time_t*)conValue.value().data());
			COMPARE2(left, right, conValue.operator_(), ok);
		}
		else if (type == MsgExpress::STRING)
		{
			COMPARE2(value.Get(i), conValue.value(), conValue.operator_(), ok);
		}
		else if (type == MsgExpress::BINARY)
		{
			COMPARE2(value.Get(i), conValue.value(), conValue.operator_(), ok);
		}
		if (ok)
			break;
	}
	return ok;
}

void Session::publish(unsigned int cmd, unsigned int serial, AppAddress srcAddr, MsgExpress::PublishData* pubData, vector<shared_ptr<Response>>* vecResponse)
{
	LOGGER_DEBUG("Publish:topic=" << pubData->topic());
	map<int, MsgExpress::DataItem> mapKeyValue;
	for (int i = 0; i<pubData->item_size(); i++)
	{
		mapKeyValue[pubData->item(i).key()].CopyFrom(pubData->item(i));
	}
	unordered_map<AppAddress, vector<MsgExpress::SubscribeData*>*, AppAddressHash> mapUserList;
	mServiceManager->getSubscriberList(pubData, mapUserList);
	unordered_map<AppAddress, vector<MsgExpress::SubscribeData*>*, AppAddressHash>::iterator it = mapUserList.begin();
	while (it != mapUserList.end())
	{
		LOGGER_DEBUG("Publish check user:"<<it->first.toString().c_str());
		if (mAddr != it->first)
		{
			MsgExpress::PublishData copyData;//(*pubData);
			copyData.set_topic(pubData->topic());
			std::set<int> setIndexes;
			vector<MsgExpress::SubscribeData*>::iterator iter = it->second->begin();
			while (iter != it->second->end())
			{
				bool allOK = true;
				for (int i = 0; i<(*iter)->excondition_size(); i++)
				{
					int key = (*iter)->excondition(i).key();
					MsgExpress::DataType type = (*iter)->excondition(i).type();
					bool oneOK = false;
					for (int j = 0; j<(*iter)->excondition(i).value_size(); j++)
					{
						const MsgExpress::ConditionValue& conValue = (*iter)->excondition(i).value(j);
						//oneOK = Satisfy2(mapKeyValue[key].value(), conValue, type);
						if (oneOK)
							break;
					}
					if (!oneOK)
					{
						allOK = false;
						break;
					}
				}
				if (allOK)
				{
					if ((*iter)->index_size()>0)
					{
						for (int i = 0; i<(*iter)->index_size(); i++)
						{
							int key = (*iter)->index(i);
							if (setIndexes.find(key) != setIndexes.end())
								continue;
							if (mapKeyValue.find(key) != mapKeyValue.end())
							{
								MsgExpress::DataItem *item = copyData.add_item();
								item->CopyFrom(mapKeyValue[key]);
								setIndexes.insert(key);
							}
						}
					}
					else
					{
						for (int j = 0; j<pubData->item_size(); j++)
						{
							int key = pubData->item(j).key();
							if (setIndexes.find(key) != setIndexes.end())
								continue;
							if (mapKeyValue.find(key) != mapKeyValue.end())
							{
								MsgExpress::DataItem *item = copyData.add_item();
								item->CopyFrom(pubData->item(j));
								setIndexes.insert(key);
							}
						}
					}
					copyData.add_subid((*iter)->subid());
				}
				delete *iter;

				iter++;
			}

			if (copyData.subid_size()>0)
			{
				string data;
				PackageHeader header;
				header.set_command ( cmd);
				header.set_type( PackageHeader::Publish);
				header.set_serialnum ( serial);
				header.set_srcaddr( srcAddr);
				header.set_dstaddr(0);
				if(MessageUtil::serializePackageToString(header, copyData, false, 50, &data))
				    post(vecResponse, data.data(), data.size(), it->first);
				else
				{
					LOG4_ERROR("SerializePackageToString failed");
				}
				/*CountInfo countInfo;
				countInfo.inPublish=1;
				countInfo.inPublishB=data.size();
				mServiceManager->updateCountInfo(it->first,countInfo);*/
				LOG4_DEBUG("Publish to %s,topic=%d,subid=%d", it->first.toString().c_str(), pubData->topic(), copyData.subid(0));
			}
		}
		delete it->second;
		it++;
	}
}

void Session::complexPublish(unsigned int cmd, unsigned int serial, AppAddress srcAddr, Package* package, vector<shared_ptr<Response>>* vecResponse)
{
	int complexKey;//只支持一个item是Complex
	map<uint64_t, const MsgExpress::Complex*> mapCompList;
	MsgExpress::PublishData* pubData = (MsgExpress::PublishData*)package->message().get();
	MsgExpress::PublishData baseData(*pubData);
	baseData.clear_item();
	const RepeatedPtrField<MsgExpress::DataItem>& list = pubData->item();
	RepeatedPtrField<MsgExpress::DataItem>::const_iterator iter = list.begin();
	while (iter != list.end())
	{
		if (iter->compval_size() > 0)
		{
			complexKey = iter->key();
			for (int i = 0; i<iter->compval_size(); i++)
			{
				mapCompList[iter->compval(i).a()] = &(iter->compval(i));
			}
		}
		else
		{
			MsgExpress::DataItem* item = baseData.add_item();
			item->CopyFrom(*iter);
		}
		iter++;
	}
	bool first = true;
	unordered_map<AppAddress, vector<MsgExpress::SubscribeData*>*, AppAddressHash> mapUserList;
	mServiceManager->getSubscriberList(pubData, mapUserList);
	unordered_map<AppAddress, vector<MsgExpress::SubscribeData*>*, AppAddressHash>::iterator it = mapUserList.begin();
	while (it != mapUserList.end())
	{
		int complexKey;
		const RepeatedPtrField<MsgExpress::Complex>* valueList = NULL;
		bool found = false;
		vector<MsgExpress::SubscribeData*>::iterator iter2 = it->second->begin();
		while (iter2 != it->second->end())
		{
			if (found)
				break;
			for (int i = 0; i<(*iter2)->condition_size(); i++)
			{
				if ((*iter2)->condition(i).compval_size()>0)
				{
					complexKey = (*iter2)->condition(i).key();
					valueList = &(*iter2)->condition(i).compval();
					found = true;
					break;
				}
			}
			iter2++;
		}
		if (found)
		{
			MsgExpress::PublishData copyData(baseData);
			for (RepeatedPtrField<MsgExpress::Complex>::const_iterator iter = valueList->begin(); iter != valueList->end(); iter++)
			{
				int64_t key = iter->a();
				if (mapCompList.find(key) != mapCompList.end())
				{
					MsgExpress::DataItem* item = copyData.add_item();
					item->set_key(complexKey);
					item->add_rawval(mapCompList[key]->b());
				}
			}
			string data;
			PackageHeader header;
			header.set_command (cmd);
			header.set_type( PackageHeader::Publish);
			header.set_serialnum (serial);
			header.set_srcaddr (srcAddr);
			header.set_dstaddr(0);
			MessageUtil::serializePackageToString(header, copyData, false, 50, &data);
			post(vecResponse, data.data(), data.size(), mAddr);
		}
		else
		{
			MsgExpress::AppInfo info;
			mServiceManager->getAppInfo(it->first, info);
			if (mInfo.type() == 3 && info.logininfo().type() != 3)
				post(vecResponse, package->content(), package->packagesize(), mAddr);
			else if (mInfo.type() != 3 && info.logininfo().type() != 3)
			{
				post(vecResponse, package->content(), package->packagesize(), mAddr);
			}
			else if (first && mInfo.type() != 3 && info.logininfo().type() == 3)
			{
				first = false;
				post(vecResponse, package->content(), package->packagesize(), mAddr);
			}
		}
		it++;
	}
}

void Session::simplePublish(unsigned int cmd, unsigned int serial, AppAddress srcAddr, Package* package, vector<shared_ptr<Response>>* vecResponse)
{
	unordered_set<AppAddress, AppAddressHash>* setIds = NULL;
	int topic = 0;
	//uint64_t t1=gettimepoint();
	if (package->classtype().compare(MsgExpress::PublishData::default_instance().GetTypeName()) == 0)
	{
		MsgExpress::PublishData* pubData = (MsgExpress::PublishData*)package->message().get();
		//LOG4_INFO("Publish:topic:%d,from:%d[0X%x],%s",pubData->topic(),srcAddr,srcAddr,pubData->ShortDebugString().c_str());
		topic = pubData->topic();
		setIds = mServiceManager->getSubscriberIdList(pubData);
	}
	else
	{
		package->SetPackageType(PackageHeader::MsgType::Publish);
		topic = package->GetCommand();
		string pubData;
		if (package->header().iszip())
		{
			bool unzipOK = MessageUtil::unCompress(package->GetBodyData(), package->GetBodySize(), package->header().compratio(), pubData);
			if (!unzipOK)
			{
				LOG4_ERROR("Publish failed,unzip failed,topic:%s", package->classtype().c_str());
				return;
			}
		}
		else
			pubData.assign(package->GetBodyData(), package->GetBodySize());
		setIds = mServiceManager->getSubscriberIdList(topic, pubData);
	}
	//uint64_t t2=gettimepoint();
	if (!this->server->getConfig().publish2myselt)//如果不需要推送给自己，就把自己从列表中删除
	{
		if (setIds->find(mAddr) != setIds->end())
			setIds->erase(setIds->find(mAddr));
	}
	//uint64_t t3=gettimepoint();
	post(vecResponse, package->content(), package->packagesize(), setIds);
	/*uint64_t t4=gettimepoint();
	int off=t4-t1;
	if(off>5)
	{
	int off1=t2-t1;
	int off2=t3-t2;
	int off3=t4-t3;
	LOG4_WARN("Publish cost time:%d(%d,%d,%d),%s",off,off1,off2,off3,pubData->ShortDebugString().c_str());
	}*/
	//string tmp;
	//char buf[32];
	//unordered_set<AppAddress, AppAddressHash>::const_iterator it = setIds->begin();
	//while (it != setIds->end())
	//{
	//	//CountInfo countInfo;
	//	//countInfo.inPublish=1;
	//	//countInfo.inPublishB=package->packagesize();
	//	//mServiceManager->updateCountInfo(*it,countInfo);
	//	sprintf(buf, "%s,", it->toString().c_str());
	//	tmp.append(buf);
	//	it++;
	//}
	//LOG4_INFO("Publish:topic:%d,from:%s,to:%s,serial:%d,content:", topic, srcAddr.toString().c_str(), tmp.c_str(), serial);
}
void Session::publish(unsigned int cmd, unsigned int serial, AppAddress srcAddr, Package* package, vector<shared_ptr<Response>>* vecResponse)
{
	if (package->classtype().compare(MsgExpress::PublishData::default_instance().GetTypeName()) == 0)
	{
		MsgExpress::PublishData* pubData = (MsgExpress::PublishData*)package->message().get();
		bool complex = false;
		const RepeatedPtrField<MsgExpress::DataItem>& list = pubData->item();
		RepeatedPtrField<MsgExpress::DataItem>::const_iterator it = list.begin();
		while (it != list.end())
		{
			if ( it->compval_size()>0)
			{
				complex = true;
				break;
			}
			it++;
		}
		if (!complex)
			simplePublish(cmd, serial, srcAddr, package, vecResponse);
		else
			complexPublish(cmd, serial, srcAddr, package, vecResponse);
	}
	else
	{
		simplePublish(cmd, serial, srcAddr, package, vecResponse);
	}
}
void Session::publish(AppAddress dstAddr, MessagePtr msg, vector<shared_ptr<Response>>* vecResponse)
{
	string data;
	PackageHeader header;
	header.set_command(CommandConfig::getInstance().GetCommand(msg->GetTypeName().c_str()));
	header.set_type(PackageHeader::Publish);
	header.set_serialnum(0);
	header.set_srcaddr(getAddress());
	header.set_dstaddr(0);
	if (MessageUtil::serializePackageToString(header, *msg.get(), true, 50, &data))
	{
		post(vecResponse, data.data(), data.size(), dstAddr);
	}
}
void Session::handle(Task* task)
{
	Package* package = task->request.get();
    if ((conn->protocol() == Protocol::P_SOCKET || conn->protocol() == Protocol::P_WEBSOCKET) && !hasLogin && package->classtype() != MsgExpress::LoginInfo::default_instance().GetTypeName())	
    { 
		stop();
		return;
	}

	std::string msgtype = CommandConfig::getInstance().GetClass(package->header().command());
	if (msgtype.length() == 0)
	{
		reply(package, MsgExpress::ERRCODE_NOSERVICE, MsgExpress::ERRMSG_NOSERVICE, &task->vecResponse);
		return;
	}
	int serviceid = CommandConfig::getInstance().GetServiceId(msgtype.c_str());
	package->set_classtype(msgtype);
	if (package->IsRequest())
	{
		if (serviceid == 0)
		{
			if (package->header().protocol() != 0)
			{
				LOG4_ERROR(package->DebugString().c_str());
				return;
			}
			if (!MessageUtil::parse(package))
			{
				LOG4_ERROR("Request to broker,but parse message failed! content:%s", package->DebugString().c_str());
				return;
			}
			else if (package->message()->GetTypeName() == MsgExpress::LoginInfo::default_instance().GetTypeName())
			{
				login(task);
			}
			else if (package->message()->GetTypeName() == MsgExpress::Logout::default_instance().GetTypeName())
			{
				logout(task);
			}
			else if (package->message()->GetTypeName() == MsgExpress::HeartBeat::default_instance().GetTypeName())
			{
				heartBeat(task);
			}
			else if (package->message()->GetTypeName() == MsgExpress::SubscribeData::default_instance().GetTypeName())
			{
				subscribe(task);
			}
			else if (package->message()->GetTypeName() == MsgExpress::UnSubscribeData::default_instance().GetTypeName())
			{
				unsubscribe(task);
			}
			else if (package->message()->GetTypeName() == MsgExpress::ComplexSubscribeData::default_instance().GetTypeName())
			{
				complexSubscribe(task);
			}
			else if (package->message()->GetTypeName() == MsgExpress::SimpleSubscription::default_instance().GetTypeName())
			{
				subscribe(task);
			}
			else if (package->message()->GetTypeName() == MsgExpress::RegisterService::default_instance().GetTypeName())
			{
				regservice(task);
			}
			else if (package->message()->GetTypeName() == MsgExpress::GetAppList::default_instance().GetTypeName())
			{
				MsgExpress::AppList resp;
				mServiceManager->getAppInfo(*(MsgExpress::GetAppList*)package->message().get(), resp);
				reply(package, resp, &task->vecResponse);
			}
			else if (package->message()->GetTypeName() == MsgExpress::GetAppInfo::default_instance().GetTypeName())
			{
				getAppInfo(task);
			}
			else {
				LOG4_ERROR("error message,content:%s", package->DebugString().c_str());
				return;
			}
		}
		else if (package->IsMulticast())
		{
			std::vector<AppAddress> sidlist;
			std::vector<AppAddress> sidlistproxy;
			mServiceManager->getServiceProviderList(serviceid, sidlist, sidlistproxy);
			for (int i = 0; i<sidlist.size(); i++)
			{
				if (sidlist[i] == mAddr)
					continue;
				post(&task->vecResponse, package->content(), package->packagesize(), sidlist.at(i));
#ifdef TONGJI
				CountInfo countInfo;
				countInfo.inRequest = 1;
				countInfo.inRequestB = package->packagesize();
				mServiceManager->updateCountInfo(sidlist.get(i).value, countInfo);
#endif
			}
			for (int i = 0; i<sidlistproxy.size(); i++)
			{
				if (sidlistproxy.at(i) == mAddr)
					continue;
				if (mInfo.type() == 3)
					break;
				post(&task->vecResponse, package->content(), package->packagesize(), sidlistproxy.at(i));
				break;
#ifdef TONGJI
				CountInfo countInfo;
				countInfo.inRequest = 1;
				countInfo.inRequestB = package->packagesize();
				mServiceManager->updateCountInfo(sidlist.get(i).value, countInfo);
#endif
			}
		}
		else if (package->GetDstAddr() != 0)
		{
			AppAddress sid(package->GetDstAddr());
			post(&task->vecResponse, package->content(), package->packagesize(), sid);
#ifdef TONGJI
			CountInfo countInfo;
			countInfo.inRequest = 1;
			countInfo.inRequestB = package->packagesize();
			mServiceManager->updateCountInfo(sid.value, countInfo);
#endif
		}
		else
		{
			AppAddress sid = mServiceManager->getServiceProvider(serviceid, mAddr, mInfo, package->header().code());
			if (sid.Id != 0/* && sid.value != mAddr*/)//可以发自己
			{
				//LOG4_INFO("Find addr %s for service %d", sid.toString().c_str(), package->GetAppId());
				post(&task->vecResponse, package->content(), package->packagesize(), sid);
#ifdef TONGJI
				CountInfo countInfo;
				countInfo.inRequest = 1;
				countInfo.inRequestB = package->packagesize();
				mServiceManager->updateCountInfo(sid.value, countInfo);
#endif
			}
			else //send error response msg
			{
				reply(package, MsgExpress::ERRCODE_NOSERVICE, MsgExpress::ERRMSG_NOSERVICE, &task->vecResponse);
				LOG4_ERROR("No service available:%s\r\n", package->DebugString().c_str());
				CountInfo countInfo;
				countInfo.inResponse = 1;
				mServiceManager->updateCountInfo(mAddr, countInfo);
			}
		}
	}
	else if (package->IsPublish())
	{
		package->AddBrokerTrace(mAddr.Broker.Id);
		bool answer = true;
		if (package->GetCommand() == 0 || package->GetSerialNum() == 0)//从broker发出的publish不应答
			answer = false;
		if (package->GetSrcAddr() != mAddr) //不是本app发出的publish不应答
			answer = false;
		if (answer && this->server->getConfig().publishneedreply)
		{
			MsgExpress::CommonResponse resp;
			resp.set_retcode(0);
			resp.set_msg("Publish success");
			reply(package, resp, &task->vecResponse);
		}
		if (package->classtype() == MsgExpress::BrokerInfo::default_instance().GetTypeName())
		{
			bool ret = MessageUtil::parse(package);
			if (package->message())
			{
				server->updateBrokerInfo((MsgExpress::BrokerInfo*)package->message().get());
				return;
			}
		}
		else if (package->classtype() == MsgExpress::RegisterService::default_instance().GetTypeName())
		{
			bool ret = MessageUtil::parse(package);
			if (package->message())
			{
				regservice(task);
			}
		}
		else
		{
			bool ret = true;
			if (package->classtype() == MsgExpress::PublishData::default_instance().GetTypeName())
			    ret = MessageUtil::parse(package);
			if (ret)
			{
				publish(package->GetCommand(), package->GetSerialNum(), package->GetSrcAddr(), package, &task->vecResponse);
			}
			else
			{
				LOG4_ERROR("Error:Publish,parse failed");
			}
		}
		
	}
	else if (package->IsResponse())
	{
		unsigned int dst = package->GetDstAddr();
		post(&task->vecResponse, package->content(), package->packagesize(), dst);
#ifdef TONGJI
		CountInfo countInfo;
		countInfo.inResponse = 1;
		countInfo.inResponseB = package->packagesize();
		mServiceManager->updateCountInfo(sid.value, countInfo);
#endif
	}
	else
		LOG4_ERROR("%s", package->DebugString().c_str());
}

void Session::login(Task* task)
{
	Package* package = task->request.get();
	MsgExpress::LoginInfo* login = (MsgExpress::LoginInfo*)package->message().get();
	if (login == nullptr)
		return;
	if (server->getConfig().clustermode==enumClusterMode::MasterSlave && server->isSlaveBroker())
	{
		if ((login->type() == 0 || login->type() == 1))
		{
			LOG4_INFO("Login to slave broker denied:addr=%s,%s", mAddr.toString().c_str(), login->ShortDebugString().c_str());
			stop();
			return;
		}
	}
	login->set_ip(sockInfo.ip);
	mInfo.CopyFrom(*login);

	time_t tm;
	time(&tm);
	uint64_t brokertime = gettimepoint();
	MsgExpress::LoginResponse resp;
	resp.set_addr(mAddr);
	resp.set_brokertime(brokertime);
	reply(package, resp, &task->vecResponse);

	vector<int> vecApps;
	if (login->type() > 0)
	{
		string md5code = md5(login->auth());
		if (md5code == mServiceManager->getAuthData())
			hasSuperRights = true;

		int total = login->serviceid_size();
		if (hasSuperRights)
		{
			for (int i = 0; i < total; i++)
			{
				int serviceId = login->serviceid(i);
				if (mServiceList.count(serviceId) == 0)
				{
					mServiceList.insert(serviceId);
					vecApps.push_back(serviceId);
				}
			}

		}
	}
	if (!closed())
	{
		mServiceManager->regApp(mAddr, *login);
		/*if (vecApps.size()>0)
			mServiceManager->regService(vecApps, mAddr);
		for (int i = 0; i < vecApps.size(); i++)
			server->updateService(vecApps[i]);*/
	}
	hasLogin = true;


	unordered_set<AppAddress, AppAddressHash>* dest = mServiceManager->getSubscriberIdList(package->GetCommand(),"");
	for (auto addr : *dest)
	{
		publish(addr, package->message(), &task->vecResponse);
	}
	
	LOG4_INFO("AppServer login:addr=%s,%s,resp:%s", mAddr.toString().c_str(), login->ShortDebugString().c_str(), resp.ShortDebugString().c_str());

    map<int, std::shared_ptr< MsgExpress::RegisterService>> services = CommandConfig::getInstance().GetAllServiceInfo();
	map<int, std::shared_ptr< MsgExpress::RegisterService>>::const_iterator it = services.begin();
	while (it != services.end())
	{
		publish(this->mAddr, it->second, &task->vecResponse);
		it++;
	}
}

void Session::logout(Task* task)
{
	Package* package = task->request.get();
	MsgExpress::CommonResponse resp;
	resp.set_retcode(0);
	reply(package, resp, &task->vecResponse);

	LOG4_INFO("AppServer logout:addr=%s,%s", mAddr.toString().c_str(), package->message()->ShortDebugString().c_str());
}

void Session::regservice(Task* task)
{
	Package* package = task->request.get();
	MsgExpress::CommonResponse resp;
	resp.set_retcode(0);
	reply(package, resp, &task->vecResponse);
	shared_ptr<MsgExpress::RegisterService> reg = dynamic_pointer_cast<MsgExpress::RegisterService>(package->message());
	CommandConfig::getInstance().Register(reg);
	simplePublish(0, 0, server->getAddress(), package, &task->vecResponse);
	mServiceList.insert(reg->serviceid());
	std::vector<int> vec;
	vec.push_back(reg->serviceid());
	mServiceManager->regService(vec, mAddr);
	server->updateService(reg->serviceid());
	LOG4_INFO("AppServer regservice:serviceid:%d,addr=%s,function list:%s", reg->serviceid(), mAddr.toString().c_str(), reg->ShortDebugString().c_str());
}

void Session::subscribe(Task* task)
{
	Package* package = task->request.get();
	/*if (!hasSuperRights)
	{
		MsgExpress::CommonResponse resp;
		resp.set_retcode(1);
		resp.set_msg("Authenrity is not correct");
		reply(package, resp, &task->vecResponse);
		return;
	}*/
	if (closed())
		return;
	if (package->classtype().compare(MsgExpress::SubscribeData::default_instance().GetTypeName()) == 0)
	{
		MsgExpress::SubscribeData* sub = (MsgExpress::SubscribeData*)package->message().get();
		if (sub == NULL)
			return;
		AppAddress addr = mAddr;
		if (sub->useraddr() != 0)
		{
			addr = sub->useraddr();
			LOG4_INFO("Subscribe for %s,topic:0X%x,subid:%d,content:%s", addr.toString().c_str(), sub->topic(), sub->subid(), sub->ShortDebugString().c_str());
		}
		else
			LOG4_INFO("Subscribe for myself %s,topic:0X%x,subid:%d,content:%s", addr.toString().c_str(), sub->topic(), sub->subid(), sub->ShortDebugString().c_str());
		mServiceManager->subscribe(*sub, addr);
	}
	else if (package->classtype().compare(MsgExpress::SimpleSubscription::default_instance().GetTypeName()) == 0)
	{
		MsgExpress::SimpleSubscription* sub = (MsgExpress::SimpleSubscription*)package->message().get();
		if (sub == NULL)
			return;
		AppAddress addr = mAddr;
		if (sub->useraddr() != 0)
		{
			addr = sub->useraddr();
			LOG4_INFO("Subscribe for %s,topic:0X%x,subid:%d,content:%s", addr.toString().c_str(), sub->topic(), sub->subid(), sub->ShortDebugString().c_str());
		}
		else
			LOG4_INFO("Subscribe for myself %s,topic:0X%x,subid:%d,content:%s", addr.toString().c_str(), sub->topic(), sub->subid(), sub->ShortDebugString().c_str());
		mServiceManager->subscribe(*sub, addr);
	}
	MsgExpress::CommonResponse resp;
	resp.set_retcode(0);
	resp.set_msg("subscribe succeed.");
	reply(package, resp, &task->vecResponse);
}

void Session::complexSubscribe(Task* task)
{
	Package* package = task->request.get();
	/*if (!hasSuperRights)
	{
		MsgExpress::CommonResponse resp;
		resp.set_retcode(1);
		resp.set_msg("Authenrity is not correct");
		reply(package, resp, &task->vecResponse);
		return;
	}*/
	MsgExpress::ComplexSubscribeData* complexSub = (MsgExpress::ComplexSubscribeData*)package->message().get();
	if (complexSub == NULL)
		return;
	LOG4_INFO("Addr=%s,ComplexSubscribe:%s\r",mAddr.toString().c_str(), complexSub->ShortDebugString().c_str());

	for (int i = 0; i<complexSub->unsub_size(); i++)
	{
		unsigned int addr = mAddr;
		if (complexSub->unsub(i).useraddr() != 0)
			addr = complexSub->unsub(i).useraddr();
		mServiceManager->unSubscribe(complexSub->unsub(i).subid(), addr);
	}
	for (int i = 0; i<complexSub->sub_size(); i++)
	{
		unsigned int addr = mAddr;
		if (complexSub->sub(i).useraddr() != 0)
			addr = complexSub->sub(i).useraddr();
		mServiceManager->subscribe(complexSub->sub(i), addr);
	}
	MsgExpress::CommonResponse resp;
	resp.set_retcode(0);
	reply(package, resp, &task->vecResponse);
}
void Session::unsubscribe(Task* task)
{
	Package* package = task->request.get();
	/*if (!hasSuperRights)
	{
		MsgExpress::CommonResponse resp;
		resp.set_retcode(1);
		resp.set_msg("Authenrity is not correct");
		reply(package, resp, &task->vecResponse);
		return;
	}*/
	MsgExpress::UnSubscribeData* unsub = (MsgExpress::UnSubscribeData*)package->message().get();
	if (unsub == NULL)
		return;
	LOG4_INFO("unSubscribe:%s\r", unsub->ShortDebugString().c_str());
	int subId = unsub->subid();
	unsigned int addr = mAddr;
	if (unsub->useraddr() != 0)
		addr = unsub->useraddr();
	mServiceManager->unSubscribe(subId, addr);

	MsgExpress::CommonResponse resp;
	resp.set_retcode(0);
	reply(package, resp, &task->vecResponse);
}

void Session::heartBeat(Task* task)
{
	Package* package = task->request.get();
	MsgExpress::HeartBeat* heartBeat = (MsgExpress::HeartBeat*)package->message().get();
	if (heartBeat == NULL)
		return;
	uint64_t brokertime = gettimepoint();

	MsgExpress::HeartBeatResponse resp;
	resp.set_retcode(0);
	resp.set_brokertime(brokertime);
	resp.set_servertime(heartBeat->servertime());
	reply(package, resp, &task->vecResponse);
	LOG4_DEBUG("heartBeat Response");
	simplePublish(0, 0, server->getAddress(), package, &task->vecResponse);
	/*time_t tm;
	time(&tm);

	MsgExpress::PublishData pub;
	pub.set_topic(MsgExpress::TOPIC_HEARTBEAT);
	PublishHelperPtr helper = CreatePublishHelper(&pub);
	helper->AddDatetime(MsgExpress::KEY_HBTIME, tm);
	helper->AddString(MsgExpress::KEY_BROKER, mServiceManager->getBrokerName());
	helper->AddUInt32(MsgExpress::KEY_ADDR, mAddr);
	helper->AddString(MsgExpress::KEY_UUID, mInfo.uuid());
	helper->AddUInt32(MsgExpress::KEY_CPU, heartBeat->cpu());
	helper->AddUInt32(MsgExpress::KEY_TOPMEM, heartBeat->topmemory());
	helper->AddUInt32(MsgExpress::KEY_MEM, heartBeat->memory());
	helper->AddUInt32(MsgExpress::KEY_CSQUEUE, heartBeat->sendqueue());
	helper->AddUInt32(MsgExpress::KEY_CRQUEUE, heartBeat->receivequeue());

	helper->AddUInt64(MsgExpress::KEY_RECVREQUEST, heartBeat->recvrequest());
	helper->AddUInt64(MsgExpress::KEY_SENTREQUEST, heartBeat->sendrequest());
	helper->AddUInt64(MsgExpress::KEY_RECVRESPONSE, heartBeat->recvresponse());
	helper->AddUInt64(MsgExpress::KEY_SENTRESPONSE, heartBeat->sendresponse());
	helper->AddUInt64(MsgExpress::KEY_RECVPUBLISH, heartBeat->recvpub());
	helper->AddUInt64(MsgExpress::KEY_SENTPUBLISH, heartBeat->sendpub());
	helper->AddString(MsgExpress::KEY_SERVERSTATUS, heartBeat->status());

	publish(0, 0, server->getAddress(), &pub, &task->vecResponse);*/
}

void Session::getAppInfo(Task* task)
{
	Package* package = task->request.get();
	MsgExpress::GetAppInfo* req = (MsgExpress::GetAppInfo*)package->message().get();
	AppAddress addr = req->addr();
	if (hasSuperRights)
	{
		MsgExpress::AppInfo resp;
		if (mServiceManager->getAppInfo(addr, resp))
			reply(package, resp, &task->vecResponse);
		else
		{
			reply(package, Gateway::ERRCODE_NOAPPINFO, Gateway::ERRMSG_NOAPPINFO, &task->vecResponse);
			LOG4_ERROR("get app info failed,addr=%s", addr.toString().c_str());
		}
	}
	else
	{
		MsgExpress::CommonResponse resp;
		resp.set_retcode(1);
		resp.set_msg("Authenrity is not correct");
		reply(package, resp, &task->vecResponse);
	}
}
